local class    = require 'gabe.class'
local gs       = require 'gamestate'
local colors   = require 'gfx.colors'
local prompts  = require 'gfx.prompts'

local escape = class(...)

function escape:init(escape_to)
	self.escape_to = escape_to
end

function escape:draw(x, y)
	local ax, ay = x+28, y+7
	local bx, by = y+2, y+6
	if S.input_method == "gamepad" then
		prompts.drawButton("b", bx, by)
	else
		prompts.drawKey("escape", "ESC", bx, by)
	end
	love.graphics.setColor(colors.white)
	love.graphics.print("QUIT", ax, ay)
	if self.timer_start then
		local w = 22
		local d = (love.timer.getTime() - self.timer_start) / .5
		love.graphics.rectangle('fill', ax, ay+12, w*d, 4)
		love.graphics.setLineStyle('rough')
		love.graphics.rectangle('line', ax+1, ay+12, w, 4)
		if not self.timer_check() then
			self.timer_start = nil
		elseif d >= 1 then
			gs.switch(self.escape_to)
		end
	end
end

function escape:escape(check)
	print("ESCAPE")
	self.timer_start = love.timer.getTime()
	self.timer_check = check
end

return escape
