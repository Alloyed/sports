-- drawing overlay
-- for funsies
local class = require 'gabe.class'
local colors = require 'gfx.colors'
local teamcolors = require 'gfx.teamcolors'

local overlay = class(...)
overlay.Brush_w  = 2
overlay.Eraser_w = 40

function overlay:init(color)
	self.team = color or 1
	self.lines = {}
	self.linedata = {}
	self.tmp_lines = {}
	self.tool  = nil
end

function overlay:keypressed(k)
	if self.tool and k == 'escape' then
		self.tool = nil
		return true
	end

	if k == '1' then
		self.tool = 'brush'
		self.place = false
		return true
	end

	if k == '2' then
		self.tool = 'eraser'
		self.place = false
		return true
	end
	return false
end

function overlay:mousepressed(x, y, m)
	if self.tool then
		self.place = true
		self.dirty = love.timer.getTime()
		self.tmp_line = {x, y}
		self.tmp_linedata = {
			color = self.tool == 'brush' and teamcolors[self.team] or colors.clear,
			width = self.tool == 'brush' and self.Brush_w or self.Eraser_w
		}
	end
	return true
end

function overlay:mousemoved(x, y, ...)
	if self.place then
		table.insert(self.tmp_line, x)
		table.insert(self.tmp_line, y)
	end
	return true
end

function overlay:mousereleased(...)
	if self.tool then
		self.place = false
		table.insert(self.lines, self.tmp_line)
		table.insert(self.linedata, self.tmp_linedata)
		self.tmp_line, self.tmp_linedata = nil, nil
		if A.client then
			A.client:msg("sendLine", {
				line = self.lines[#self.lines],
				data = self.linedata[#self.lines]
			})
		end
	end
	return true
end

function overlay:update(dt)
	if self.tmp_line and love.timer.getTime() - self.dirty > .05 then
		A.client:msg("tmpLine", {
			line = self.tmp_line,
			data = self.tmp_linedata
		})
		self.dirty = love.timer.getTime()
	end
end

function overlay:recv_line(data)
	table.insert(self.lines,    data.line)
	table.insert(self.linedata, data.data)
	self.tmp_lines[data.origin] = nil
end

function overlay:recv_tmp_line(data)
	self.tmp_lines[data.origin] = data
end

function overlay:draw()
	if true then return end
	if not C[self].cv then
		C[self].cv = love.graphics.newCanvas(A.lbox:dimensions())
	end
	local cv = C[self].cv

	love.graphics.push'all'
	love.graphics.setCanvas(cv)
	love.graphics.clear(0,0,0,0)
	love.graphics.setLineStyle('rough')
	love.graphics.setLineWidth(2)
	love.graphics.setLineJoin('bevel')
	love.graphics.setBlendMode('replace')
	for i, line in ipairs(self.lines) do
		if #line >= 4 then
			local ldata = self.linedata[i]
			love.graphics.setColor(ldata.color)
			love.graphics.setLineWidth(ldata.width)
			love.graphics.line(line)
		else
			love.graphics.circle('fill', line[1], line[2], 2)
		end
	end

	for id, data in pairs(self.tmp_lines) do
		if #data.line >= 4 then
			local ldata = data.data
			love.graphics.setColor(ldata.color)
			love.graphics.setLineWidth(ldata.width)
			love.graphics.line(data.line)
		else
			love.graphics.circle('fill', data.line[1], data.line[2], 2)
		end
	end

	if self.tmp_line  then
		local line = self.tmp_line
		if #self.tmp_line >= 4 then
			local ldata = self.tmp_linedata
			love.graphics.setColor(ldata.color)
			love.graphics.setLineWidth(ldata.width)
			love.graphics.line(self.tmp_line)
		else
			love.graphics.circle('fill', line[1], line[2], 2)
		end
	end
	love.graphics.pop()
	love.graphics.setColor(colors.white)
	love.graphics.draw(cv, 0, 0)
	if self.tool then
		love.graphics.print(self.tool, 300, 4)
		local mx, my = A.lbox:mouse()
		local diam = self.tool == "brush" and self.Brush_w or self.Eraser_w
		love.graphics.circle('line', mx, my, diam/2)
	end
end

return overlay
