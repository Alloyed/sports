--- A small pubsub system.
-- It works by buffering a single frame of events, so a reciever will get an
-- event one frame after it triggers.
--
-- On the pub side:
--   myQueue:pub("myEvent", { owner = self })
-- On the sub side:
--   for eventName, eventData in myQueue:iter("myEvent") do
--		print(eventData.owner)
--   end
-- please make event names simple string keys thanks~

local class = require 'gabe.class'
local misc  = require 'misc'

local events = class(...)

function events:init()
	self.lastFrame    = {}
	self.currentFrame = {}
end

function events:swap()
	self.lastFrame, self.currentFrame = self.currentFrame, self.lastFrame
	misc.iclear_(self.currentFrame)
end

function events:pub(eventName, data)
	table.insert(self.currentFrame, {eventName, data})
end

-- publish an event, and if possible also send it to the server for
-- dissemination
function events:netpub(eventName, data)
	self:pub(eventName, data)
	if A.client then
		A.client:msg("event", eventName, data)
	end
end

function events:iter(...)
	local keys = misc.set(...)
	return coroutine.wrap(function()
		for _, event in ipairs(self.lastFrame) do
			if keys[event[1]] then
				coroutine.yield(event[1], event[2])
			end
		end
		coroutine.yield(nil)
	end)
end

return events
