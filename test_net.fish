#!/usr/bin/fish

# don't forget to change the server URL

zip -ru "../sports-alpha.love" * -x \*.zip \*.love \*.wav \*.ase \*.sh \*.fish assets/unused
share ../sports-alpha.love

# then on the server do
# love shared/sports-alpha.love --server
