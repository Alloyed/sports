local colors = require 'gfx.colors'
local assman = require 'assman'
local vec    = require 'vec'

local prompts = {}

function prompts.load()
	assman.load.anim("prompts")
end

function prompts.keyBox(display)
	local fnt = love.graphics.getFont()
	return fnt:getWidth(display) + 2 + 4, fnt:getHeight()
end

function prompts.drawKey(key, display, x, y)
	x, y = vec.map(x+.5, y+.5, math.floor)
	--y = y +1
	local old_f = love.graphics.getFont()
	--love.graphics.setFont(A.openbm)
	local m = display
	local fnt = love.graphics.getFont()
	local w = fnt:getWidth(m) + 2
	local h = fnt:getHeight() - 2
	if love.keyboard.isDown(key) then
		love.graphics.setColor(colors.lite)
		love.graphics.print(m, x+1, y+2)
		love.graphics.setLineStyle('rough')
		love.graphics.rectangle('line', x-1, y, w+2, h+2)
	else
		love.graphics.setColor(colors.white)
		love.graphics.print(m, x+1, y-0)
		love.graphics.setLineStyle('rough')
		love.graphics.rectangle('line', x-1, y, w+2, h+2)
	end
	love.graphics.setFont(old_f)
	return w+4, 0
end

local bset = {
	"dpup", "dpright", "dpdown", "dpleft",
	"y", "b", "a", "x",
	"lstick", "rstick",
	"rt", "rb", "lt", "lb",
	"l3", "r3"
}
local __btns = {}
for i, b in ipairs(bset) do
	__btns[b] = i
end
function prompts.drawButton(key, x, y)
	love.graphics.setColor(255, 255, 255)
	x, y = math.floor(x), math.floor(y)
	A.prompts:drawFrame(1, __btns[key] or 1, x, y-6, 0, 2, 2)
	return 24+3, 0
end

return prompts
