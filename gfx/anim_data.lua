local data = {}

data["prompts.png"] = {
	cellSize = {12, 12}
}

data["male_archer.png"] = {
	cellSize = {64, 64},
	rows = {
		{lastFrame = 1}, -- idle
		{lastFrame = 8}, -- walk
		{lastFrame = 1}, -- jump
		{lastFrame = 1}, -- shoot
		{lastFrame = 2}, -- arms
		{lastFrame = 1}, -- fall
	}
}

data["target.png"] = {
	cellSize = {32, 64},
	rows = {
		{},{},{},{}, -- idle variants
		{lastFrame=1},
		{lastFrame=1},
		{lastFrame=1},
		{lastFrame=1}, -- surprised
	}
}

data["obstacle_rock1.png"] = {
	image = true,
}

data["arrow.png"] = {
	cellSize = {15, 10},
	frameDuration = 16*4,
	rows = {
		{}, -- spin
		{lastFrame = 1}, -- embedded
	}
}

data["stars.png"] = {
	cellSize = {16, 16},
}

data["moon.png"] = {
	image = true
}

data["hud_time.png"] = {
	image = true
}

data["hud_players.png"] = {
	cellSize = {100, 45},
}

data["offscreen_arrow.png"] = {
	image = true
}

data["goddess.png"] = {
	image = true
}

data["target_pole.png"] = {
	image = true
}

data["apple.png"] = {
	cellSize = {16, 16}
}

data["medals.png"] = {
	cellSize = {16, 16}
}

data["spelunking/thief.png"] = {
	cellSize = {48, 48},
	rows = {
		{lastFrame = 6}, -- idle
		{lastFrame = 6}, -- walk
		{lastFrame = 6}, -- strike_right
		{lastFrame = 6}, -- strike_left
	}
}

data["spelunking/small_loot.png"] = {
	cellSize = {16, 16},
}

data["spelunking/ground_tiles.png"] = {
	cellSize = {16, 16},
}

data["spelunking/platform_tiles.png"] = {
	cellSize = {16, 16},
}

data["editor.png"] = {
	cellSize = {24, 24},
}

return data
