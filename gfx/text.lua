local misc  = require 'misc'
local text = {}

function text.chars_t(str, fn, x, y)
	local t
	local fnt = love.graphics.getFont()
	if not C[text].t then
		t = love.graphics.newText(fnt, nil)
		C[text].t = t
	else
		C[text].t:clear()
	end
	t = C[text].t
	local nx, ny = 0, 0
	local maxw = 0
	for i=1, string.len(str) do
		local substr = string.sub(str, i, i)
		local id, w, h = fn(t, substr, i, x+nx, y+ny)
		assert(id or w or h, "missing return value")

		if w then
			ny = ny + (h or 0)
			nx = nx + (w or 0)
		elseif substr == "\n" then
			ny = ny + t:getHeight(id) * fnt:getLineHeight()
			nx = 0
		else
			nx = nx + t:getWidth(id)
		end
		if nx > maxw then maxw = nx end
	end
	return t, maxw
end

function text.chars(str, fn, x, y)
	local color = {love.graphics.getColor()}
	local t, maxw = text.chars_t(str, fn, x, y)
	love.graphics.setColor(color)
	love.graphics.draw(t)
	return maxw
end

function text.words(str, fn, x, y)
	local color = {love.graphics.getColor()}
	local fnt   = love.graphics.getFont()

	local t
	if not C[text].t then
		t = love.graphics.newText(love.graphics.getFont(), nil)
		C[text].t = t
	else
		C[text].t:clear()
	end
	t = C[text].t
	local nx, ny = 0, 0
	local i = 1
	local iter = 0
	while i <= string.len(str) do
		iter = iter+1 assert(iter < 10000)
		local s, e = string.find(str, "%S+", i)
		local substr = string.sub(str, s, e)
		local id, w, h = fn(t, substr, s, math.floor(x+nx), math.floor(y+ny))
		assert(id or w or h, "missing return value")

		if not id then
			ny = ny + (h or 0)
			nx = nx + (w or 0)
		else
			nx = nx + t:getWidth(id)
		end

		i = e+1
		substr = string.sub(str, i, i)

		while substr:match("^%s$")  do
			--iter = iter+1 assert(iter < 10000)
			if substr == "\n" then
				ny = ny + fnt:getHeight("\n") * fnt:getLineHeight()
				nx = 0
			else
				nx = nx + fnt:getWidth(substr)
			end
			i = i + 1
			substr = string.sub(str, i, i)
		end
	end
	love.graphics.setColor(color)
	love.graphics.draw(t)
	return iter
end

text.fx = {}

local _wobble = function(t, str, i, x, y)
	return t:add(str, x, y+misc.osc(1, 2, i/4)-1)
end
function text.fx.wobble()
	return _wobble
end

local _pulse = function(t, str, i, x, y)
	local h = 6
	return t:add(str, x, y+h, 0, .6+math.max(.4, misc.osc(1, .6, -i/20)), nil, 0, h)
end
function text.fx.pulse()
	return _pulse
end

local RNG = love.math.newRandomGenerator()
local _shake = function(t, str, i, x, y)
	RNG:setSeed(i+math.floor(love.timer.getTime()*20))
	local n1, n2 = love.math.noise(love.timer.getTime()*2, i), love.math.noise(love.timer.getTime()*10, i)
	return t:add(str, x+(n1*2-1), y+(n2*2-1))
end
function text.fx.shake()
	return _shake
end

text.fx.none = function(t, str, i, x, y)
	return t:add(str, x, y)
end

return text
