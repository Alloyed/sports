local colors = require 'gfx.colors'
local misc   = require 'misc'

local template = {}

function template.draw(fn)
	love.graphics.push()
	love.graphics.clear(colors[15])

	local stripe_w = 40
	local duty = .5
	local screen_w, screen_h = A.lbox:dimensions()
	local sx = (love.timer.getTime()*.33 % 1) * stripe_w*(1/duty)
	for x=sx-stripe_w, screen_w, stripe_w*(1/duty) do
		love.graphics.setColor(colors[2])
		love.graphics.rectangle('fill', x, 0, stripe_w, screen_h)
		love.graphics.rectangle('fill', 0, x, screen_w, stripe_w*.75)
	end

	love.graphics.setColor(misc.rgba(colors.black, 127))
	love.graphics.rectangle('fill', 5, 5, screen_w-10, screen_h-10)

	love.graphics.setColor(colors.white)
	love.graphics.setScissor(5, 5, screen_w-10, screen_h-10)
	love.graphics.translate(5, 5)
	fn(screen_w-10, screen_h-10)
	love.graphics.setScissor()
	love.graphics.pop()
end

return template
