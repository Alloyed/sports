local class    = require 'gabe.class'
local misc     = require 'misc'
local vec      = require 'vec'
local polyline  = require 'gfx.polyline'

local trail = class("gfx.trail")

function trail:init(startx, starty)
	self.emitterDist = 5
	self.emitterPos  = vec.new(startx, starty)

	self.dotLifetime = 3
	self.dotMax      = 64

	-- private vars
	self._dots     = {}
	for i=1, self.dotMax do self._dots[i] = {
		i=i,
		x=0,y=0,
		vx=0,vy=0,
		ax=0,ay=0,
		t=0,
		alive=false
	} end
	self._currentDot = 1
end

function trail.Activate(dot)
end

function trail:moveEmitter(newx, newy)
	local x, y = vec.unpack(newx, newy)
	self.emitterPos:set_(x, y)

	local cdot = self._dots[self._currentDot]
	vec.set_(cdot, self.emitterPos)

	local ldot = self._dots[((self._currentDot-2) % self.dotMax) + 1]
	local dist = vec.dist(ldot, cdot)
	if dist > self.emitterDist then
		self._currentDot = (self._currentDot % self.dotMax) + 1 

		cdot = self._dots[self._currentDot]
		cdot.alive = true
		cdot.t = self.dotLifetime
		self.Activate(cdot)
	end

	vec.set_(cdot, self.emitterPos)
end

function trail:moveAll(dx, dy)
	local i = self._currentDot
	local nextDot = self._dots[i]
	local iters = 0

	while nextDot and nextDot.alive and iters < self.dotMax do
		local dot = nextDot

		i = ((i-2) % self.dotMax) + 1 
		iters = iters + 1
		nextDot = self._dots[i]

		if nextDot and nextDot.alive then
			vec.add_(dot, dx, dy)
		end
	end
end

function trail:update(dt)
	for _, dot in ipairs(self._dots) do
		if dot.alive then
			dot.y = dot.y + dot.vy*dt
			dot.vy = dot.vy + dot.ay*dt
			dot.t = dot.t - dt
			if dot.t <= 0 then
				dot.alive = false
				dot.final_t = false
			end
		end
	end
end

function trail:draw(x, y)
	x, y = vec.unpack(x, y)
	x = x or 0
	y = y or 0

	local i = self._currentDot
	local nextDot = self._dots[i]
	local iters = 0
	local lastDot = nil
	local points = {}

	while nextDot and nextDot.alive and iters < self.dotMax do
		local dot = nextDot

		i = ((i-2) % self.dotMax) + 1 
		iters = iters + 1
		nextDot = self._dots[i]

		if nextDot and nextDot.alive then
			table.insert(points, dot.x)
			table.insert(points, dot.y)
		elseif lastDot and lastDot.alive then
			local t = 1
			local x, y = misc.lerpVec(lastDot, dot, t)
			table.insert(points, x)
			table.insert(points, y)
		end
		lastDot = dot
	end
	if #points < 4 then
		return
	end

	love.graphics.push()
	love.graphics.translate(x, y)

	local vs, idxs, mode = polyline("miter", points, love.graphics.getLineWidth()/2, 1, false)

	for i=1, #vs, 1 do
		local coef = ((i)/(#vs))
		coef = 1-coef
		coef = coef * coef
		vs[i][8] = vs[i][8] * coef
	end

	local mesh = love.graphics.newMesh(vs, mode)
	mesh:setVertexMap(idxs)
	love.graphics.draw(mesh)
	
	love.graphics.pop()
end

return trail
