local class = require 'gabe.class'
local animdata = require 'gfx.anim_data'
local lg = require 'love.graphics'

local anim = class(...)

anim.timer = love.timer.getTime

function anim:init(prefix, fname)
	self.fname = fname
	local key = prefix:gsub("^[^/]+/","")..fname
	local data = animdata[key]
	if not data then error(("sheet %q has no associated data"):format(key)) end

	local img = love.graphics.newImage(prefix.."sprites/"..fname)
	img:setFilter('nearest', 'nearest')
	if data.image then
		data.cellSize = {img:getDimensions()}
	end
	if not data.cellSize then error("missing cellSize") end
	if #data.cellSize < 2 then error("missing cellSize") end
	data.rows = data.rows or {}

	self.img = img

	self.dur = data.frameDuration or 96
	self.quads    = {}
	self.sequence = {}
	local cellw, cellh = unpack(data.cellSize)
	for y=0, img:getHeight()-1, cellh do
		local i = math.floor(y / cellh)+1
		self.quads[i] = {}
		self.sequence[i] = {}
		local idata = data.rows[i] or {}
		local lastFrame = idata.lastFrame or math.huge
		for x=0, img:getWidth()-1, cellw do
			if #self.quads[i] < lastFrame then
				local W, H = self.img:getDimensions()
				local q = lg.newQuad(x, y, cellw, cellh, W, H)
				table.insert(self.quads[i], q)
			end
		end
		if idata.sequence then
			error("TODO")
		else
			for qi=1, #self.quads[i] do
				self.sequence[i][qi] = qi
			end
		end
	end
	self.cellw, self.cellh = cellw, cellh
end

function anim:draw(i, ...)
	return self:drawFrame(i, self:frameFor(i), ...)
end

function anim:frameFor(i, offset, time)
	offset = offset or 0
	time = time or self.timer()
	assert(self.quads[i], i)
	local t = ((time+offset)*1000 / self.dur) % #self.quads[i]
	return math.floor(t)+1
end

function anim:drawFrame(i, t, ...)
	if not self.quads[i][t] then
		error(string.format("missing sprite [%d][%d]", i, t))
	end
	return love.graphics.draw(self.img, self.quads[i][t], ...)
end

function anim:getDimensions(i)
	return self.cellw, self.cellh
end

function anim:getRows()
	return #self.quads
end

local animbatch = class((...)..".batch")
function animbatch:init(anim, maxsprites, usage)
	self.anim = anim
	if usage then
		self.sb = love.graphics.newSpriteBatch(anim.img, maxsprites, usage)
		self.usage = usage
	elseif maxsprites then
		self.sb = love.graphics.newSpriteBatch(anim.img, maxsprites)
	else
		self.sb = love.graphics.newSpriteBatch(anim.img)
	end
end

function animbatch:enableAttributes(vertexformat)
	local fmt   = vertexformat
	local count = self.sb:getBufferSize()*4
	local usage = self.usage
	self.mesh = love.graphics.newMesh(fmt, count, nil, usage)
	self._attribute_indices = {}
	for i, attr in ipairs(fmt) do
		self.sb:attachAttribute(attr[1], self.mesh)
		self._attribute_indices[attr[1]] = i
	end
end

function animbatch:add(i, ...)
	return self:addFrame(i, self.anim:frameFor(i), ...)
end

function animbatch:addFrame(i, t, ...)
	if not (self.anim.quads[i] and self.anim.quads[i][t]) then
		error(string.format("missing sprite [%d][%d]", i, t))
	end
	local id = self.sb:add(self.anim.quads[i][t], ...)
	self.last_id = id
	return id
end

function animbatch:attribute(name, ...)
	local start  = (self.last_id-1)*4
	local attr_i = self._attribute_indices[name]

	if not attr_i then
		error("No such attribute: " .. tostring(name))
	end
	for i=0, 3 do
		self.mesh:setVertexAttribute(1+start+i, attr_i, ...)
	end
end

function animbatch:draw(...)
	love.graphics.draw(self.sb, ...)
	self.last_id = nil
end

local function make_proxy(name)
	animbatch[name] = function(self, ...)
		return self.sb[name](self.sb, ...)
	end
end

make_proxy"clear"

function anim:newBatch(maxsprites, usage)
	return animbatch.new(self, maxsprites, usage)
end

return anim
