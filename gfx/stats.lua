-- Memory visualizer to detect GC problems

local F = 60*10 -- frames to record
local W = F -- pixels
local H = 40 -- pixels
local TS = 1/60

local stats = {}

local time = {}
local time_points = {}
local time_i = 1
local mem   = {}
local mem_points = {}
local mem_i = 1
for i=1, F do
	time[i] = 0
	mem[i] = 0
	time_points[i] = 0
	mem_points[i]  = 0
end
for i=F+1,F+F do
	time_points[i] = 0
	mem_points[i]  = 0
end
local dt_max = 0
local m_max = 0

local last_sample = 0

function stats.draw()
	local tt = love.timer.getTime()
	if tt - last_sample > TS then
		last_sample = tt

		local dt = love.timer.getDelta()*1000
		dt_max = math.max(dt_max, dt)
		time_i = (time_i % F) + 1
		time[time_i] = dt

		local m = collectgarbage("count")/1024
		m_max = math.max(m_max, m)
		mem_i = (mem_i % F) + 1
		mem[mem_i] = m
	end

	local x = 0
	for i=time_i, time_i + F do
		i = (i % F) + 1
		local s = x*2+1
		time_points[s]  = x
		time_points[s+1] = H-(time[i]*H/dt_max)
		x = x + 1
	end

	x = 0
	for i=mem_i, mem_i + F do
		i = (i % F) + 1
		local s = x*2+1
		mem_points[s]  = x
		mem_points[s+1] = H-(mem[i]*H/m_max)
		x = x + 1
	end

	love.graphics.push()
	love.graphics.translate(4, 4)
	love.graphics.line(0, H, W, H, W, 0)
	if dt_max > 20 then
		love.graphics.setColor(255, 40, 40, 100)
		love.graphics.rectangle('fill', 0, 0, W, H*((dt_max-20)/dt_max))
		love.graphics.setColor(255, 255, 255, 255)
	end
	love.graphics.line(time_points)
	love.graphics.print(string.format("%.0f ms", dt_max), W+2, 2)
	local t = love.timer.getAverageDelta() * 1000
	love.graphics.print(string.format("%.0f ms", t), W+2, 14)

	love.graphics.translate(0, H)
	love.graphics.line(0, H, W, H, W, 0)
	love.graphics.line(mem_points)
	love.graphics.print(string.format("%.1fkb max", m_max), W+2, 2)
	love.graphics.print(string.format("%.1fkb", mem[mem_i]), W+2, 14)
	love.graphics.pop()
end

return stats
