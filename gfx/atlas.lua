local class = require 'gabe.class'
local json  = require 'dkjson'
-- FIXME: reload safety

local atlas = class(...)

-- atlas.json doesn't pass this to us so we just need to know that the inner
-- padding exists
atlas.padding = 6

local function make_spritemap(self, s)
	local data = assert(json.decode(s))
	local offsets = {}
	local map = self.map or {}
	for _, image in ipairs(data.frames) do
		assert(image.rotated == false)
		assert(image.trimmed == false)

		local path = image.filename
		-- tags r cool
		local name, tag, idx = path:match("^([^.]*)%.([^.]*)%.([^.]*)$")
		assert(name)
		assert(tag)
		assert(idx)
		local nametag = name
		if tag ~= "" then
			nametag = name.."."..tag
			if tonumber(idx) then
				idx = tonumber(idx)
				offsets[nametag] = offsets[nametag] or idx or 0
				idx = (idx - offsets[nametag])
			end
		end

		if idx == "" then
			idx = 0
		end
		idx = tonumber(idx)
		map[nametag] = map[nametag] or {}
		map[nametag][idx+1] = image

		local frame = image.frame
		local x, y, w, h = frame.x, frame.y, frame.w, frame.h
		local dimw, dimh = self.img:getDimensions()
		local pad = self.padding
		frame.quad = love.graphics.newQuad(x+pad, y+pad, w-pad, h-pad, dimw, dimh)
	end
	self.map = map
end

local sock = require 'socket'
local function get_data(rep, fname, s, img)
	local err
	if not s then
		s, err = love.filesystem.read(fname .. ".json")
	end
	if not s then
		sock.sleep(.001)
		if rep > 10 then error(err) end
		return get_data(rep+1, fname)
	end
	if not img then
		img, err = love.graphics.newImage(fname .. ".png")
	end
	if not img then
		sock.sleep(.001)
		if rep > 10 then error(err) end
		return get_data(rep+1, fname, s)
	end
	return s, img
end

---
-- Creates the atlas.
function atlas:init(fname, clock)
	self.clock = clock or love.timer.getTime
	self.ranges = {}
	self.last_frames = {}
	self.new_frames  = {}

	local s
	s, self.img = get_data(0, fname)
	make_spritemap(self, s)
	self.img:setFilter('nearest', 'nearest')
end

function atlas:flip()
	local last_f, new_f = #self.last_frames, #self.new_frames
	for i=1, math.max(last_f, new_f) do
		self.last_frames[i] = self.new_frames[i]
	end

	for i=1, new_f do
		self.new_frames[i] = nil
	end
end

---
-- Returns the metadata for the given sprite
-- Note that frame counts start at 1, even though the source json starts at 0
function atlas:about(sprite, frame)
	if frame == nil then
		frame = 1
	end
	return self.map[sprite] and self.map[sprite][frame]
end

local function cant_get(sprite, frame)
	if sprite == nil then
		return "sprite is nil"
	end
	if frame == nil then
		return string.format("no such sprite %q", sprite)
	end
	return string.format("no such sprite %q, frame %q", sprite, tostring(frame))
end

---
-- Returns the image/quad pair associated with the given sprite.
-- We return image first because that's how love.graphics.draw() takes it.
-- @returns image, quad
function atlas:get(sprite, frame)
	local r = self.map[sprite]
	if not r then
		error(cant_get(sprite), 2)
	end
	return self.img, self:getq(sprite, frame)
end

--- like get, but only the quad.
function atlas:getq(sprite, frame)
	local r = self.map[sprite]
	if not r then
		error(cant_get(sprite), 2)
	end
	if frame == nil then
		if r[2] then -- idle animation :O
			local last_f = self.last_frames[sprite] or 1
			local _, _, quad = self:get_anim(sprite, self.clock(), last_f)
			self.new_frames[sprite] = last_f
			return quad
		else -- static image
			frame = 1
		end
	end

	local Q = r[frame]
	assert(Q, cant_get(sprite, frame))
	assert(Q.frame.quad, "Missing quad")
	return Q.frame.quad
end

function atlas:get_anim(sprite, time, last_frame)
	local r = self.ranges[sprite]
	if r == nil then
		r = self:range(sprite, 1, self:len(sprite))
	end

	local frame_n = r.map(time, last_frame)
	local Q = self:about(sprite, frame_n)

	return frame_n, self.img, Q.frame.quad
end

---
-- returns the number of frames associated with sprite. static sprites have 1
-- frame. 0 frames means sprite does not exist.
function atlas:len(sprite)
	local frames = 1
	while self:about(sprite, frames) do
		frames = frames + 1
	end
	return frames - 1
end

---
-- Returns a list of quads for the range of sprites given. Memoized.
function atlas:range(sprite, s, e)
	local q = string.format("%s.%d.%d", sprite, s, e)
	if self.ranges[q] then
		return self.ranges[q]
	end

	local r = {}
	local timing   = {}
	local duration = 0
	for i=s, e do
		local Q = self:about(sprite, i)
		assert(Q, cant_get(sprite, i))
		assert(Q.frame.quad)
		table.insert(r, Q.frame.quad)
		table.insert(timing, duration)
		duration = duration + (Q.duration/1000)
	end

	r.map = function(time, last_frame)
		time = time % duration

		for i=2, #timing do
			if timing[i] > time then
				return i-1
			end
		end
		return #timing
	end
	self.ranges[q] = r
	if s == 1 and e == self:len(sprite) then
		self.ranges[sprite] = r
	end
	return r
end

return atlas
