local recolor = {}

function recolor.generate(image_name, palette)
	if type(palette) == "string" then
		local pimg = love.image.newImageData(palette)
		palette = {}
		local row = {}
		local colors = {}
		for x=0, pimg:getWidth()-1 do
			local color = {pimg:getPixel(x, 0)}

			local key = string.format("%d.%d.%d.%d", unpack(color))
			row[x+1] = key
			colors[key] = color
		end

		for y=1, pimg:getHeight()-1 do
			palette[y] = {}
			for x=0, pimg:getWidth()-1 do
				local kcolor = colors[row[x+1]]
				local vcolor = {pimg:getPixel(x, y)}
				palette[y][kcolor] = vcolor
			end
		end
	end

	local imgdata
	if type(image_name) == "string" then
		imgdata = love.image.newImageData(image_name)
	else
		imgdata = image_name:getData()
	end

	local pn = 0
	local pset = {}
	local setp = {}
	imgdata:mapPixel(function(x, y, r, g, b, a)
		local key = string.format("%d.%d.%d.%d", r, g, b, a)
		if not pset[key] then
			pset[key] = pn
			setp[pn] = {r, g, b, a}
			pn = pn + 1
		end
		return r, g, b, a
	end)

	imgdata:mapPixel(function(x, y, r, g, b, a)
		local key = string.format("%d.%d.%d.%d", r, g, b, a)
		local i = pset[key]
		assert(i)
		return ((i+.5)/pn)*255, 0, 0, a
	end)
	local image = love.graphics.newImage(imgdata)

	local palette_image = love.image.newImageData(pn, 1+#palette)
	local function threshold(a, b)
		local d = .1
		if a[4] and b[4] then
		return math.abs(a[1]-b[1]) < d and
		       math.abs(a[2]-b[2]) < d and
		       math.abs(a[3]-b[3]) < d and
		       math.abs(a[4]-b[4]) < d
	   else
		return math.abs(a[1]-b[1]) < d and
		       math.abs(a[2]-b[2]) < d and
		       math.abs(a[3]-b[3]) < d
	   end
	end
	local function get_color_for(y, color)
		assert(color)
		for kcolor, vcolor in pairs(palette[y]) do
		assert(kcolor)
			if threshold(kcolor, color) then
				return vcolor
			end
		end
		return nil
	end
	for i = 0, pn - 1 do
		local color = setp[i]
		palette_image:setPixel(i, 0, unpack(color))
		for y=1, #palette do
			local vcolor = get_color_for(y, color)
			if vcolor then
				palette_image:setPixel(i, y, unpack(vcolor))
			else
				palette_image:setPixel(i, y, unpack(color))
			end
		end
	end

	local pimg = love.graphics.newImage(palette_image)
	pimg:setFilter("nearest", "nearest")
	image:setFilter("nearest", "nearest")
	return image, pimg
end

local shad = [==[
#ifdef PIXEL
extern Image palette;
extern float scheme;

vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords)
{
	vec4 index = Texel(texture, texture_coords);
	vec4 o = Texel(palette, vec2(index.r, scheme));
	o = o * color;
#ifdef GREYSCALE
	o = o.ggga;
#endif
	return o;
}
#endif
]==]
local shad_grey = "#define GREYSCALE\n" .. shad

local shad2 = [==[ // vim: ft=glsl :
#ifdef VERTEX
attribute float colorscheme;
varying float scheme;
vec4 position(mat4 transform_projection, vec4 vertex_position)
{
	scheme = colorscheme;
	return transform_projection * vertex_position;
}
#endif

#ifdef PIXEL
extern Image palette;
varying float scheme;

vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords)
{
	vec4 index = Texel(texture, texture_coords);
	vec4 o = Texel(palette, vec2(index.r, scheme));
	return o * color;
}
#endif
]==]

function recolor.load()
	if type(shad) == "string" then
		shad  = love.graphics.newShader(shad)
		shad2 = love.graphics.newShader(shad2)
		shad_grey = love.graphics.newShader(shad_grey)
	end
end

function recolor.apply(palette_image, scheme_n)
	shad:send("palette", palette_image)
	shad:send("scheme",  (scheme_n+1)/palette_image:getHeight())
	love.graphics.setShader(shad)
end

function recolor.apply_grey(palette_image, scheme_n)
	shad_grey:send("palette", palette_image)
	shad_grey:send("scheme",  (scheme_n+1)/palette_image:getHeight())
	love.graphics.setShader(shad_grey)
end

-- return a colorscheme number, normalized
function recolor.texy(palette_image, scheme_n)
	return (scheme_n+1)/palette_image:getHeight()
end

function recolor.applyBatched(palette_image)
	shad2:send("palette", palette_image)
	love.graphics.setShader(shad2)
end

return recolor
