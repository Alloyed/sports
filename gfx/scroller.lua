-- A "camera controller"
-- declaratively defines how a camera follows a point or a set of points

local scroller = {}
local scroller_mt = {__index = scroller}

function scroller.new(init_x, init_y)
	local self = setmetatable({}, scroller_mt)
	self._x, self._y = init_x or 0, init_y or 0
	self._mixer = {x = "average", y = "average"}
	self._args  = {x = {}, y = {}}
	return self
end

local n = {x = 1, y = 2}
local function getAxis(point, axis)
	if type(point) == "function" then
		return (select(n[axis], point()))
	elseif point[axis] then
		return point[axis]
	elseif getmetatable(point).__call then
		return (select(n[axis], point()))
	end
end

function scroller:setPoint(name, point)
	self._points[name] = point
end

function scroller:getPoint(name)
	return self._points[name]
end

function scroller:setAxis(axis, name)
	if axis == "xy" then
		self:setAxis("x", name)
		return self:setAxis("y", name)
	end
	self._mixer[axis] = name
end

local p = {}

function p.smooth(x, _, old, dt)
	return old + (x-old) * .5 * dt
end

p.floor = math.floor

function p.window(x)
end

function p.clamp(x, axis)
end

local m = {}

function m.average(scroller, axis)
	local sum, len = 0, 0
	for _, point in pairs(self._points) do
		local i = getAxis(point, axis)
		sum = sum + i
		len = len + 1
	end
	if len == 0 then return 0 end
	return sum / len
end

function m.lock(scroller, axis)
	return self.args[axis][1]
end

function scroller:update(dt)
	self._x = m[self._mixer.x](self, "x")
	self._y = m[self._mixer.y](self, "y")
end

function scroller:debugDraw()
	for _, point in pairs(self._points) do
		local px, py = getPoint(point)
		love.graphics.circle('line', px, py, 4)
	end
	local x, y = self._x, self._y
	love.graphics.line(x-3, y, x+3, y)
	love.graphics.line(x, y-3, x, y+3)
end

return scroller
