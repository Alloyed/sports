--- Fixed-resolution letterboxing.
-- possible new features:
--  mixel-mode: basically, instead of canvases, use the scale function and draw
--  black rectangles on top.
--
--  subpixel camera movement. Render on a canvas slightly larger than the
--  target, then move the canvas after rendering and scissor
--
local class = require 'gabe.class'

local lbox = class(...)

function lbox:init(layers, w, h, perfect)
	self.layerN = layers
	self.target_w, self.target_h = w, h
	self.perfect = perfect
	self:recalculate()
end

function lbox:recalculate()
	local w, h = self.target_w, self.target_h
	self.out_w, self.out_h = love.graphics.getDimensions()

	local ws, hs = self.out_w / w, self.out_h / h
	local scale = math.min(ws, hs)
	if self.force_scale then
		self.force_scale = math.min(10, math.max(.25, self.force_scale))
		scale = self.force_scale
	end

	w = w
	local cv1 = self.cv1 and self.cv1[1]
	if (not cv1) or (w ~= cv1:getWidth()) or (h ~= cv1:getHeight()) then
		self.cv1 = {}
		for i=1, self.layerN do
			self.cv1[i] = love.graphics.newCanvas(w, h)
			self.cv1[i]:setFilter('nearest', 'nearest')
		end
	end

	self.iscale = math.ceil(scale)
	if not self.perfect then
		local w2, h2 = w*self.iscale, h*self.iscale
		local cv2 = self.cv2 and self.cv2[1]
		if (not cv2) or (w2 ~= cv2:getWidth()) or (h2 ~= cv2:getHeight()) then
			self.cv2 = {}
			for i=1, self.layerN do
				self.cv2[i] = love.graphics.newCanvas(w2, h2)
				self.cv2[i]:setFilter('linear', 'linear')
			end
		end
	else
		scale = math.floor(scale)
	end
	self.scale = scale


	self.real_w, self.real_h = self.target_w*scale, self.target_h*scale
	self.ox = (self.out_w * .5) - (self.real_w * .5)
	self.oy = (self.out_h * .5) - (self.real_h * .5)
	self.subx, self.suby = 0, 0
end

function lbox:render(layer, renderfn)
	--love.graphics.clear(0, 0, 0)

	love.graphics.push('all')
	love.graphics.setCanvas(self.cv1[layer])
	renderfn()
	love.graphics.pop()

	if not self.perfect then
		love.graphics.push('all')
		love.graphics.setCanvas(self.cv2[layer])
		love.graphics.clear(0, 0, 0, 0)
		local s = self.iscale
		love.graphics.draw(self.cv1[layer], 0, 0, 0, s, s)
		love.graphics.pop()
	end
end

function lbox:draw()
	local scale, cv
	if self.scale < 1 then
		local msg = "Sorry, the screen is too small. try at least %dx%d"
		msg = msg:format(self:dimensions())
		love.graphics.setColor(255, 255, 255)
		love.graphics.printf(msg, 0, (self.out_h*.5)-10, self.out_w, 'center')
		return
	end
	if self.perfect then
		-- nearest upscale
		scale = self.scale
		cv = self.cv1
	else
		-- nearest upscale, then linear downscale
		scale = self.scale / self.iscale
		cv = self.cv2
	end

	for i=1, self.layerN do
		local x, y = self.ox+math.floor(self.subx*scale), self.oy+math.floor(self.suby*scale)
		love.graphics.setScissor(self.ox, self.oy, self.real_w, self.real_h)
		love.graphics.draw(cv[i], math.floor(x-math.floor(0*scale)), math.floor(y), 0, scale, scale)
		love.graphics.setScissor()
	end
end

--- Convert native window coordinates (what love provides) into virtual screen
--  coordinates.
function lbox:win2screen(x, y)
	local scale = self.scale
	return math.floor((x-self.ox) / scale), math.floor((y-self.oy) / scale)
end

function lbox:screen2win(x, y)
	local scale = self.scale
	return (x*scale) + self.ox, (y*scale) + self.oy
end

function lbox:mouse()
	return self:win2screen(love.mouse.getPosition())
end

function lbox:dimensions()
	return self.target_w, self.target_h
end

return lbox
