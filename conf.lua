if love.filesystem then
	require 'rocks' ()
end

function love.conf(t)
	t.identity = "sports"
	t.window.title = "SPORTS"

	t.window = false

	if love.filesystem then
		local is_server = false
		for _, a in ipairs(arg) do
			if a == "--server" then
				is_server = true
				A = A or {}
				A.is_server = true
				break
			end
		end
		if is_server then
			t.window = false
			t.modules.window = false
			t.modules.graphics = false
			t.modules.audio = false
		end
	end

	t.version = "0.10.2"
	t.rocks_servers = {
		--"file:///home/kyle/src/love-libs/loverocks-repo/build",
		"http://alloyed.me/shared/rocks",
		"http://luarocks.org/dev",
	}
	t.dependencies = {
		"dkjson ~> 2",
		"bump ~> 3.1",
		"fun-alloyed ~> 0.1",
		"love-imgui ~> 0.7",
		-- wall of shame
		"gabe ~> scm",
		"repler ~> scm",
		"flux ~> scm", -- has versions but not tags
		"bitser-alloyed ~> scm",
		"splash ~> scm",
		"sti ~> scm",
		"suit ~> scm",
		"baton ~> scm",
		-- not currently used
		--"readline",
		--"gifcat-alloyed ~> scm",
	}
	t.releases = {
		title = "Sports",         -- The project title (string)
		package = "sports",       -- The project command and package name (string)
		loveVersion = "0.10.2",   -- The project LÖVE version
		version = "dd15",         -- The project version
		author = nil,             -- Your name (string)
		email = nil,              -- Your email (string)
		description = nil,        -- The project description (string)
		homepage = nil,           -- The project homepage (string)
		identifier = nil,         -- The project Uniform Type Identifier (string)
		excludeFileList = {"wav$", "^assets/unused", "sh$", "fish$", "so$", ".love$", ".zip$"},
		releaseDirectory = "release",
	}
end
