local class  = require 'gabe.class'
local suit_gp  = require 'suit_gamepad'
--local reload = require 'gabe.reload'

local gamestate = {}

function gamestate.exit()
	if S.gamestate.exit then
		S.gamestate:exit()
	end
end

function gamestate.switch(name, ...)
	log.verbose(function() return "switching to %q", name end)
	gamestate.exit()
	suit_gp.init()
	local klass = require(name)
	if A.loaded ~= true and (name:match("^gs.game") or klass.loadme) then
		-- FIXME: loading
		if not A.is_server and klass.load then klass.load() end
		S.gamestate = klass.new(S.gamestate, ...)
		S.gamestate.name = name
	else
		if not A.is_server and klass.load then klass.load() end
		S.gamestate = klass.new(S.gamestate, ...)
		S.gamestate.name = name
	end

	if S.gamestate.enter then
		S.gamestate:enter()
	end

	if A.client then
		A.client:msg("updateView", name)
	end
end

function gamestate.switch_obj(obj)
	if obj.enter then
		obj:enter(S.gamestate)
	end
	S.gamestate = obj

	if A.client then
		A.client:msg("updateView", S.gamestate.name)
	end
end

function gamestate.xfer(xfer_name, name, ...)
	log.verbose(function() return "switching to %q", name end)
	gamestate.exit()
	local dest = {name, ...}

	local xfer_class = require(xfer_name)
	if not A.is_server and xfer_class.load then xfer_class.load() end
	local xfer = xfer_class.new(S.gamestate, dest)
	xfer.name = xfer_name

	S.gamestate = xfer

	if A.client then
		A.client:msg("updateView", name)
	end
end

function gamestate.reset(name, ...)
	suit_gp.init()
	local gs = require(name)
	if gs.load then gs.load() end
	S.gamestate = gs.new(...)
	S.gamestate.name = name

	if S.gamestate.enter then
		S.gamestate:enter()
	end
	if A.client then
		A.client:msg("updateView", name)
	end
end

function gamestate.call(name, ...)
	if S.gamestate[name] then
		return S.gamestate[name](S.gamestate, ...)
	end
end

function gamestate.inject(name, input_m)
	love[name] = function(...)
		if input_m then S.input_method = input_m end
		return gamestate.call(name, ...)
	end
end

function gamestate.refresh()
	local r = require(class.xtype(S.gamestate))
	if r.load then r.load() end
end

return gamestate
