-- ASSet MANager.
local atlas  = require 'gfx.atlas'
local anim   = require 'gfx.anim'
local tiled  = require 'sti'

local assman = {}
local loaders = {}

function loaders.image(prefix, name)
	local fname = prefix .. name:gsub("%.","/") .. ".png"
	local img = love.graphics.newImage(fname)
	img:setFilter("nearest", "nearest")
	return img, fname
end

function loaders.anim(prefix, name)
	local a = anim.new(prefix, name..".png")
	return a, a.fname
	--local fname = prefix .. name:gsub("%.","/") .. ".png"
	--local img = love.graphics.newImage(fname)
	--img:setFilter("nearest", "nearest")
	--return img, fname
end

function loaders.atlas(prefix, name)
	return atlas.new(prefix .. name)
end

function loaders.sound(prefix, name)
	local fname = prefix .. name .. ".ogg"
	return love.audio.newSource(fname, 'static'), fname
end

function loaders.song(prefix, name)
	local fname = prefix .. name .. ".ogg"
	return love.audio.newSource(fname, 'stream'), fname
end

function loaders.map(prefix, name)
	local fname = prefix .. name .. ".lua"
	return tiled(fname), fname
end

function loaders.font(prefix, name)
	local size = tonumber(name:match("_(%d+)$"))
	if size then
		name = name:gsub("_%d+$", "")
	end
	local fname = prefix .. name .. ".ttf"
	local data = assman._font_data[fname]
	if not data then
		data = love.filesystem.newFileData(fname)
		assman._font_data[fname] = data
	end

	return love.graphics.newFont(data, size), fname
end

function loaders.bmfont(prefix, name)
	local fname = prefix .. name .. ".fnt"

	local data = assman._font_data[fname]
	if not data then
		data = love.filesystem.newFileData(fname)
		assman._font_data[fname] = data
	end

	local f = love.graphics.newFont(data)
	f:setFilter('nearest', 'nearest')

	return f, fname
end

function loaders.imgfont(prefix, name, glyphs, spacing)
	local fname = prefix .. name .. ".png"

	local f = love.graphics.newImageFont(fname, glyphs, spacing)
	f:setFilter('nearest', 'nearest')

	return f, fname
end

function loaders.shader(prefix, name)
	local mod = "shaders." .. name
	local code = require(mod)

	return love.graphics.newShader(code), mod
end

local reloaders = {}

--function reloaders.atlas(a)
--	a:reload()
--end

function reloaders.sound(a)
	a:stop()
end

function reloaders.song(a)
	a:stop()
end

assman.prefix = "assets/"

function assman.init(tbl)
	assman.loaded = tbl or {}
	assman.howto  = assman.howto or {}
	assman.meta   = assman.meta or {}
	assman._font_data = {}
end

local function splitName(name)
	if name:find(":") then
		local prefix, rname = name:match("^([^.]+):([^.]+)$")
		assert(prefix)
		assert(rname)
		return prefix, rname
	end
	return nil
end

assman.load = setmetatable({}, {__index = function(t, class)
	local function loader(name, ...)

		local argn = select('#', ...)
		local args = {...}
		assman.howto[name] = {class = class, args = args}

		local cb
		if argn ~= 0 then
			cb = select(argn, ...)
		end
		if type(cb) == 'function' then -- TODO: callable?
			table.remove(args)
		else
			cb = nil
		end

		local a, n, k, pfix = assman.loaded, name, name, assman.prefix
		local pre, name2 = splitName(name)
		if pre then
			a = assman.loaded[pre] or {}
			assman.loaded[pre] = a
			n = name2
			k = name2:gsub("^.*/", "")
			pfix = assman.prefix .. pre .. "/"
		end

		if not a[k] then
			local out, fname = loaders[class](pfix, n, unpack(args))
			if cb then cb(out, unpack(args)) end
			a[k] = out
			if fname then
				assman.meta[name] = {
					fname = fname,
					modtime = love.filesystem.getLastModified(fname)
				}
			end
		end
	end
	rawset(t, class, loader)
	return loader
end})

--- Reloads an existing asset, using the same strategy as the most recently
--  registered load
function assman.reload(name)
	local howto = assman.howto[name]
	assert(howto, name .. " has not been previously loaded")
	if reloaders[howto.class] then
		reloaders[howto.class](assman.loaded[name])
	end
	assman.loaded[name] = nil

	return assman.load[howto.class](name, unpack(howto.args))
end

function assman.reload_all()
	for name, _ in pairs(assman.howto) do
		assman.reload(name)
	end
end

-- Has all loading completed?
function assman.completed()
	return true
end

return assman
