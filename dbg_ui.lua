-- luacheck: ignore IMGUI_VIEWME
return function()
	local ok, imgui = pcall(require, 'imgui')
	if not ok then return end
	local class = require 'gabe.class'

	_G.IMGUI_VIEWME  = _G.IMGUI_VIEWME or setmetatable({}, {__mode = 'k'})
	local viewme = IMGUI_VIEWME

	local _draw   = love.draw or function() end
	local prop_flags = {"AlwaysAutoResize", "NoCollapse"}
	function love.draw()
		_draw()
		for t, shape in pairs(viewme) do
			local id = string.match(tostring(t), "(0x[^>]+)$")
			local win = string.format("%s:<%s>", class.xtype(t) or "tbl", id or t)

			local isVisible, isOpen = imgui.Begin(win, true, prop_flags)
			if isVisible then
				if type(shape) == "table" then
					imgui.Table(t, shape, nil, true)
				else
					imgui.Table(t, nil, nil, true)
				end
			end

			if isOpen == false then
				viewme[t] = nil
			end
			imgui.End()
		end
		imgui.Render()
		--imgui.NewFrame()
	end
	imgui.NewFrame()

	local _textinput = love.textinput
	function love.textinput(t, ...)
		imgui.TextInput(t)
		if not imgui.GetWantCaptureKeyboard() then
			if _textinput then
				return _textinput(t, ...)
			end
		end
	end

	local _keypressed = love.keypressed
	function love.keypressed(t, ...)
		imgui.KeyPressed(t)
		if not imgui.GetWantCaptureKeyboard() then
			if _keypressed then
				return _keypressed(t, ...)
			end
		end
	end

	local _keyreleased = love.keyreleased
	function love.keyreleased(t, ...)
		imgui.KeyReleased(t)
		if not imgui.GetWantCaptureKeyboard() then
			if _keyreleased then
				return _keyreleased(t, ...)
			end
		end
	end

	local _mousemoved = love.mousemoved
	function love.mousemoved(x, y, ...)
		imgui.MouseMoved(x, y)
		if not imgui.GetWantCaptureMouse() then
			if _mousemoved then
				return _mousemoved(x, y, ...)
			end
		end
	end

	local _mousepressed = love.mousepressed
	function love.mousepressed(x, y, b, ...)
		imgui.MousePressed(b)
		if not imgui.GetWantCaptureMouse() then
			if _mousepressed then
				return _mousepressed(x, y, b, ...)
			end
		end
	end

	local _mousereleased = love.mousereleased
	function love.mousereleased(x, y, b, ...)
		imgui.MouseReleased(b)
		if not imgui.GetWantCaptureMouse() then
			if _mousereleased then
				return _mousereleased(x, y, b, ...)
			end
		end
	end

	local _wheelmoved = love.wheelmoved
	function love.wheelmoved(x, y, ...)
		imgui.WheelMoved(y)
		if not imgui.GetWantCaptureMouse() then
			if _wheelmoved then
				return _wheelmoved(x, y, ...)
			end
		end
	end


	imgui.TableHandlers = {}
	function imgui.TableHandlers.default(tbl, k)
		local val = tbl[k]
		local key
		if type(k) == "string" then
			key = string.format("%q", k)
		else
			key = tostring(k)
		end

		local val_type = type(val)
		local _
		if val_type == "number" then
			_, tbl[k] = imgui.DragFloat(key, val)
		elseif val_type == "boolean" then
			_, tbl[k] = imgui.Checkbox(key, val)
		elseif val_type == "string" then
			_, tbl[k] = imgui.InputText(key, val, 255)
		elseif val_type == "table" and val.x and val.y and val.z then
			if type(val.x) ~= 'number' or
				type(val.y) ~= 'number' or
				type(val.z) ~= 'number' then
				error(k)
			end
			_, val.x, val.y, val.z = imgui.DragFloat3(key, val.x, val.y, val.z)
		elseif val_type == "table" and val.x and val.y  then
			_, val.x, val.y = imgui.DragFloat2(key, val.x, val.y)
		elseif val_type == "userdata" and val.typeOf and val:typeOf("ParticleSystem") then
			imgui.TableHandlers.psystem(tbl, key)
		elseif val_type == "table" then
			if imgui.Button(tostring(val)) then
				imgui.Inspect(val)
			end
			imgui.SameLine()
			imgui.Text(key)
		else
			imgui.LabelText(key, tostring(val))
		end
	end

	function imgui.TableHandlers.text(tbl, key)
		imgui.LabelText(tostring(key), tostring(tbl[key]))
	end

	function imgui.TableHandlers.color3(tbl, key)
		local _, r, g, b = imgui.ColorEdit3(key, unpack(tbl[key]))
		tbl[key][1], tbl[key][2], tbl[key][3] = r, g, b
	end

	function imgui.TableHandlers.color4(tbl, key)
		local _
		local r, g, b, a = unpack(tbl[key])
		a = a or 255
		r, g, b, a = r/255, g/255, b/255, a/255
		_, r, g, b, a = imgui.ColorEdit4(key, r, g, b, a)
		r, g, b, a = r*255, g*255, b*255, a*255
		tbl[key][1], tbl[key][2], tbl[key][3], tbl[key][4] = r, g, b, a
	end

	function imgui.TableHandlers.angle(tbl, key)
		local _
		_, tbl[key] = imgui.SliderAngle(key, tbl[key])
	end

	function imgui.TableHandlers.psystem(tbl, key)
		local ps = tbl[key]
		if imgui.TreeNode(key .. " (ParticleSystem)") then
			local changed, _, a, b

			changed, a = imgui.DragInt("bufferSize", ps:getBufferSize(), 1, 1)
			if changed then
				a = math.max(1, a)
				ps:setBufferSize(a)
			end

			a, b = ps:getParticleLifetime()
			changed, a, b = imgui.DragFloat2("particleLifetime", a, b, .1)
			if changed then
				if a > b then
					a, b = b, a
				end
				ps:setParticleLifetime(a, b)
			end

			changed, a, b = imgui.DragFloat2("position", ps:getPosition())
			if changed then
				ps:setPosition(a, b)
			end

			changed, a, b = imgui.DragFloat2("speed", ps:getSpeed())
			if changed then
				ps:setSpeed(a, b)
			end

			changed, a = imgui.SliderAngle("direction", ps:getDirection())
			if changed then
				ps:setDirection(a)
			end

			local sizes = {ps:getSizes()}
			local newSizes = {unpack(sizes)}

			if imgui.SmallButton("-") and #newSizes > 1 then
				newSizes[#newSizes] = nil
			end
			imgui.SameLine()
			if imgui.SmallButton("+") and #newSizes < 8 then
				newSizes[#newSizes+1] = newSizes[#newSizes]
			end

			for i, s in ipairs(newSizes) do
				_, a = imgui.DragFloat("size["..i.."]", s, .01)
				newSizes[i] = a
			end
			ps:setSizes(unpack(newSizes))

			changed, a = imgui.SliderFloat("sizeVariation", ps:getSizeVariation(), 0, 1)
			if changed then
				ps:setSizeVariation(a)
			end

			a, b = ps:getSpin()
			a, b = math.deg(a), math.deg(b)
			changed, a, b = imgui.DragFloat2("spin", a, b)
			if changed then
				if a > b then
					a, b = b, a
				end
				ps:setSpin(math.rad(a), math.rad(b))
			end

			changed, a = imgui.SliderFloat("spinVariation", ps:getSpinVariation(), 0, 1)
			if changed then
				ps:setSpinVariation(a)
			end

			changed, a = imgui.SliderAngle("spread", ps:getSpread())
			if changed then
				ps:setSpread(a)
			end

			local modes = {"top", "bottom", "random"}
			a = ps:getInsertMode()
			local i = 1
			for _i, v in ipairs(modes) do
				if a == v then
					i = _i
					break
				end
			end
			changed, a = imgui.Combo("insertMode", i, modes, #modes)
			if changed then
				ps:setInsertMode(modes[a])
			end
			imgui.TreePop()
		end
	end

	function imgui.Table(t, shape, id, canClose)
		local keys = {}
		local isVec = t.x and t.y
		local isVec3 = t.x and t.y and t.z
		if shape then
			for k, _ in pairs(shape) do
				table.insert(keys, k)
			end
		else
			for k, _ in pairs(t) do
				if (not isVec) or not (k == "x" or k == "y" or k == "z") then
					table.insert(keys, k)
				end
			end
		end
		table.sort(keys, function(a, b)
			return tostring(a) < tostring(b)
		end)

		local _
		if isVec3 then
			_, t.x, t.y, t.z = imgui.DragFloat3("<vec>", t.x, t.y, t.z)
		elseif isVec then
			_, t.x, t.y = imgui.DragFloat2("<vec>", t.x, t.y)
		end
		for _, k in ipairs(keys) do
			if shape and shape[k] then
				if imgui.TableHandlers[shape[k]] then
					-- string handler
					imgui.TableHandlers[shape[k]](t, k)
				elseif shape[k] == true then
					imgui.TableHandlers.default(t, k)
				else
					-- custom handler
					shape[k](t, k)
				end
			else
				imgui.TableHandlers.default(t, k)
			end
		end
	end

	-- restricted variant of imgui.Table. only pick the keys you care about
	function imgui.Tweak(t, ...)
		local N = select('#', ...)
		if N == 0 then imgui.Table(t) end
		--local id = (class.xtype(t) or "") ..":".. tostring(t)
		for i=1, N do
			local k = select(i, ...)
			imgui.TableHandlers.default(t, k)
		end
	end

	-- use for debugging: adds a table to an "inspect" list, that will stay
	-- open until manually closed
	function imgui.Inspect(t, shape)
		shape = shape or true
		viewme[t] = shape
	end


	local cc_data = {}
	-- map a float value to a midiCC controller
	function imgui.CCFloat(name, CC, min, max, start)
		local changed, out
		local data = cc_data[CC] or {real = start, focus = "real"}
		cc_data[CC] = data
		local input = data[data.focus]
		if data.focus == "cc" then
			input = (input/127) * (max-min) + min
		end

		changed, out = imgui.SliderFloat(name, input, min, max)
		if changed then data.focus = "real" end
		data.real = out
		return out
	end

	local midi_in
	function imgui.pollCCs()
		if midi_in == nil then
			local midi = require'rtmidi'
			midi_in = midi.openin(1)
		end
		local a, b, c, _ = midi_in:getMessage()
		while a do
			if a == 176 then -- CC
				if cc_data[b] then
					cc_data[b].cc = c
					cc_data[b].focus = "cc"
				end
			end
			a, b, c, _ = midi_in:getMessage()
		end
	end

	-- wrapper for Begin/End
	function imgui.Window(id, hidden, flags, fn)
		if flags == nil and fn == nil and type(hidden) == 'function' then
			fn = hidden
			hidden, flags = nil, nil
		end
		local view = imgui.Begin(id, hidden, flags)
		if view then
			fn()
		end
		imgui.End()
	end

	local pos_data = {}
	function imgui.Position(name, dx, dy)
		local _
		local data = pos_data[name] or {x = dx or 0, y = dy or 0}
		_, data.x, data.y = imgui.DragFloat2(name, data.x, data.y)
		pos_data[name] = data
		return data.x, data.y
	end

	local float_data = {}
	function imgui.Float(name, _f, ...)
		local data = float_data[name] or {f = _f or 0}
		local _
		_, data.f = imgui.DragFloat(name, data.f, ...)
		float_data[name] = data
		return data.f
	end
end
