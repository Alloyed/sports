return function()
	local reload = require 'gabe.reload'
	reload.add_modules("patch", "bitser", "assman", "gifcat", "flux")
	_G.log = require 'log'
	local class = require 'gabe.class'
	function class.subclass(parent, name)
		if type(parent) ~= "string" then
			parent = class.xtype(parent)
		end
		return class.class(parent.."."..name)
	end
end
