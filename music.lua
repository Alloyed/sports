-- magical musical switcheroo
local class = require 'gabe.class'
local assman = require 'assman'
local flux = require 'flux'

local music = class(...)

function music:init()
	self.current = false
	self.songs = {}
	self.flux  = flux.group()
	self.mute = false
end

function music:play(songname)
	log.verbose("play %s", songname)
	if not self.songs[songname] then
		log:error("song %q not loaded!", songname)
	end

	if self.mute then return end

	if self.current ~= songname then
		for _, song in pairs(self.songs) do
			song:stop()
		end
		self.songs[songname]:play(.1)
		self.current = songname
	end
end

function music:fadein(songname)
	if not self.songs[songname] then
		error(songname .. " not loaded!")
	end
	if self.mute then return end

	if self.current ~= songname then
		log.verbose("fadein %s", songname)
		for name, song in pairs(self.songs) do
			if name ~= self.current then
				song:stop()
			end
		end
		self.songs[songname]:setVolume(0)
		self.songs[songname]:play()
		self.vol = 0
		local last = self.current
		self.flux:to(self, 3, {vol = .1}):onupdate(function()
			if self.current then
				self.songs[self.current]:setVolume(self.vol)
			end
			if last then
				self.songs[last]:setVolume(.1 - self.vol)
			end
		end):oncomplete(function()
			if last then
				self.songs[last]:stop()
				self.songs[last]:setVolume(.1)
			end
		end)
		self.current = songname
	end
end

function music:setMute(m)
	self.mute = m
	self.flux = nil
	self.flux = flux.group()
	if m then
		for _, song in pairs(self.songs) do
			song:setVolume(0)
			song:pause()
		end
	else
		for k, song in pairs(self.songs) do
			song:setVolume(.1)
			if k == self.current then
				song:play()
			end
		end
	end
end

function music:update(dt)
	self.flux:update(dt)
end

function music:pause()
	if self.songs[self.current] then
		self.songs[self.current]:pause()
	end
end

function music:stop()
	if self.songs[self.current] then
		self.songs[self.current]:stop()
		self.current = false
	end
end

function music:load(songname)
	assman.load.song("music/"..songname)
	self.songs[songname] = A["music/"..songname]
	self.songs[songname]:setVolume(.1)
end

return music
