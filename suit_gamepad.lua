local suit = require 'suit'
local vec  = require 'vec'
local misc = require 'misc'

-- FIXME: we are blowing out the state somehow when this is a local
-- stop that
_G.suit_gp = _G.suit_gp or {
	focused = false,
	pointer = vec.new(1, 1),
	cursor  = vec.new(0, 0),
	indexes = {},
	last_input = {down=nil, up=nil},
	last_mx = love.mouse.getX(),
	last_my = love.mouse.getY(),
}
local suit_gp = _G.suit_gp

function suit_gp.isPressed()
	if love.keyboard.isDown'x' then return true end

	for _, gp in ipairs(love.joystick.getJoysticks()) do
		if gp:isGamepad() and gp:isGamepadDown('a') then
			return true
		end
	end
	return false
end

function suit_gp.isEscape()
	if love.keyboard.isDown'z' then return true end

	for _, gp in ipairs(love.joystick.getJoysticks()) do
		if gp:isGamepad() and gp:isGamepadDown('b') then
			return true
		end
	end
	return false
end

function suit_gp.pollInput()
	local input = {}
	input.down  = love.keyboard.isDown("down")
	input.up    = love.keyboard.isDown("up")
	input.left  = love.keyboard.isDown("left")
	input.right = love.keyboard.isDown("right")


	for _, gp in ipairs(love.joystick.getJoysticks()) do
		if gp:isGamepad() then
			if gp:isGamepadDown("dpup")   then input.up = true end
			if gp:isGamepadDown("dpdown") then input.down = true end
			if gp:isGamepadDown("dpleft")   then input.left = true end
			if gp:isGamepadDown("dpright") then input.right = true end

			local axisY = gp:getGamepadAxis("lefty")
			if axisY < -.5 then input.up = true end
			if axisY > .5 then input.down = true end

			local axisX = gp:getGamepadAxis("leftx")
			if axisX < -.5 then input.left = true end
			if axisX > .5 then input.right = true end
		end
	end

	local old = suit_gp.last_input
	if not suit_gp.isPressed() then
		if input.down then
			if old.down and old.down % 10 == 1 then
				suit_gp.pointer:add_(0, 1)
			end
		end

		if input.up then
			if old.up and old.up % 10 == 1 then
				suit_gp.pointer:add_(0, -1)
			end
		end
	else
		if input.left then
			if old.left and old.left % 2 == 1 then
				suit_gp.pointer:add_(-.01, 0)
			end
		end

		if input.right then
			if old.right and old.right % 2 == 1 then
				suit_gp.pointer:add_(.01, 0)
			end
		end
	end

	if input.up == false and
	   input.down == false and
	   input.left == false and
	   input.right == false then
		suit_gp.released = true
	else
		suit_gp.released = false
	end

	for k, v in pairs(input) do
		if v then
			if old[k] then
				old[k] = old[k] + 1
			else
				old[k] = 1
			end
		else
			old[k] = false
		end
	end
end

function suit_gp.getPointerPos()
	local idx_x, idx_y = suit_gp.pointer:map(math.floor)
	idx_x = misc.clamp(idx_x, 1, #suit_gp.indexes)

	if not suit_gp.indexes[idx_x] then return 0, 0 end
	idx_y = misc.clamp(idx_y, 1, #(suit_gp.indexes[idx_x]))
	if not suit_gp.indexes[idx_x][idx_y] then return 0, 0 end
	local box = suit_gp.indexes[idx_x][idx_y]

	if suit_gp.released then
		local val = box.val or .5
		suit_gp.pointer:set_(idx_x+val, idx_y)
	end

	local val = suit_gp.pointer.x - math.floor(suit_gp.pointer.x)

	return box.x+(box.w*val), box.y+(box.h*.5)
end

function suit_gp.update()
	suit_gp.pollInput()
	if S.input_method ~= "mouse" then
		local x, y = suit_gp.getPointerPos()
		local m1 = suit_gp.isPressed()
		suit_gp.s:updateMouse(x, y, m1)
		return true
	else
		local x, y = love.mouse.getPosition()
		if A.lbox then
			x, y = A.lbox:mouse()
		end
		suit_gp.s:updateMouse(x, y, love.mouse.isDown(1))
		return false
	end
end

function suit_gp.init()
	suit_gp.s = suit.new()
	suit_gp.layout = suit_gp.s.layout
end

function suit_gp.reset(...)
	suit_gp.layout:reset(...)
	suit_gp.cursor:set_(1, 1)
end

function suit_gp.row(_w, _h, val)
	local x, y, w, h = suit_gp.layout:row(_w, _h)

	local c = suit_gp.cursor
	suit_gp.indexes[c.x]      = suit_gp.indexes[c.x] or {[c.x] = {}}
	suit_gp.indexes[c.x][c.y] = suit_gp.indexes[c.x][c.y] or {}
	local p = suit_gp.indexes[c.x][c.y]
	p.x, p.y, p.w, p.h = x, y, w, h
	p.val = val
	c.y = c.y + 1

	return x, y, w, h
end

function suit_gp.col(...)
	local x, y, w, h = suit_gp.layout:row(...)

	local c = suit_gp.cursor
	suit_gp.indexes[c.x]      = suit_gp.indexes[c.x] or {[c.x] = {}}
	suit_gp.indexes[c.x][c.y] = suit_gp.indexes[c.x][c.y] or {}
	local p = suit_gp.indexes[c.x][c.y]
	p.x, p.y, p.w, p.h = x, y, w, h
	c.x = c.x + 1

	return x, y, w, h
end

function suit.theme.Button(text, opt, x,y,w,h)
	local c = suit.theme.getColorForState(opt)

	suit.theme.drawBox(x,y,w,h, c, opt.cornerRadius)
	love.graphics.setColor(c.fg)
	love.graphics.setFont(opt.font)

	if S.input_method ~= "mouse" and opt.state == "hovered" then
		love.graphics.print(">", x-6, y)
	end

	y = y + suit.theme.getVerticalOffsetForAlign(opt.valign, opt.font, h)
	love.graphics.printf(text, x+2, y, w-4, opt.align or "center")
end

return suit_gp
