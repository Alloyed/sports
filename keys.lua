-- list of key constants used in-engine
local k = love.keyboard.getKeyFromScancode

local keys = {
	confirm = k'x',
	cancel  = k'z',
	x       = k'x',
	z       = k'z',
	lshift  = k'lshift',
	escape  = k'escape',
	w       = k'w',
	a       = k'a',
	s       = k's',
	d       = k'd',
	space   = k'space',
	up      = k'up',
	down    = k'down',
	left    = k'left',
	right   = k'right',
}

local function group(...)
	local set = {}

	for i=1, select('#', ...) do
		local keyname = select(i, ...)
		assert(keys[keyname], keyname)
		set[keys[keyname]] = true
	end

	return set
end

function keys.isDown(actionName)
	for keyname, _ in pairs(keys[actionName]) do
		if love.keyboard.isDown(keyname) then return true end
	end
	return false
end

keys.Jump      = group('w', 'space', 'up', 'x')
keys.Secondary = group('z', 'lshift')
keys.Left      = group('a', 'left')
keys.Right     = group('d', 'right')
keys.Down      = group('s', 'down')

return keys
