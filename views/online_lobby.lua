local class    = require 'gabe.class'
local client   = require 'net.client'
local prompts  = require 'gfx.prompts'
local colors   = require 'gfx.colors'
local overlay  = require 'overlay'

local lobby = class(...)
class.mixin(lobby, require 'views.lobby')

function lobby:init(lastState, gamemode)
	self.ready = false
	if not A.client or A.client.connected == false then
		A.client = client.new("localhost")
	end
	love.window.setTitle("Bclient "..Settings.nick)

	self.t = 0
	self.readystates = {}
	self.dirty = { nick = true, ready = true}

	self.overlay = overlay.new()

	self:initLobby(gamemode)
end

function lobby.load()
	lobby.loadLobby()

	A.lobbycolors = love.image.newImageData("assets/teamcolors.png")
end

function lobby:tick(dt)
	self:tickLobby(dt)

	self.overlay:update(dt)

	self.t = self.t + dt
	if self.t > .2 then
		self.t = self.t - .2
		if A.client.conn then
			A.client.conn:send({
				nick  = Settings.nick,
				ready = self.ready,
			})
		end
	end

	if A.client.connected then
		if self.dirty.nick then
			if A.client:msg("updateNick", Settings.nick) then
				self.dirty.nick = false
			end
		elseif self.dirty.ready then
			if A.client:msg("updateReady", self.ready) then
				self.dirty.ready = false
			end
		end
	end
end

function lobby:recv(data, peer, channel)
	self.readystates = data
end

function lobby:draw()
	local msg  = {colors.white}
	table.insert(msg, "\tPLAYERS:\n\n\t")
	if A.client.players then
		for peerN, player in pairs(A.client.players) do
			local ready = self.readystates[peerN] and "X" or " "
			if A.client.player_colors then
				for i, playerID in ipairs(A.client.player_colors) do
					if playerID == peerN then
						if playerID == A.client.clientID then
							self.overlay.team = i
						end
						local r, g, b, a = A.lobbycolors:getPixel(2, i)
						table.insert(msg, {r, g, b, a})
					end
				end
			else
				table.insert(msg, colors.white)
			end
			table.insert(msg, player.nick)
			table.insert(msg, colors.white)
			table.insert(msg, string.format(" [%s]\n\t", ready))
		end
	end
	self:drawLobby(function(_self, ww, hh)
		if not A.client.players then return end
		local players = {}
		for peerN, player in pairs(A.client.players) do
			local ready = _self.readystates[peerN] and "X" or " "
			if A.client.player_colors then
				for i, playerID in ipairs(A.client.player_colors) do
					if playerID == peerN then
						local r, g, b, a = A.lobbycolors:getPixel(2, i)
						table.insert(players, {
							nick = player.nick,
							id = peerN,
							ready = ready,
							color = {r, g, b, a}
						})
					end
				end
			else
				table.insert(players, {
					nick = player.nick,
					id = peerN,
					ready = ready,
					color = colors.white
				})
			end
		end

		love.graphics.push()
		love.graphics.translate(0, 246)
		local fnt = love.graphics.getFont()
		love.graphics.print("Ready up!\nPress", 4, -12)
		if S.input_method == "gamepad" then
			prompts.drawButton("a", 4+30, 0)
		else
			prompts.drawKey("x", "X", 6+35, 1)
		end
		local offset = fnt:getWidth("Ready up!") + 4
		love.graphics.translate(offset, 0)
		for i, player in ipairs(players) do
			local msg = string.format("[%s] %s", player.ready, player.nick)
			local w = ((ww-offset)/#players)
			local x = (i-1) * w
			love.graphics.setColor(player.color)
			love.graphics.printf(msg, x, 0, w, "center")
		end

		love.graphics.pop()
	end)

	self.overlay:draw()
end

function lobby:keypressed(k)
	if self.overlay:keypressed(k) then
		return
	elseif k == 'x' then
		self:toggleReady()
		S.events:pub("ding")
	elseif k == 'escape' then
		self.escape:escape(function()
			return love.keyboard.isDown('escape')
		end)
	end
end

function lobby:mousemoved(...)
	return self.overlay:mousemoved(...)
end

function lobby:mousepressed(...)
	return self.overlay:mousepressed(...)
end

function lobby:mousereleased(...)
	return self.overlay:mousereleased(...)
end

function lobby:toggleReady()
	self.ready = not self.ready
end

return lobby
