local class    = require 'gabe.class'
local gs       = require 'gamestate'
local suit_gp  = require 'suit_gamepad'
local lbox     = require 'gfx.lbox'
local template = require 'gfx.ui_template'

local mainmenu = class(...)
class.mixin(mainmenu, require 'views.menu')

function mainmenu:init()
	self.vol  = {value = Settings.volume}
	self:initMusic()
end

function mainmenu:suit(dt)
	suit_gp.reset(10, 10, 10, 10)
	local suit = suit_gp.s

	local h = 25
	if suit:Button("Local", suit_gp.row(100, h)).hit then
		gs.switch("views.assign_controllers")
		S.events:pub("ding", {})
	end

	--local t = require'version'
	--if suit:Button("Online", suit_gp.row(100, h)).hit then
	--	gs.switch("views.pickroom")
	--	S.events:pub("ding", {"picko"})
	--end

	local msg = Settings.fullscreen and "Fullscreen" or "Windowed"
	if suit:Button(msg, suit_gp.row(100, h)).hit then
		S.events:pub("ding", {})
		Settings.fullscreen = not Settings.fullscreen
		if Settings.fullscreen then
			love.window.setFullscreen(true, "desktop")
		else
			love.window.setFullscreen(false, "desktop")
		end
	end

	msg = Settings.stretch and "Stretched" or "Pixel-perfect"
	if suit:Button(msg, suit_gp.row(100, h)).hit then
		S.events:pub("ding", {})
		Settings.stretch = not Settings.stretch
		A.lbox  = lbox.new(2, 1920/4, 1080/4, not Settings.stretch)
	end

	msg = sss("Volume: %.2f", self.vol.value)
	suit:Label(msg, suit_gp.layout:row(100, 4))
	local s = suit:Slider(self.vol, suit_gp.row(100, 12, self.vol.value))
	if s.changed then

		love.audio.setVolume(self.vol.value)
		Settings.volume = self.vol.value
	end
	if s.hit then
		S.events:pub("test_volume", {})
	end

	msg = A.music.mute and "Music off" or "Music on"
	if suit:Button(msg, suit_gp.row(100, h)).hit then
		A.music:setMute(not A.music.mute)
	end

	if suit:Button("Quit", suit_gp.row(100, h)).hit then
		love.event.push("quit")
	end

--	msg = self.opened_discord and "Check your browser" or "Join our discord"
--	if suit:Button(msg, 210, 200, 120, 25).hit then
--		S.events:pub("ding", {})
--		love.system.openURL("https://discord.gg/JwEqGMy")
--		self.opened_discord = true
--	end
end

local demo_msg = [[
SPORTES TEMPORARY MENU

Thanks for playing the video game <3

The game is meant to be played online, but the netcode isn't quite there yet.  You can join an online game, but only one game room exists at the moment, so for now all games are public games.

If you'd like to organize a pickup, or try the secret net-test branch, you can join our discord room:
]]

local test_msg = [[
SPORTES (working title)

Max 4 players, gamepads recommended.

Online is currently broken at the moment, sorry :p
]]

function mainmenu:draw()
	template.draw(function(w, h)
		local msg = test_msg
		if (require'version'):match("demo") then
			msg = demo_msg
		end
		A.bbattle:setLineHeight(1.2)
		love.graphics.printf(msg, 120, 13, w-125)
		A.bbattle:setLineHeight(1)
		love.graphics.translate(-5, -5)
		suit_gp.s:draw()
	end)
end

function mainmenu:keypressed(...)
	suit_gp.s:keypressed(...)
end

function mainmenu:textinput(...)
	suit_gp.s:textinput(...)
end


return mainmenu
