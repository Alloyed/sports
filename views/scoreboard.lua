local class    = require 'gabe.class'
local gs       = require 'gamestate'
local template = require 'gfx.ui_template'
local colors   = require 'gfx.colors'
local tcolors  = require 'gfx.teamcolors'
local misc     = require 'misc'
local flux     = require 'flux'
local assman   = require 'assman'

local scoreboard = class(...)
class.mixin(scoreboard, require 'views.menu')

local point_map = {
	[4] = {5, 3, 2, 1},
	[3] = {5, 3, 2},
	[2] = {5, 0},
	[1] = {5},
}
local finishPoints = 15

function scoreboard:init(last, scores, nextgame)
	self.ready = false
	self.scores = scores
	self.nextGame = nextgame
	self:initMusic()

	if not self.scores then
		A.local_nicks = {"A", "b", "c", "d"}
		A.VPoints     = {1, 2, 3, 4}
		self.scores = {
			{id="local.1", score = math.random(0, 0)},
			{id="local.2", score = math.random(200, 10000)},
			{id="local.3", score = math.random(300, 10000)},
			{id="local.4", score = math.random(400, 10000)},
		}
	elseif A.client then
		-- log("am I? %t\n%t", A.client, self.scores)

		local colors = {}
		for color, playerID in ipairs(A.client.player_colors) do
			colors[playerID] = color
		end

		local score_list = {}
		for id, score in pairs(self.scores) do
			table.insert(score_list, {
				id = id,
				score = score
			})
		end
		self.scores = score_list

		A.local_nicks = {"A", "b"}
		A.VPoints   = {}
		-- TMP
		for i, _ in ipairs(A.client.player_colors) do
			A.VPoints[i] = math.random(0, 10)
		end

		table.sort(self.scores, function(a, b)
			return colors[a.id] < colors[b.id]
		end)
	else
		table.sort(self.scores, function(a, b)
			return a.id < b.id
		end)
	end
	local tmp = {}
	for i=1, #self.scores do
		tmp[i] = {i, self.scores[i].score}
	end
	table.sort(tmp, function(a, b)
		return a[2] > b[2]
	end)
	self.order = {false, false}
	for i, p in ipairs(tmp) do
		self.order[p[1]] = i
	end
	log("%t", tmp)
	self.winner_i = tmp[1][1]
	self.winner = self.scores[self.winner_i]

	self.scoretimes = {0, 0}
	self.flux = flux.group()
	local f = self.flux:to(self.scoretimes, .2, {[#self.scores] = 1})
	for i=#self.scores-1, 1, -1 do
		f = f:after(self.scoretimes, .2, {[i] = 1}):delay(.1)
	end

	-- this is gross but I'm unwilling to type something cleaner
	local doneCount = #tmp
	for pos, pair in ipairs(tmp) do
		local player_idx = pair[1]
		local diff = point_map[#tmp][pos]
		local np = A.VPoints[player_idx] + diff
		f:after(A.VPoints, diff*.33, {[player_idx] = np}):oncomplete(function()
			doneCount = doneCount - 1
			if doneCount <= 0 then
				self.animDone = true
			end
		end):ease("linear")
	end
end

function scoreboard.load()
	scoreboard.loadMusic()
	assman.load.anim("goddess")
	assman.load.anim("medals")
end

function scoreboard:tick(dt)
end

function scoreboard:recv(data, peer, channel)
end

function scoreboard:netDraw()
	local rows = {}
	local prepared = {}
	for peerID, score in pairs(self.scores) do
		assert(A.client.players)
		assert(A.client.players[peerID], tostring(peerID))
		table.insert(prepared, {
			id = peerID,
			score = score,
			nick = A.client.players[peerID].nick
		})
	end
	table.sort(prepared, function(a, b)
		return a.score > b.score -- higher == better
	end)

	for i, o in ipairs(prepared) do
		table.insert(rows, string.format("%d. \t %s \t %d", i, o.nick, o.score))
	end

	local msg = string.format([[
	SCORES:

%s
	]], table.concat(rows, "\n"))
	love.graphics.print(msg, 10, 10)
	love.graphics.print(tostring(prepared[1].nick) .. " won!", 10, 80)
	love.graphics.print("press X to go back to lobby", 10, 100)
end

local function drop(isf, str, x, y, ...)
	if isf == 'printf' then
			love.graphics.setColor(colors.black)
			love.graphics.printf(str, x+1, y+1, ...)
			love.graphics.setColor(colors.white)
			love.graphics.printf(str, x, y, ...)
	else
		error()
	end
end

function scoreboard:update(dt)
	self.flux:update(dt)
end

function scoreboard:draw()
	template.draw(function(w, h)
		local px, py = 20, 10
		local winner = A.local_nicks[self.winner_i]
		local wr    = self.winner.score
		if wr == 0 then wr = 1 end
		love.graphics.print(winner.." has been blessed!", px, py, 0, 2, 2)
		local msg = ("First to %d VP wins."):format(finishPoints)
		love.graphics.print(msg, px, py+24, 0)
		local span, iw = 96, 56
		span = span * 4 / #self.scores
		iw = iw * 4 / #self.scores
		local oy = 212
		local maxheight = 160
		for i=1, #self.scores do
			local height = maxheight * (self.scoretimes and self.scoretimes[i] or 1)
			local score = self.scores[i].score
			local pos   = self.order[i]
			local ratio = math.max(.3, (score == 0 and 1 or score) / wr)
			local nick = A.local_nicks[i] or "!OOPS!"

			i = i - 1 -- does not mutate loop !
			love.graphics.setColor(tcolors[i+1])
			love.graphics.rectangle('fill', (i+.5)*span-(iw*.5), oy, iw, -height*ratio)

			if self.scoretimes[i+1] >= .99 then
				love.graphics.push()
				love.graphics.translate((i+.5)*span-(iw*.5), oy-(height*ratio))
				drop('printf', tostring(score), 0, 18, iw, 'center')
				drop('printf', '#'..tostring(pos), 0, 4, iw, 'center')
				msg = ("+%d VP%s"):format(point_map[#self.scores][pos], pos == 1 and "!" or "")
				drop('printf', msg, 0, 4+24, iw, 'center')
				love.graphics.pop()
			end

			love.graphics.push()
			love.graphics.translate((i+.5)*span-(iw*.5) + 4, oy + 4+24)
			love.graphics.setColor(colors.white)
			local mpos = 1
			local points = A.VPoints[i+1]
			for _i, p in ipairs(A.VPoints) do
				if _i ~= i+1 and p > points then
					mpos = mpos + 1
				end
			end
			if mpos < 4 then
				A.medals:draw(mpos, -5, -15)
			end

			love.graphics.printf(nick, 0, -24, iw, 'left')
			points = string.format("%2.f", A.VPoints[i+1])
			love.graphics.print("VP: ", 0, 0, 0, 1, 1)
			local dx, dy = 26, 6
			local ix, iy =  6, 6
			local scale = (1-((A.VPoints[i+1]+.99) % 1)) + 1
			love.graphics.print(points, dx, dy, 0, scale, scale, ix, iy)
			love.graphics.pop()
		end
		love.graphics.setColor(colors.white)
		local x, y = 376, 30
		A.goddess:draw(1, x, y+misc.osc(3)*6)
	end)
end

function scoreboard:nextEvent()
	if self.animDone then
		S.events:pub("ding", {})
		if A.VPoints then
			for i, s in ipairs(A.VPoints) do
				if s >= finishPoints then
					return gs.switch("views.winner", i)
				end
			end
		end
		if A.client then
			-- XXX: nextGame is untrusted input, make sure it's a game before
			-- it gets here
			gs.switch("views.online_lobby", "games."..self.nextGame..".view")
		else
			gs.switch("views.local_lobby", self.nextGame)
		end
	else
		for i=1, 1000 do self.flux:update(1) end
	end
end

function scoreboard:keyreleased(k)
	if k == 'x' then
		self:nextEvent()
	end
end

function scoreboard:gamepadreleased(gp, btn)
	if btn == 'a' then
		self:nextEvent()
	end
end

return scoreboard
