-- A simple transition

local class  = require 'gabe.class'
local colors = require 'gfx.colors'
local gs     = require 'gamestate'
local Flux   = require 'flux'

local fade = class(...)
function fade:init(old, new)
	self.old = old
	self.new = new
	self.t = 0
	self.flux = Flux.group()
	local f = self.flux

	f = f:to(self, .3, {t = .5}):ease('quadout')
	f = f:oncomplete(class.method(self, "loadNext"))

	f = f:after(self, .3, {t = 1}):ease('quadout')
	f:oncomplete(class.method(self, "finish"))
end

function fade:loadNext()
	self.load_next = true
end

function fade:finish()
	gs.switch_obj(self.new)
end

function fade.load()
end

function fade:update(dt)
	-- we expect the frame in which we load everything to be longer than the
	-- rest so cap to 1/60 
	if self.load_next then
		self.load_next = nil
		local N = require(self.new[1])
		N.load()
		self.new = N.new(self, unpack(self.new, 2))

		self.show_new = true
		self.toss_frame = true
		return
	end
	if self.toss_frame then
		self.toss_frame = nil
		dt = 1/60
	end
	self.flux:update(dt)
end

function fade:draw()
	local W, H = A.lbox:dimensions()
	local t = self.t
	if not self.show_new then
		if self.old.draw then
			self.old:draw()
		end
	else
		if self.new.draw then
			self.new:draw()
		end
	end
end

function fade:drawUI()
	local W, H = A.lbox:dimensions()
	local t = self.t
	if not self.show_new then
		if self.old.drawUI then
			self.old:drawUI()
		end
	else
		if self.new.drawUI then
			self.new:drawUI()
		end
	end

	--t = 1-t
	if not self.show_new then
		love.graphics.setColor(colors.black)
		t = t * 2
		love.graphics.rectangle('fill', W-(W*t), 0, W, H)
	else
		t = t-.5
		t = t * 2
		love.graphics.setColor(colors.black)
		love.graphics.rectangle('fill', 0, 0, W-(W*t), H)
	end
end

return fade
