-- YOUR WINNER!
--
local class    = require 'gabe.class'
local gs       = require 'gamestate'
local template = require 'gfx.ui_template'
local colors   = require 'gfx.colors'
local misc     = require 'misc'
local flux     = require 'flux'
local assman   = require 'assman'

local winner = class(...)
class.mixin(winner, require 'views.menu')

function winner:init(last, winner)
	self.winner_i = winner or 1
	self.flux = flux.group()
end

function winner.load()
	winner.loadMusic()
	assman.load.anim("goddess")
end

function winner:tick(dt)
end

function winner:recv(data, peer, channel)
end

function winner:update(dt)
	self.flux:update(dt)
end

function winner:draw()
	template.draw(function(w, h)
		local px, py = 20, 10
		local winner = A.local_nicks[self.winner_i]
		love.graphics.print(winner.." has won!", px, py, 0, 2, 2)
		love.graphics.setColor(colors.white)
		local x, y = 376, 30
		A.goddess:draw(1, x, y+misc.osc(3)*6)
		love.graphics.print("tmp screen :)\n press x/a to continue", 10, h-40)
	end)
end

function winner:nextEvent()
	gs.switch("views.mainmenu")
end

function winner:keyreleased(k)
	if k == 'x' then
		self:nextEvent()
	end
end

function winner:gamepadreleased(gp, btn)
	if btn == 'a' then
		self:nextEvent()
	end
end

return winner
