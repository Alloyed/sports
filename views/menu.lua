local class    = require 'gabe.class'

local menu = class(...)

function menu:initMusic()
	A.music:fadein("menu")
end

function menu.loadMusic()
	A.music:load("menu")
	A.music.songs["menu"]:setLooping(true)
end

function menu.load()
	menu.loadMusic()
end

return menu
