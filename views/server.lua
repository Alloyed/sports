local class  = require 'gabe.class'
local shared = require 'net.shared'
local gs     = require 'gamestate'
local reload = require 'gabe.reload'
local Server = require 'net.server'

local log    = require 'log'

require 'vec'
require 'games.spelunking.logic'

local server = class(...)

local lobby  = class("lobby")
function server:init()
	log.info("Server version: %q", require'version')
	self.open_peers = 0
	A.server = A.server or Server.new()
	Settings.debug_ui = true

	self.serverID = math.random()

	self:cleanup()

	if love.window then
		love.window.setTitle("Bserver")
	end
end

function server:cleanup(n)
	-- FIXME: the smart thing to do would be only toss playerdata if there's no
	-- active peer with that ID
	if n ~= 2 then
		self.playerData = {}
	end
	self.room = {
		players  = {},
	}
	self:setRoom(lobby, self.room)
end

function server:setRoom(klass, ...)
	assert(klass, debug.traceback())
	self.room.state = klass.new(...)
	log.info("room.state = %s(%s)", class.xtype(klass), table.concat(..., ", "))
end

function lobby:init(room, game_name)
	local ready = {}
	for playerID, _ in pairs(room.players) do
		ready[playerID] = false
	end
	self.ready = ready
	self.gameName = game_name or "archery"
end

function lobby:updateColors()
	local pconfig = self.colors or {}
	for i=#pconfig, 1, -1 do
		pconfig[i] = nil
	end
	for peerID, _ in pairs(self.ready) do
		table.insert(pconfig, peerID)
	end
	self.colors = pconfig
end

function lobby.disconnect(state, srv, peer)
	state.ready[peer:id()] = nil
end

local game    = require 'games.mixins.server'
require 'games.archery.server'
local spelunking = class("games.spelunking.server")
function lobby.update(state, srv, room, dt)
	local N = 0
	local all_ready = true
	for _, ready in pairs(state.ready) do
		N = N + 1
		if ready ~= true then
			all_ready = false
			break
		end
	end
	state:updateColors()
	if N >= 2 and all_ready then
		local game = state.gameName
		assert(game)
		A.server:broadcast({"startInstancedMatch", game, state.colors}, 1)
		local klass = class.get("games."..game..".server")
		srv:setRoom(klass, state.colors)
	end
end

local function sendRoomData(srv, room, peer)
	--peer:send({"updatePlayers", room.players}, 1)
	local data = {}
	for playerID, _ in pairs(room.players) do
		data[playerID] = srv.playerData[playerID]
	end
	peer:msg("updatePlayers", data)
	peer:msg("updatePlayerColors", room.state.colors)
end

function lobby.recv(state, srv, room, data, peer, channel)
	if data then
		if room.players[peer:id()] then
			state.ready[peer:id()] = data.ready
			peer:send(state.ready)
			sendRoomData(srv, room, peer)
		end
	end
end
class.mixin(spelunking, game)

function spelunking:init(config)
	self.config = config
	self.playerObjects = {}
	self.time_now = 0
end

function spelunking.update(state, srv, room, dt)
	state.time_now = state.time_now + dt
end

function spelunking.recv(state, srv, room, data, peer, channel)
	if channel == 2 then
		local old = state.playerObjects[peer:id()]
		if not old or old.time_now < data.time_now then
			state.playerObjects[peer:id()] = data
			peer:send({
				time_now = state.time_now,
				playerObjects = state.playerObjects
			}, 2, "unsequenced")
		end
	end
end

function server:tick(dt)
	local ok, err = pcall(function()
		self.room.state:update(self, self.room, dt)
	end)
	if not ok then
		log.warning("%s", err)
	end
end

function server:connect(peer)
	log.info("%s (%s) has connected.", peer, peer.conn:connect_id())
	self.open_peers = self.open_peers + 1
	peer:timeout(16, 3000, 6000) -- TODO
	if self.open_peers >= 4 then
		log.info("%s tried to join, but no room!", peer)
		peer:disconnect(shared.DISCONNECT.FULL)
		return
	elseif not class.is(self.room.state, "lobby") then
		log.info("%s tried to join, but game in progress!", peer)
		peer:disconnect(shared.DISCONNECT.NOT_IN_LOBBY)
		return
	end
	self.playerData[peer:id()] = {
		nick = "_FIXME",
	}
	peer:msg("setClientID", peer:id())
	peer:msg("testVersion", require'version')
	peer:msg("requestState")
end

function server:disconnect(peer, data)
	self.open_peers = self.open_peers - 1
	log.info("%s has disconnected.", peer)
	self.playerData[peer:id()] = nil
	if self.open_peers == 0 then
		-- RESET
		log.info("Room is empty, time to reset!")
		self:cleanup()
	else
		if self.room.state.disconnect then
			self.room.state:disconnect(self, peer)
		end
		self.room.players[peer:id()] = nil
	end
end

local messages = {}

function messages.updateReady(srv, peer, ready)
end

function messages.updateNick(srv, peer, nick)
	srv.playerData[peer:id()].nick = nick
	log.info("[%s].nick = %s", peer, nick)
end

function messages.updateView(srv, peer, view)
	srv.playerData[peer:id()].view = view
end

function messages.sendLine(srv, peer, line)
	-- TODO: don't send to origin
	--line.origin = peer:id()
	--A.server:msgFrom(peer, "sendLine", line)
end

function messages.tmpLine(srv, peer, line)
	-- TODO: don't send to origin
	--line.origin = peer:id()
	--A.server:msgFrom(peer, "tmpLine", line)
end

function messages.event(srv, peer, evname, evdata)
	if srv.room.time_now then
		evdata.server_time = srv.room.time_now
	end
	evdata.origin = peer:id()

	A.server:msgFrom(peer, "event", evname, evdata)
end

function messages.joinRoom(srv, peer, nick, roomID)
	-- roomID goes in the trash
	local player = srv.playerData[peer:id()]
	player.nick = nick
	srv.room.players[peer:id()] = true

	local state = srv.room.state

	if class.is(state, "lobby") then
		local view = ("games.%s.view"):format(state.gameName)
		peer:msg("switchView", "views.online_lobby", view)
	else
		error("NYI")
	end
end

function messages.finishInstancedMatch(self, peer, score)
	log.info("[%s].score = %s", peer, score)
	local room = self.room

	if not room.state.results then room.state.results = {} end
	room.state.results[peer:id()] = score
	local finished = true
	for peerN, _ in pairs(room.players) do
		if not room.state.results[peerN] then
			finished = false
			break
		end
	end
	if finished then
		local gn = "archery"
		A.server:msg("switchView", "views.scoreboard", room.state.results, gn)
		log.info("match results: %t", room.state.results)
		self:setRoom(lobby, self.room, gn)
	end
end

function messages:reload()
	log.info("reloading")
	reload.reload_all()
	gs.refresh()
	A.server:msg("reload")
end

function messages:reset()
	log.info("resetting")
	reload.reload_all()
	self:cleanup(2)
	A.server:msg("reset")
end

function server:recv(data, peer, channel)
	local ok, err = pcall(function()
		S.last_peer = peer
		if not data then return end -- invalid packet

		if channel == 1 then -- RPC recieve
			local name = table.remove(data, 1)
			if messages[name] then
				return messages[name](self, peer, unpack(data))
			else
				local msg = name
				local sep = "("
				for _, arg in ipairs(data) do
					msg = msg .. sep .. tostring(arg)
					sep = ", "
				end
				msg = msg .. ")"
				return log.warning("missing message: %s", msg)
			end
		else
			self.room.state:recv(self, self.room, data, peer, channel)
		end
	end)
	if not ok then
		log.warning("%s", err)
	end
end

function server:draw()
	if Settings.debug_ui then
		imgui.Text("data")
		imgui.Text(require'inspect'(self))
	end
end

return server
