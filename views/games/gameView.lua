-- base mixin for game views
local class = require 'gabe.class'
local misc  = require 'misc'

local base = class(...)

function base:baseInit(_game, player_config)
	self.W = _game.new(player_config)
	local R = self:renderer()
	self.W:worldEach("initView", R)
	self.time_now = 0
end

function base:startTime()
	if not C[self].timer then
		C[self].timer = function() return self.W.time_now end
	end

	misc.timer = C[self].timer
end

function base:endTime()
	misc.timer = love.timer.getTime
end

function base:wrapTime(fn, ...)
	self:startTime()
	fn(...)
	self:endTime()
end

function base:baseTick(dt)
	--self.flux:update(dt)

	self:wrapTime(self.W.tick, self.W, dt)
end

function base:baseUpdateView(dt, R)
	self:wrapTime(function()
		self.W:worldEach("updateView", self.W, dt, R)
	end)
end

base.updateView = base.baseUpdateView

function base:renderer()
	local R = C[self].render
	if not C[self].render then
		R = {}

		self:initRenderer()

		C[self].render = R
	end
	return R
end

function base:initRenderer(R) end
function base:cleanRenderer() end

function base:startDraw()
	self:startTime()
	self:cleanRenderer(self:renderer())
	love.graphics.clear(33, 33, 33)
	love.graphics.push()
end

function base:drawCollision(W, R)
	if Settings.debug_ui then
		local _
		_, Settings.draw_collision = imgui.Checkbox("draw collision", Settings.draw_collision)
	end
	if Settings.draw_collision then
		W:worldEach("drawCollision", W)
	end
end

function base:endDraw()
	love.graphics.pop()
	self:endTime()
end

return base
