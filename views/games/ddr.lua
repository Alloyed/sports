local class = require 'gabe.class'
local misc  = require 'misc'
local colors  = require 'gfx.colors'

local name = string.match((...), "([^.]+)$")

local _game = require('games.'..name)

local game = class(...)
class.mixin(game, require'views.games.gameView')

-- lifecycle
function game:init(prev, player_config)
	self:baseInit(_game, player_config)
end

function game.load()
	_game.load()
end

function game:exit()
end

-- update
function game:tick(dt)
	self:baseTick(dt)
end

function game:updateAudio()
end

-- draw
function game:initRenderer(R)
end

function game:cleanRenderer(R)
end

function game:draw()
	local view = self
	view:startDraw()
	local R = view:renderer()
	local W = view.W

	-- camera
	love.graphics.push()
	love.graphics.setColor(colors.white)

	W:worldEach("draw1", W, R)
	view:drawCollision(W)
	love.graphics.pop()
	view:endDraw()
end

-- events
function game:keypressed(k)
	if k == 'j' then
		self.W.track:press(1)
	elseif k == 'k' then
		self.W.track:press(2)
	elseif k == 'i' then
		self.W.track:press(3)
	elseif k == 'l' then
		self.W.track:press(4)
	end
end

function game:keyreleased(k)
	if k == 'j' then
		self.W.track:release(1)
	elseif k == 'k' then
		self.W.track:release(2)
	elseif k == 'i' then
		self.W.track:release(3)
	elseif k == 'l' then
		self.W.track:release(4)
	end

end

return game
