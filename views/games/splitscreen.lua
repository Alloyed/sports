-- splitscreen view container.

local class   = require 'gabe.class'

local splitscreen = class(...)

function splitscreen:init()
end

function splitscreen.load()
end

function splitscreen:enter()
	self.old_w, self.old_h = A.lbox:dimensions()
	A.lbox.target_w, A.lbox.target_h = self.old_w*2, self.old_h*2
	A.lbox:recalculate()

	local archery = require 'games.archery.view'
	A.controls = love.joystick.getJoysticks()
	archery.load()
	self.states = {
		archery.new(nil, {"local.1"}),
		archery.new(nil, {"local.2"}),
		--archery.new(nil, {"local.3"}),
		--archery.new(nil, {"local.4"}),
	}
	for _, state in ipairs(self.states) do
		state:enter()
		for _, p in pairs(state.W.players) do
			print(p.method)
			if type(p.method) == "userdata" then
				print(p.method:getName())
			end
		end
	end
end

local function inject(name)
	splitscreen[name] = function(self, ...)
		for _, state in ipairs(self.states) do
			state[name](state, ...)
		end
	end
end

inject"tick"
inject"keypressed"
inject"keyreleased"
inject"gamepadpressed"
inject"gamepadpressed"
inject"mousepressed"
inject"mousereleased"

local function injectD(name)
	splitscreen[name] = function(self, ...)
		A.lbox.target_w, A.lbox.target_h = self.old_w, self.old_h
		if #self.states == 2 then -- 2
			local dx = math.floor(self.old_w - (self.old_w*.5))
			love.graphics.push("all")
			love.graphics.translate(dx, 0)
			love.graphics.setScissor(dx, 0, self.old_w, self.old_h)
			self.states[1][name](self.states[1])
			love.graphics.pop()

			love.graphics.push("all")
			love.graphics.translate(dx, self.old_h)
			love.graphics.setScissor(dx, self.old_h, self.old_w, self.old_h)
			self.states[2][name](self.states[2])
			love.graphics.pop()
		else -- 3 or 4
			local state = self.states[1]
			local dx, dy = 0, 0
			if state then
				love.graphics.push("all")
				love.graphics.translate(dx, dy)
				love.graphics.setScissor(dx, dy, self.old_w, self.old_h)
				state[name](state)
				love.graphics.pop()
			end

			local state = self.states[2]
			local dx, dy = self.old_w, 0
			if state then
				love.graphics.push("all")
				love.graphics.translate(dx, dy)
				love.graphics.setScissor(dx, dy, self.old_w, self.old_h)
				state[name](state)
				love.graphics.pop()
			end

			local state = self.states[3]
			local dx, dy = 0, self.old_h
			if state then
				love.graphics.push("all")
				love.graphics.translate(dx, dy)
				love.graphics.setScissor(dx, dy, self.old_w, self.old_h)
				state[name](state)
				love.graphics.pop()
			end

			local state = self.states[4]
			local dx, dy = self.old_w, self.old_h
			if state then
				love.graphics.push("all")
				love.graphics.translate(dx, dy)
				love.graphics.setScissor(dx, dy, self.old_w, self.old_h)
				state[name](state)
				love.graphics.pop()
			end
			love.graphics.setLineStyle('rough')
			love.graphics.line(self.old_w, 0, self.old_w, self.old_h*2)
			love.graphics.line(0, self.old_h, self.old_w*2, self.old_h)
		end
		A.lbox.target_w, A.lbox.target_h = self.old_w*2, self.old_h*2
	end
end

injectD "draw"
injectD "drawUI"

function splitscreen:exit()
	A.lbox.target_w, A.lbox.target_h = self.old_w, self.old_h
	A.lbox:recalculate()
end

return splitscreen
