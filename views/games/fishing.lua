local class = require 'gabe.class'
local misc  = require 'misc'
local colors  = require 'gfx.colors'

local name = string.match((...), "([^.]+)$")

local _game = require('games.'..name)

local game = class(...)
class.mixin(game, require'views.games.gameView')

-- lifecycle
function game:init(prev, player_config)
	self:baseInit(_game, player_config)
end

function game.load()
	_game.load()
end

function game:exit()
end

-- update
function game:tick(dt)
	self:baseTick(dt)
end

function game:updateAudio()
end

-- draw
function game:initRenderer(R)
end

function game:cleanRenderer(R)
end

function game:draw()
	local view = self
	view:startDraw()
	local R = view:renderer()
	local W = view.W

	-- camera
	love.graphics.push()
	love.graphics.setColor(colors.white)
	local p = W.player.p
	local winw, winh = A.lbox:dimensions()
	love.graphics.translate(winw*.5-p.x, winh*.5-p.y)

	W:worldEach("draw1", W, R)

	local w, h = A.lbox:dimensions()
	love.graphics.rectangle('line', 0, 77, w, h-77)
	view:drawCollision()
	love.graphics.pop()
	W:worldEach("drawUI", W, R)
	view:endDraw()
end

-- events
function game:keypressed(k)
end

function game:mousepressed(x, y, btn)
	self.W.player:mousepressed(self.W, x, y, btn)
end

function game:mousereleased(x, y, btn)
	self.W.player:mousereleased(self.W, x, y, btn)
end

return game
