local class  = require 'gabe.class'
local misc   = require 'misc'
local colors  = require 'gfx.colors'
local vec     = require 'vec'
local flux    = require 'flux'

local hurdles = class(...)
class.mixin(hurdles, require 'games.mixins.world')

local player = class((...)..".player")
player.color_n = 11

function player:init()
	self.p = vec.new(100, 100)
	self.v = vec.new(0, 0)
	self.bar = 0
	self.last_bar = 0
	self.speed = 200
	self.onGround = true
	self.bounce = 0

	local ps = love.graphics.newParticleSystem(A.pixel)
	ps:setEmissionRate(18)
	ps:setEmitterLifetime(.15)
	ps:setParticleLifetime(1)
	ps:setSizes(10, 8, 4, 1, 0)
	ps:setSpeed(100)
	ps:setLinearAcceleration(0, 0, -20, -100)
	ps:stop()

	self.ps = ps
end

local points = {0, .1, .2, .4, .8, .95, 1, .4}
local function curve(n)
	n = misc.clamp(n, 0, 1)
	local steps = #points-1
	local bot = math.min(steps, math.floor(n*steps)+1)
	if n == 1 then
		return points[steps+1]
	end
	return misc.lerp(points[bot], points[bot+1], (n*steps)%1)
end
_G.curve = curve

local free_grav = 400*2
local held_grav = 210*2
function player:tick(dt)
	local bar_decay = .66
	if self.crouching and self.bar < .4 then
		bar_decay = .7
	elseif not self.onGround then
		bar_decay = .3
	end
	self.bar = self.bar - bar_decay*dt
	self.bar = misc.clamp(misc.lerp(self.last_bar, self.bar, .5), 0, 1)
	self.last_bar = self.bar
	self.v.x = curve(self.bar) * self.speed
	local grav = love.keyboard.isDown('x') and held_grav or free_grav

	self.v.y = self.v.y + (grav * dt)
	self.p:add_(self.v:scale(dt))
	self.p.y = math.min(self.p.y, 100)
	if not self.onGround and self.p.y == 100 then
		self.onGround = true
		self.bounce = 1
		flux.to(self, .2, {bounce = 0})
		self.ps:stop()
		self.ps:start()
	end
	if self.crouching and self.bar > .5 then
		self.ps:start()
	end

	self.ps:moveTo(self.p.x, self.p.y+10)
	self.ps:setSpeed(self.v.x*.6)
	self.ps:update(dt)
end

function player:crouch(set)
	if set == true and self.onGround then
		self.crouching = true
	else
		if self.crouching then
			self.bounce = .5
			flux.to(self, .1, {bounce = 0})
		end
		self.crouching = false
	end
end

function player:draw()
	love.graphics.setLineStyle('rough')
	local h = 10
	local w = 10
	if self.crouching then
		h = 3
		w = 14
	elseif not self.onGround then
		h = 10 + math.min(4, math.abs(self.v.y)/100)
		w = 10 - math.min(4, math.abs(self.v.y)/100)
	elseif self.bounce > .01 then
		h = 10 - self.bounce
		w = 10 + self.bounce
	end
	love.graphics.setColor(colors.white)
	love.graphics.draw(self.ps, 0, 0)
	love.graphics.setColor(colors[self.color_n])
	love.graphics.rectangle('fill', self.p.x, self.p.y+10-h, w, h)
	love.graphics.setColor(colors.white)
end

function player:drawUI()
	if self.skip_ui then
		return
	end

	love.graphics.push()
	love.graphics.translate(100, 180 - 20)

	if self.bar < .7 then
		love.graphics.setColor(colors[11])
	elseif self.bar > .85 then
		love.graphics.setColor(colors[28])
	else
		love.graphics.setColor(colors[10])
	end
	love.graphics.rectangle('fill', 0, 100, 10, -100*self.bar)
	love.graphics.setColor(colors.white)
	love.graphics.rectangle('line', 0, 0, 10, 100)
	love.graphics.line(0, 100-(70), 10, 100-(70))
	love.graphics.line(0, 100-(85), 10, 100-(85))
	local btns = {
		x     = {x = 0,   y = -20},
		z     = {x = 0,   y = 20},
		left  = {x = -20, y = 0},
		right = {x = 20,  y = 0}
	}
	for k, pos in pairs(btns) do
		if (k == 'left' or k == 'right') and k ~= self.lastStep then
			love.graphics.circle('line', pos.x-40, pos.y, 10)
		end
		local style = love.keyboard.isDown(k) and 'fill' or 'line'
		love.graphics.circle(style, pos.x-40, pos.y, 8)
	end

	local t = {}
	local graph_w, graph_h, steps = 60, 30, 60
	for i=0, steps do
		t[#t+1], t[#t+2] = 30+(i*graph_w/steps), 30-curve(i/steps)*graph_h
	end
	love.graphics.line(unpack(t))
	love.graphics.circle('line', 30+self.bar*graph_w, 30-curve(self.bar)*graph_h, 3)
	love.graphics.line(30, 30, 30, 30-graph_h)
	love.graphics.line(30, 30, 30+graph_w, 30)
	local msg = string.format("Speed %.2f pix/s", self.v.x)
	love.graphics.print(msg, 30, 80)
	love.graphics.pop()
end

function player:step()
	local a = math.max(.1, .7 - self.bar)
	if self.onGround and not self.crouching then
		self.bar = self.bar + a
	end
end

function player:tryJumping() -- memes
	if self.onGround then
		self.v.y = -160
		self.onGround = false
	end
end

local npc = class((...)..".npc")
class.mixin(npc, player)
npc.skip_ui = true
npc.color_n = "white"
npc.step_rate = .12
npc.step_top_rate = .17

function npc:update(dt)
	self.step_ct = self.step_ct or 0
	self.step_ct = self.step_ct + dt
	local step_rate = self.bar < .7 and self.step_rate or self.step_top_rate

	if self.step_ct > step_rate then
		self:step()
		self.step_ct = self.step_ct - step_rate
	end

	self.jump_ct = self.jump_ct or 0
	self.jump_ct = self.jump_ct + dt
	local jump_rate = 1
	if self.jump_ct > jump_rate then
		self.jump_ct = self.jump_ct - jump_rate
		if math.random() < .5 then
			self:tryJumping()
		end
	end
	player.update(self, dt)
end

function hurdles:init()
	self:worldInit()

	self.player = self:addItem(player.new())
	self:addItem(npc.new())

	local ps = love.graphics.newParticleSystem(A.wind)
	ps:setEmissionRate(2)
	ps:setParticleLifetime(20)
	ps:setSpeed(100)
	ps:setDirection(math.pi)
	ps:setAreaSpread('uniform', 40, 60)

	self.wind = ps

	self.timeLeft = 4
	flux.to(self, 4, {timeLeft = 0}):ease("linear"):oncomplete(function()
		if A.client then
			A.client:msg("finishInstancedMatch", self.player.p.x)
		end
	end)
end

-- stick asset loading here, will be used as a coroutine
function hurdles.load()
end

function hurdles:update(dt)
	flux.update(dt)
	self.wind:update(dt)
	self:worldEach("update", dt)
end

local function draw_bg(cam_x, w)
	local obj_w = 80
	for x=misc.quantize(cam_x-w, obj_w), cam_x+w, obj_w do
		math.randomseed(x)
		local n = math.random(#colors)
		love.graphics.setColor(colors[n])
		love.graphics.rectangle('fill', x, 0, obj_w, 20)

		if math.floor(x / obj_w)%20 == 0 then
			love.graphics.setColor(colors.white)
			love.graphics.rectangle('fill', x, 90, 10, 20)
		elseif math.floor(x / obj_w)%10 == 0 then
			love.graphics.setColor(colors.white)
			love.graphics.rectangle('fill', x, 70, 10, 30)
		end
	end
end

function hurdles:draw()
	love.graphics.clear(colors[2])
	love.graphics.push()
	local w, h = love.graphics.getDimensions()
	self.last_ratio = self.last_ratio or .4
	local ratio = misc.lerp(self.last_ratio, misc.lerp(.4, .25, self.player.bar), .025)
	self.last_ratio = ratio
	local cam_x = (w*ratio)-self.player.p.x

	love.graphics.setColor(colors[26])
	love.graphics.rectangle('fill', 0, 110, w, h)

	love.graphics.translate(cam_x, 0)
	self.wind:moveTo(-cam_x+w, h*.5 - 70)
	draw_bg(-cam_x, w)
	self:worldEach("draw")
	local alpha = math.min(1, math.max(0, self.player.v.x - 170)/30)
	love.graphics.setColor(misc.rgba(colors.white, alpha*255))
	--love.graphics.draw(self.wind, 0, 0)
	love.graphics.pop()
	self:worldEach("drawUI")
	love.graphics.setColor(colors.white)
	love.graphics.print(tostring(self.timeLeft), 10, 10)
end

function hurdles:keypressed(k)
	if (k == 'left' or k == 'right') and k ~= self.player.lastStep then
		self.player:step()
		self.player.lastStep = k
	elseif k == 'x' then
		self.player:tryJumping()
	elseif k == 'z' then
		self.player:crouch(true)
	end
end

function hurdles:keyreleased(k)
	if k == 'z' then
		self.player:crouch(false)
	end
end

return hurdles
