local vec  = require 'vec'
local class = require 'gabe.class'
local assman = require 'assman'
local colors  = require 'gfx.colors'
local misc = require 'misc'

local name = string.match((...), "([^.]+)$")

local _game = require('games.'..name)

local game = class(...)
class.mixin(game, require'views.games.gameView')

-- lifecycle
function game:init(prev, player_config)
	self.Worlds = {}
	for i=0,9 do
		self:baseInit(_game, player_config)
		self.Worlds[i] = self.W
	end
	self.W = self.Worlds[1]
	self.cursor = vec.new(1, 1)
	self.cursor.tile = 1
	self.report = { text = "", ts = 0 }
end

function game:log(msg, ...)
	self.report.text = string.format(msg, ...)
	self.report.ts   = love.timer.getTime()
end

function game:setWorld(n)
	self.W = self.Worlds[n]
	self:log("World: %d", n)
end

function game.load()
	_game.load()
	love.graphics.setFont(A.bbattle)
	assman.load.anim("editor")
end

function game:exit()
end

-- update
local thekeys = {
	up = 0,
	down = 0,
	left = 0,
	right = 0,
}
function game:tick(dt)
	self:baseTick(dt)
	for k in pairs(thekeys) do
		if love.keyboard.isDown(k) then
			thekeys[k] = thekeys[k] + dt
			if thekeys[k] > .2 then
				thekeys[k] = thekeys[k] - .2
				self:keypressed(k)
			end
		else
			thekeys[k] = -.1
		end
	end
end

function game:updateAudio()
end

-- draw
function game:initRenderer(R)
end

function game:cleanRenderer(R)
end

function game:draw()
	local view = self
	view:startDraw()
	local R = view:renderer()
	local W = view.W

	-- camera
	love.graphics.push()
	love.graphics.setColor(colors.white)
	love.graphics.setLineStyle('rough')

	-- tiles
	W:worldEach("drawLayer", W, R, 1)

	-- grid
	love.graphics.setColor(55, 55, 55, 100)
	local cell = 24
	local dimw, dimh = A.lbox:dimensions()
	for x=0, dimw, cell do
		love.graphics.line(x, 0, x, dimh)
	end
	for y=0, dimh, cell do
		love.graphics.line(0, y, dimw, y)
	end

	-- cursor
	love.graphics.setColor(colors.white)
	local cx, cy = self.cursor:unpack()
	if love.keyboard.isDown('z', 'x') or love.timer.getTime() % 1.2 < 1 then
		if self.cursor.tile then
			love.graphics.setColor(255, 255, 255, 66)
			A.editor:drawFrame(1, self.cursor.tile, cx*cell, cy*cell)
		end
		love.graphics.setColor(colors.white)
		love.graphics.rectangle('line', cx*cell, cy*cell, cell, cell)
	end

	view:drawCollision(W)
	love.graphics.pop()

	if self.tilepick then
		local img = A.editor.img
		love.graphics.push()
		love.graphics.translate(100, 100)
		love.graphics.setColor(colors.black)
		love.graphics.rectangle('fill', -10, -10, img:getWidth()+20, img:getHeight()+20)
		love.graphics.setColor(colors.white)
		love.graphics.draw(A.editor.img)
		local tile = self.cursor.tile
		love.graphics.rectangle('line', (tile-1)*cell, 0, cell, cell)
		love.graphics.pop()
	end

	if self.report and love.timer.getTime() - self.report.ts < 2 then
		local w = A.bbattle:getWidth(self.report.text)
		local h = A.bbattle:getHeight()
		love.graphics.setColor(colors.black)
		love.graphics.rectangle('fill', 0, 0, w+8, h+8)
		love.graphics.setColor(colors.white)
		love.graphics.print(self.report.text, 4, 4)
	end
	view:endDraw()
end

-- events
local dir = {
	up    = vec.new(0, -1),
	down  = vec.new(0, 1),
	left  = vec.new(-1, 0),
	right = vec.new(1, 0),
}
dir.h = dir.left
dir.j = dir.down
dir.k = dir.up
dir.l = dir.right

function game:keypressed(k)
	if k == 'a' then
		self.editmode = self.editmode == "edit" and "play" or "edit"
	end
	if self.editmode == "play" then
		return
	end
	if self.tilepick then
		if tonumber(k) then
			self.cursor.tile = tonumber(k)
		end
	end

	if k == 'z' then
		self:placeTile()
	elseif k == 'x' then
		self:removeTile()
	elseif dir[k] then
		self:moveCursor(dir[k])
	elseif tonumber(k) and love.keyboard.isDown('lctrl') then
		self:setWorld(tonumber(k))
	elseif k == 'space' then
		self.tilepick = true
	elseif k == 'g' then
		self:fillTile()
	end
end

function game:keyreleased(k)
	if k == 'space' then
		self.tilepick = false
	end
end

function game:placeTile()
	self.W.map:set(1, self.cursor.x, self.cursor.y, self.cursor.tile)
end

function game:removeTile()
	self.W.map:set(1, self.cursor.x, self.cursor.y, nil)
end

local function tov(x, y)
	return x .. ":" .. y
end
local function fromv(str)
	local x, y = str:match("^([^|]*):(.*)$")
	return tonumber(x), tonumber(y)
end
local function search(map, x, y)
	local c = map:get(1, x, y)
	local open = {tov(x, y)}
	local seen = {[open[1]] = true}
	local iter = 0
	while #open > 0 do
		iter = iter + 1
		if iter > 100 then
			return {} -- early out
		end
		local iv = table.remove(open)
		local ix, iy = fromv(iv)
		for dx=-1, 1 do
			for dy = -1, 1 do
				local nx, ny = ix+dx, iy+dy
				local nv = tov(nx, ny)
				if not seen[nv] and map:get(1, nx, ny) == c then
					seen[nv] = true
					table.insert(open, nv)
				end
			end
		end
	end
	return seen
end

function game:fillTile()
	local locs = search(self.W.map, self.cursor:unpack())
	for v, _ in pairs(locs) do
		local x, y = fromv(v)
		self.W.map:set(1, x, y, self.cursor.tile)
	end
end

function game:moveCursor(v, rpt)
	if self.tilepick then
		self.cursor.tile = misc.clamp(self.cursor.tile + v.x, 1, 4)
		return
	end
	self.cursor:add_(v)
	if love.keyboard.isDown('z') then
		self:placeTile()
	elseif love.keyboard.isDown'x' then
		self:removeTile()
	end
	if rpt and rpt > 0 then
		self:moveCursor(v, rpt-1)
	end
	if rpt == nil and love.keyboard.isDown('lshift', 'rshift') then
		self:moveCursor(v, 1)
		return
	end
end

return game
