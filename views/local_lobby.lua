local class    = require 'gabe.class'
local gs       = require 'gamestate'
local text     = require 'gfx.text'
local prompts  = require 'gfx.prompts'
local misc     = require 'misc'

local lobby = class(...)
class.mixin(lobby, require 'views.lobby')

function lobby:init(last, gamemode)
	self:initLobby(gamemode, 'views.assign_controllers')
end

function lobby.load()
	lobby.loadLobby()
end

function lobby:tick(dt)
	self:tickLobby()
end

local function pressStart(self)
	love.graphics.push()
	love.graphics.translate(250, 240)
	local msg = "Press  A to play!"
	if not self.msg_w then
		love.graphics.push("all")
		love.graphics.setScissor(0, 0, 1, 1)
		self.msg_w = text.chars(msg, function(t, s, i, x, y)
			y = y+(misc.osc(1, 1, i*math.pi*.02)*4)
			if s == "A" then
				if S.input_method == "gamepad" then
					return nil, prompts.drawButton("a", x, y)
				else
					return nil, prompts.drawKey("x", "X", x, y+4)
				end
			end
			local w = A.bbattle:getWidth(s)*2
			return t:add(s, x, y, 0, 2, 2), w, 0
		end, 0, -10)
		love.graphics.pop()
	end

	self.msg_w = text.chars(msg, function(t, s, i, x, y)
		y = y+(misc.osc(1, 1, i*math.pi*.02)*4)
		if s == "A" then
			if S.input_method == "gamepad" then
				return nil, prompts.drawButton("a", x, y)
			else
				return nil, prompts.drawKey("x", "X", x, y+4)
			end
		end
		local w = A.bbattle:getWidth(s)*2
		return t:add(s, x, y, 0, 2, 2), w, 0
	end, self.msg_w*-.5, -10)
	love.graphics.pop()
end

function lobby:draw()
	self:drawLobby(pressStart)
end

local function start_game(self)
	--A.controls = {"all"}
	local players = {}
	for i=1, #A.controls do
		players[i] = "local."..tostring(i)
	end
	if #A.controls == 1 then
		players = {"local"}
	end
	gs.xfer("views.transitions.wipe_right", self.gameMode, players, unpack(self.gameArgs))
end

function lobby:keypressed(k)
	if k == 'escape' then
		self.escape:escape(function()
			return love.keyboard.isDown('escape')
		end)
	end
end

function lobby:keyreleased(k)
	if k == 'x' then
		start_game(self)
	end
end

function lobby:gamepadpressed(gp, btn)
	if btn == "b" or btn == "select" then
		self.escape:escape(function()
			return gp:isGamepadDown(btn)
		end)
	end
end

function lobby:gamepadreleased(gp, btn)
	if btn == "a" then
		start_game(self)
	end
end

return lobby
