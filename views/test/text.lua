local class = require 'gabe.class'

local test = class(...)

function test:init()
end

function test.load()
end

function test:ui()
	if imgui.Begin(class.xtype(self), false, {"AlwaysAutoResize"}) then
		imgui.Text(class.xtype(self))
	end
	imgui.End()
end

function test:update()
	self:ui()
end

function test:draw()
end

return test
