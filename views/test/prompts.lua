local class  = require 'gabe.class'
local assman = require 'assman'
local text   = require 'gfx.text'
local prompts   = require 'gfx.prompts'
local colors = require 'gfx.colors'
local anim   = require 'gfx.anim'

local test = class(...)

function test:init()
end

function test.load()
	assman.load.bmfont("bbattle")
	assman.load.bmfont("openbm")
	A.prompts = anim.new("prompts.png")
end

function test:ui()
	if imgui.Begin(class.xtype(self), false, {"AlwaysAutoResize"}) then
		imgui.Text(class.xtype(self))
	end
	imgui.End()
end

function test:update()
	self:ui()
end

local function btn(...)
	return prompts.drawKey(...)
end

local __btns = {} 
for i, b in ipairs {
	"dpup", "dpright", "dpdown", "dpleft",
	"a", "b", "x", "y",
    "lstick", "rstick",
    "rt", "rb", "lt", "lb",
    "l3", "r3"} do
	__btns[b] = i
end
local function gp(key, x, y)
	love.graphics.setColor(255, 255, 255)
	A.prompts:drawFrame(1, __btns[key] or 1, x, y)
	return 12+3, 0
end

function test:draw()
	local chars = "abcdefghijklmnopqrstuvwxyz"
	love.graphics.push()
	love.graphics.translate(10, 10)
	for m in chars:gmatch(".") do
		--love.graphics.translate(btn(string.lower(m) , string.upper(m), 0, 0))
	end
	love.graphics.pop()

	local fnt = love.graphics.getFont()
	local h = fnt:getHeight()
	love.graphics.push()
	love.graphics.translate(10, 10)
	love.graphics.translate(0, h+2)

	local keys = {"return", "lshift", "rshift", "space"}
	for _, m in ipairs(keys) do
		m = string.upper(m)
		local fnt = love.graphics.getFont()
		local w = fnt:getWidth(m)
		local h = fnt:getHeight()
		if love.keyboard.isDown(string.lower(m)) then
			love.graphics.setColor(200, 200, 200)
			love.graphics.print(m, 0, 1)
			love.graphics.setLineStyle('rough')
			love.graphics.rectangle('line', -1, -1, w+2, h+2, 1)
			love.graphics.translate(w+4, 0)
		else
			love.graphics.setColor(255, 255, 255)
			love.graphics.print(m, 0, 0)
			love.graphics.setLineStyle('rough')
			love.graphics.rectangle('line', -1, -1, w+2, h+2, 1)
			love.graphics.translate(w+4, 0)
		end
	end
	love.graphics.pop()
	local msg = "hey buddy I love you\n"..
	"press  {w} {a} {s} {d} to wasd around\n"..
	"thanks  {w} man great\n"..
	"also btw  {1} you can also  {lshift} {w} ty\n"..
	"dpad ←→↑↓ woah\n" ..
	"Gamepad!\n"..
	"{gp:dpup} {gp:dpleft} {gp:dpdown} {gp:dpright}\n"..
	"{gp:a} {gp:b} {gp:x} {gp:y}\n"..
	"{gp:lstick} {gp:rstick} {gp:l3} {gp:r3}\n"..
	"{gp:lt} {gp:lb} {gp:rb} {gp:rt}\n"

	A.bbattle:setLineHeight(1.2)
	text.words(msg, function(t, str, i, x, y)
		if string.match(str, "^%b{}$") then
			local str = str:sub(2, -2)
			if str:match("^gp:") then
				local str = str:sub(4)
				local w, h = gp(str, x, y)
				return nil, w-2, h
			end
			local w, h = btn(str:lower(), str:upper(), x, y)
			return nil, w-2, h
		else
			return t:add(str, x, y)
		end
	end, 10, 60)
end

return test
