local class = require 'gabe.class'
local fun   = require 'fun'

local node = class((...)..".node")

local tween_i = 1
function node:nodeInit()
	self.showme  = true
	self.dead    = false
	self.disable = false
	self.label = self.prefix .. " " .. tween_i
	tween_i = tween_i + 1
end

function node:nodeUI(fn)
	local view, close = imgui.Begin(self.label.."###"..tostring(self), true, {"AlwaysAutoResize"})
	if view then
		local new_l, ok, _
		ok, new_l = imgui.InputText("label", self.label, 25, {"EnterReturnsTrue"})
		if ok then self.label = new_l end

		_, self.disable = imgui.Checkbox("disable", self.disable)
		imgui.SameLine()
		imgui.Dummy(70, 0)
		imgui.SameLine()
		if imgui.Button("delete") then
			local b = love.window.showMessageBox("Confirm?", "Do you really want to delete "..self.label.."?", {"Yes", "Cancel", escapebutton=2})
			if b == 1 then
				self.dead = true
			end
		end
		imgui.Separator()

		fn()

	end
	if not close then self.showme = false end
	imgui.End()
end

node.update = function() end
node.apply  = function() end
node.after  = function() end
node.ui     = function(self) self:nodeUI(function() imgui.Text("TODO") end) end

local tweenNode = class((...)..".tweenNode")
tweenNode.prefix = "tween"
class.mixin(tweenNode, node)

function tweenNode:init()
	self:nodeInit()

	self.fn = "sin"
	self.offset = 0
	self.period = 1
	self.scale = 0
	self.angle = 0
	self.time  = 0
	self.dx, self.dy = 0, 0
	self.show = {}
end

function tweenNode:update(dt)
	if self.period == 0 then
		dt = 0
	else
		dt = dt/self.period
	end
	self.time = self.time+dt
end

local oscillators = {
	ramp = function(t)
		return (t%1)*2-1
	end,
	tri = function(t)
		t = (t%1)*2
		if t > 1 then
			t = 2-t
		end
		return (t*2)-1
	end,
	sin = function(t)
		return math.sin(t*math.pi*2)
	end,
	noise = function(t)
		return love.math.noise(t) * 2 - 1
	end,
	random = function(t)
		local rng = love.math.newRandomGenerator(math.floor(t*30))
		return rng:random() * 2 - 1
	end
}

function tweenNode:apply(img, q, x, y, r, sx, sy, ox, oy)
	if not self.disable then
		local fn = oscillators[self.fn]
		assert(fn)
		local t = fn(self.time+self.offset)

		x, y = x+self.dx*t, y+self.dy*t
		r     = r + self.angle*t
		sx, sy = sx + self.scale*t, sy + self.scale*t
	end

	return img, q, x, y, r, sx, sy, ox, oy
end

local funs = {
	"sin",
	"tri",
	"ramp",
	"noise",
	"random"
}

function tweenNode:ui()
	self:nodeUI(function()
		local box_i, _ = fun.index_of(self.fn, funs) or 1, nil
		_, box_i = imgui.Combo("wave", box_i, funs, #funs)
		self.fn = funs[box_i]
		_, self.offset = imgui.SliderFloat("offset", self.offset, 0, 1)
		_, self.period = imgui.DragFloat("period", self.period, .001)
		for i, ctrl in ipairs {"position", "scale", "angle"} do
			msg = ctrl .. "##"..tostring(i)
			_, self.show[ctrl] = imgui.Checkbox(msg, self.show[ctrl])
			--if imgui.SmallButton(msg) then
			--	self.show[ctrl] = not self.show[ctrl]
			--end
			imgui.SameLine()
		end
		imgui.NewLine()
		if self.show.position then
			_, self.dx, self.dy = imgui.DragFloat2("position", self.dx, self.dy)
		end
		if self.show.scale then
			_, self.scale = imgui.DragFloat("scale", self.scale, .01)
		end
		if self.show.angle then
			_, self.angle = imgui.DragFloat("angle", self.angle, 1/32)
		end
	end)
end

local shaderNode = class((...)..".shaderNode")
shaderNode.prefix = "shader"
class.mixin(shaderNode, node)


function shaderNode:init()
	self:nodeInit()
	self.shader = love.graphics.newShader(require'shaders.silhouette')
	--self.shader:send("level", 1)
end

function shaderNode:apply(...)
	if self.shader then
		love.graphics.setShader(self.shader)
	end
	return ...
end

function shaderNode:after()
	love.graphics.setShader()
end

local test = class(...)

function test:init()
	self.fname = "male_archer.png"
	self.timescale = 1
	self.img = love.graphics.newImage("assets/sprites/"..self.fname)
	self.img:setFilter("nearest", "nearest")
	self.anim_i = 1
	self.anims = {}
	self.nodes = {}

	self.cellw, self.cellh = self.img:getWidth(), self.img:getHeight()
	love.window.setMode(800, 600, {resizable = true})
	A.lbox.force_scale = 3
	A.lbox:recalculate()
end

function test.load()
end

function test:ui()
	if imgui.Begin("sprite", false, {"AlwaysAutoResize"}) then
		local _, ok, new_fname

		if self.err then
			imgui.Text(self.err)
		end

		ok, self.fname = imgui.InputText("file", self.fname, 255, {"EnterReturnsTrue"}, nil)
		if ok then
			local err
			ok, err = pcall(function()
				self.img = love.graphics.newImage("assets/sprites/"..self.fname)
				self.cellw = self.img:getWidth()
				self.cellh = self.img:getHeight()
				self.img:setFilter("nearest", "nearest")
				self.anims = {}
			end)
			if not ok then
				self.err = err:gsub("^[^:]+:[^:]+: ", "")
			else
				self.err = nil
			end
		end

		if not love.filesystem.exists("/assets/"..self.fname) then
			local fnames = love.filesystem.getDirectoryItems("/assets/")
			for i, name in ipairs(fnames) do
				if i > 4 then break end
				if string.match(name, self.fname) then
					imgui.Text(name)
				end
			end
		end

		_, self.cellw, self.cellh = imgui.DragInt2("cellSize", self.cellw, self.cellh)
		local cols, rows = math.floor(self.img:getWidth() / self.cellw), math.floor(self.img:getHeight() / self.cellh)
		imgui.Text("rows: " .. rows)
		imgui.Text("cols: " .. cols)


		_, self.anim_i = imgui.SliderInt("current animation", self.anim_i, 1, rows, "%f")
		local anim = self.anims[self.anim_i] or {lastFrame = cols, frameDuration = 96}
		_, anim.frameDuration = imgui.DragInt("frameDuration", anim.frameDuration)
		_, anim.lastFrame = imgui.SliderInt("lastFrame", anim.lastFrame, 1, cols)
		self.anims[self.anim_i] = anim

		if imgui.Button("Copy config") then
			self.err = "config export: TODO"
		end

	end
	imgui.End()

	if imgui.Begin("nodes", false, {"AlwaysAutoResize"}) then
		local ok, _
		if imgui.Button("Add node") then
			table.insert(self.nodes, tweenNode.new())
		end
		for _, node in ipairs(self.nodes) do
			if imgui.Selectable(node.label, node.showme) then
				node.showme = not node.showme
			end
		end
	end
	imgui.End()

	if imgui.Begin("window", false, {"AlwaysAutoResize"}) then
		local new_scale
		local ok, _
		_, self.timescale = imgui.DragFloat("timescale", self.timescale, .01, .1, 4)
		self.timescale = math.min(math.max(.1, self.timescale), 4)
		ok, new_scale = imgui.DragFloat("canvas scale", A.lbox.scale, .25)
		if ok then
			A.lbox.force_scale = new_scale
			A.lbox:recalculate()
		end
		ok, A.lbox.perfect = imgui.Checkbox("pixel perfect?", A.lbox.perfect)
		if ok then
			A.lbox:recalculate()
		end
	end
	imgui.End()

	for _, node in ipairs(self.nodes) do
		if node.showme then
			node:ui()
		end
	end
end

function test:update(dt)
	dt = dt * self.timescale
	self:ui()
	for i = #self.nodes, 1, -1 do
		if self.nodes[i].dead then
			table.remove(self.nodes, i)
		end
	end
	for _, node in ipairs(self.nodes) do
		node:update(dt)
	end
end

function test:draw()
	love.graphics.clear(127, 127, 127)
	local qy = (self.anim_i-1) * self.cellh
	local frameDuration = self.anims[self.anim_i].frameDuration
	local len = self.anims[self.anim_i].lastFrame * frameDuration
	local frame_i = math.floor((love.timer.getTime()*1000 % len) / frameDuration)
	local qx = (frame_i) * self.cellw
	local q = love.graphics.newQuad(qx, qy, self.cellw, self.cellh, self.img:getDimensions())
	local img = self.img

	do
		local HW, HH = A.lbox:dimensions()
		HW = HW/2
		HH = HH/2
		local x, y = HW, HH
		local r = 0
		local sx, sy = 1, 1
		local ox, oy = self.cellw*.5, self.cellh*.5
		for _, node in ipairs(self.nodes) do
			assert(img)
			assert(q)
			assert(x)
			img, q, x, y, r, sx, sy, ox, oy = node:apply(img, q, x, y, r, sx, sy, ox, oy)
		end

		love.graphics.draw(img, q, x, y, r, sx, sy, ox, oy)

		for i=#self.nodes, 1, -1 do
			local node = self.nodes[i]
			node:after()
		end
	end
end

return test
