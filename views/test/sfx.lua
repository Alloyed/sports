local class = require 'gabe.class'

local test = class(...)

function test:init()
end

function test.load()
	A.target_hit = love.audio.newSource("assets/sfx/time_up.ogg")
	A.target_hit:setVolume(.2)
end

function test:ui()
	if imgui.Begin(class.xtype(self), false, {"AlwaysAutoResize"}) then
		imgui.Text(class.xtype(self))
	end
	imgui.End()
end

local semis = {0, 2, 5, 7}
function test:update(dt)
	self:ui()
	self.t = (self.t or 0) + dt
	if self.t > .4 then
		self.t = self.t - .4
		self.combo = ((self.combo or 0) + 1) % 4
		local a = A.target_hit:clone()
		a:setPitch(1 + math.min(12, semis[self.combo+1])/12)
		a:play()
	end
end

function test:draw()
end

return test
