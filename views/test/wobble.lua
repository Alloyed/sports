local class = require 'gabe.class'
local misc  = require 'misc'
local Text  = require 'gfx.text'
local gifcat = require 'gifcat'

local test = class(...)

function test:init()
	self.gif = gifcat.newGif("text.gif", 1920/4, 1080/4, 60)
	self.gif:onFinish(function()
		print("done")
	end)
	self.t = love.timer.getTime()
end

function test.load()
	gifcat.init()
end

function test:ui()
	if imgui.Begin(class.xtype(self), false, {"AlwaysAutoResize"}) then
		imgui.Text(class.xtype(self))
	end
	imgui.End()
end

function test:update(dt)
	--self:ui()
	if self.gif then
		local cv = A.lbox.cv1[1]
		self.gif:frame(cv)
	end
	if self.gif and love.timer.getTime() - self.t > 2 then
		print("close")
		self.gif:close()
		self.gif = nil
	end
	gifcat.update(dt)
end

function love.quit()
	gifcat.close()
end

function test:draw()
	Text.chars("WOBBLE", function(t, str, i, x, y)
		local r = misc.osc(2, 255, 1-i/4)
		local g = misc.osc(2, 255, 2-i/4)
		local b = misc.osc(2, 255, 3-i/4)
		local a = 255
		return t:add({{r, g, b, a}, str}, x, y+misc.osc(1, 2, i/4)-1)
	end, 400, 40)
	Text.words("it was the {r}best of times.\nIt was the worst {A} of times",
	function(t, str, i, x, y)
		y = y + misc.osc(2, 2, i/10)
		local m = str:match"^%b{}"
		if m then
			if m == "{r}" then
				str = str:gsub(m, "")
				return t:add({{255, 0, 0}, str}, x, y)
			elseif m == "{A}" then
				love.graphics.circle('line', x+5, y+5, 5)
				return nil, 10, 0
			end
		end
		return t:add(str, x, y)
	end,
	40, 40)
	Text.words("The quick brown fox jumps over the lazy dog.\n0123456789", function(t, str, i, x, y)
		return t:add(str, x, y)
	end, 40, 80)
	Text.chars("WOBBLE2", Text.fx.wobble(), 400, 60)
	Text.chars("Shaking", Text.fx.shake(), 400, 80)
	Text.chars("PULSE",   Text.fx.pulse(), 400, 100)
end

return test
