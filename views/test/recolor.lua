local class = require 'gabe.class'
local recolor = require 'gfx.recolor'

local test = class(...)

function test:init()
	local fname = "assets/sprites/male_archer.png"
	self.img, self.palette = recolor.generate(fname, "assets/teamcolors.png")
	self.cool = love.graphics.newImage(fname)
end

function test.load()
end

function test:ui()
	if imgui.Begin(class.xtype(self), false, {"AlwaysAutoResize"}) then
		imgui.Text(class.xtype(self))
	end
	imgui.End()
end

function test:update()
	self:ui()
end

function test:draw()
	love.graphics.clear(100, 40, 40)
	love.graphics.push()
	love.graphics.scale(4)
	love.graphics.draw(self.img, 0, 0)
	recolor.apply(self.palette, 0)
	love.graphics.draw(self.img, 0, self.img:getHeight())
	love.graphics.setShader()
	recolor.apply(self.palette, 1)
	love.graphics.draw(self.img, 0, 2*self.img:getHeight())
	love.graphics.setShader()

	love.graphics.draw(self.palette, 0, 50)
	love.graphics.pop()
end

return test
