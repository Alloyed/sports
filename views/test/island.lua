local class = require 'gabe.class'
local assman = require 'assman'
local fun    = require 'fun'
local misc   = require 'misc'

local test = class(...)

function test:init()
	local W, H = A.spelunking.bg_tiles:getDimensions()
	self.quads = {}
	for iy = 0, H-1, 16 do
		for ix = 0, W-1, 16 do
			table.insert(self.quads, love.graphics.newQuad(ix, iy, 16, 16, W, H))
		end
	end
	self.heatmaps = {}
	self.cam_x = 0
	self.start_x = 0
	self.end_x = 0
	self:gen(0)
end

function test:gen(start_x)
end

function test.load()
	assman.load.image("spelunking:map/bg_tiles")
end

function test:ui()
end

function test:update(dt)
	local W, _ = A.lbox:dimensions()
	self:ui()
	if self.cam_x+W+128 - self.end_x > 0 then
		self:gen(self.end_x)
	end
	if self.cam_x - self.start_x < W then
		self:gen(self.start_x - W)
	end
	self.cam_x = self.cam_x + 60 * dt
end

function test:draw()
	imgui.Begin(class.xtype(self), false, {})
	local W, H = A.lbox:dimensions()
	love.graphics.clear(35, 4, 3)

	local x, y = 100, 100
	x = imgui.CCFloat("x", 78, 100, 200, 100)
	love.graphics.circle('line', x, y, 32)

	imgui.End()
end

return test
