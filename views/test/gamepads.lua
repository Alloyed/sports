local class = require 'gabe.class'

local test = class(...)

function test:init()
end

function test.load()
end

function test:ui()
	if imgui.Begin(class.xtype(self), false, {"AlwaysAutoResize"}) then
		imgui.Text(class.xtype(self))
	end
	imgui.End()
end

function test:update()
	self:ui()
end

function test:draw()
	local joysticks = love.joystick.getJoysticks()
	love.graphics.print(require'inspect'(joysticks))
	for i, joy in ipairs(joysticks) do
		if not joy:getName():match("Motion Sensor") then
			love.graphics.print(tostring(joy:isGamepad()), 0, i*40)
			love.graphics.print(tostring(joy:getName()), 70, i*40)
			love.graphics.print(tostring(joy:getGUID()), 100, i*40+20)

			local x, y = joy:getGamepadAxis("leftx"), joy:getGamepadAxis("lefty")
			love.graphics.circle('line', 400, i*40, 10)
			love.graphics.circle('line', 400+x*10, i*40+y*10, 2)
			local x, y = joy:getGamepadAxis("rightx"), joy:getGamepadAxis("righty")
			love.graphics.circle('line', 440, i*40, 10)
			love.graphics.circle('line', 440+x*10, i*40+y*10, 2)
		end
	end
end

function test:gamepadpressed(gp, btn)
	if gp:getName():match("Motion Sensor") then return end 
	print(gp, btn)
end

function test:gamepadaxis(gp, btn, d)
	if gp:getName():match("Motion Sensor") then return end 
	print(gp, btn, d)
end

return test
