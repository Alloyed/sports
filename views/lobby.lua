local class         = require 'gabe.class'
local escape_button = require 'ui.escape'
local text          = require 'gfx.text'
local colors        = require 'gfx.colors'
local keys          = require 'keys'
local prompts       = require 'gfx.prompts'
local template      = require 'gfx.ui_template'

local lobby = class(...)
class.mixin(lobby, require 'views.menu')

function lobby:initLobby(gamemode, escape_to)
	self:initMusic()
	if not gamemode then
		self.gameMode = S.playlist[S.playlist.i][1]
		self.gameArgs = {unpack(S.playlist[S.playlist.i], 2)}
		S.playlist.i = (S.playlist.i % #S.playlist) + 1
	else
		self.gameMode = gamemode
		self.gameArgs = {}
	end
	assert(pcall(require, self.gameMode))
	self.escape = escape_button.new(escape_to)
end

function lobby.loadLobby()
	A.tutorials = require 'tutorial_data'
	lobby.loadMusic()
	prompts.load()
end

function lobby:tickLobby(dt)
	local video = A.tutorials[self.gameMode].video
	if video:isPlaying() then return end
	video:rewind()
	video:play()
end

local widgets = {}

function widgets.video(video, x, y)
	local scale = .25*A.lbox.scale
	love.graphics.draw(video, x, y, 0, scale, scale)
end

function widgets.title(name, ww)
	love.graphics.push()
	love.graphics.scale(2)
	love.graphics.printf(name, 0, 20/2, ww/2, 'center')
	love.graphics.pop()
end

function widgets.tutorial(msg, _, _, w, h)
	love.graphics.getFont():setLineHeight(2)
	text.words(msg, function(t, str, i, x, y)
		if string.match(str, "^%b{}$") then
			str = str:sub(2, -2)
			if str == "reset" then
				love.graphics.getFont():setLineHeight(1.2)
				return nil, 0, -23
			elseif str == "move" then
				if S.input_method == "gamepad" then
					return nil, prompts.drawButton("lstick", x, y)
				else
					local s = ("%s%s%s%s"):format(keys.w, keys.a, keys.s, keys.d)
					return nil, prompts.drawKey(s, string.upper(s), x, y)
				end
			elseif str == "fire" then
				if S.input_method == "gamepad" then
					return nil, prompts.drawButton("rt", x, y)
				else
					return nil, prompts.drawKey("LMB", "LMB", x, y)
				end
			end
		elseif string.match(str, "^%b[]") then
			local color = str:match("^(%b[])")
			color = color:sub(2, -2)
			if tonumber(color) then color = tonumber(color) end
			str = str:gsub("%b[]", "")
			return t:add({colors[color], str}, x, y)
		else
			return t:add(str, x, y)
		end
	end, 260, 75)
end

function lobby:postDraw()
	local data = A.tutorials[self.gameMode]
	A.lbox:draw()
	widgets.video(data.video, A.lbox:screen2win(10, 75))
end

function lobby:drawLobby(fn)
	local data = A.tutorials[self.gameMode]
	template.draw(function(ww, hh)
		self.escape:draw(2, 2)
		widgets.title(data.title, ww)
		widgets.tutorial(data.description)
		fn(self, ww, hh)
	end)
end

return lobby
