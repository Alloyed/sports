local class    = require 'gabe.class'
local template = require 'gfx.ui_template'
local gs       = require 'gamestate'
local client   = require 'net.client'
local suit     = require 'suit'

local pickroom = class(...)

function pickroom:init()
	if not A.client or A.client.connected == false then
		if A.local_server then
			A.client = client.new("localhost")
		else
			A.client = client.new("ssh.alloyed.me")
			--A.client = client.new("192.168.169.110")
		end
	end
	self.nick = Settings.nick or "Gaston"
	self.nickBox = {text = self.nick}
	if A.submitroom then
		self:submit()
	end
	love.keyboard.setKeyRepeat(true)
end

function pickroom:exit()
	love.keyboard.setKeyRepeat(false)
end

function pickroom.update() end

function pickroom:suit(dt)
	if A.client.connected and self.should_submit then
		Settings.nick = self.nick
		A.client:msg("joinRoom", self.nick)
		self.should_submit = false
		S.events:pub("ding", {"wew"})
	end
	--local ox, oy = imgui.Position("pos", 190, 100)
	local ox, oy = 190, 80
	suit.layout:reset(ox, oy, 10, 10)
	suit.layout:row(100, 16)
	if A.client.err then
		suit.Label("Error: "..A.client.err, suit.layout:row())
	elseif not A.client.connected then
		suit.Label("Connecting...", suit.layout:row())
	else
		suit.Label("Nickname:", suit.layout:row())
		local o = suit.Input(self.nickBox, suit.layout:row())
		self.nick = self.nickBox.text
		if o.submitted then
			self:submit()
		end
		if suit.Button("join room", suit.layout:row()).hit then
			self:submit()
		end
	end
end

function pickroom:submit()
	self.should_submit = true
end

function pickroom:recv()
end

function pickroom:draw()
	template.draw(function(w, h)
		love.graphics.translate(-5, -5)
		suit.draw()
	end)
end

function pickroom:keypressed(k, ...)
	if k == "escape" then
		gs.switch("views.mainmenu")
	end
	suit.keypressed(k, ...)
end

function pickroom:textinput(...)
	suit.textinput(...)
end

return pickroom
