local class    = require 'gabe.class'
local fun      = require 'fun'
local gs       = require 'gamestate'
local text     = require 'gfx.text'
local keys     = require 'keys'
local escape   = require 'ui.escape'
local prompts  = require 'gfx.prompts'
local misc     = require 'misc'
local template = require 'gfx.ui_template'
local assman   = require 'assman'
local recolor  = require 'gfx.recolor'

local screen = class(...)
local nicks = {
	"Gaston",
	"Motavio",
	"Stan",
	"Celery",
	"Sandman",
	"Prompto",
	"Cornholio",
	"Doubleday",
	"Wrinkles",
	"Giorgio",
	"Mark",
	"Valentino",
	"Blofeld",
	"Mustard",
	"Ideaguy",
	"Yocheved",
	"Yorick",
	"Rosencrantz",
	"Guildenstern",
	"Hamtaro",
	"Caillou",
}
function screen:init()
	A.controls    = {}
	A.local_nicks = {}
	self.escape = escape.new("views.mainmenu")
end

function screen.load()
	prompts.load()
	recolor.load()
	assman.load.anim("male_archer", function(anim)
		A.archer = anim
		anim.img, anim.palette = recolor.generate(anim.img, "assets/teamcolors.png")
	end)
end

local pulse = text.fx.pulse()
function screen:draw()
	template.draw(function(ww, hh)
		self.escape:draw(2, 2)

		for i=0, 3 do
			if A.controls[i+1] == nil then
				--love.graphics.print("Player " .. (i+1), i*120+4, 50)
				local t, w = text.chars_t("Player "..(i+1), text.fx.none, 0, 0)
				love.graphics.draw(t, (i+.5)*120+4-w*.5, 50)
				local msg = "A/X to start!"
				if fun.index_of("m", A.controls) then
					msg = "A to start!"
				end
				love.graphics.printf(msg, i*120+4, 74, 120, 'center')
				recolor.apply_grey(A.archer.palette, 0)
				A.archer:draw(1, i*120, 100-20, 0, 2, 2)
			else
				local t, w = text.chars_t(A.local_nicks[i+1], pulse, 0, 0)
				love.graphics.draw(t, (i+.5)*120+4-w*.5, 50)
				local msg = "B to cancel"
				if A.controls[i+1] == "m" then
					msg = "Z to cancel"
				end
				love.graphics.printf(msg, i*120+4, 74, 120, 'center')
				recolor.apply(A.archer.palette, i+1)
				A.archer:draw(2, i*120, 100-20, 0, 2, 2)
			end
			love.graphics.setShader()
		end

		love.graphics.push()
		love.graphics.translate(ww*.5, hh-30)
		if #A.controls < 1 then
			love.graphics.printf("Waiting for players", -ww*.5, 0, ww, 'center')
			love.graphics.pop()
			return
		end
		local msg = "Press A to play!"
		if not self.msg_w then
			self.msg_w = A.bbattle:getWidth(msg)*2
		end

		self.msg_w = text.chars(msg, function(t, s, i, x, y)
			y = y+(misc.osc(1, 1, i*math.pi*.02)*4)
			if s == "A" then
				if S.input_method == "gamepad" then
					return nil, prompts.drawButton("a", x, y)
				else
					return nil, prompts.drawKey("x", "X", x, y+4)
				end
			end
			local w = A.bbattle:getWidth(s)*2
			return t:add(s, x, y, 0, 2, 2), w, 0
		end, self.msg_w*-.5, -10)
		love.graphics.pop()
	end)
end

local events = {}
function events.confirm(self, pad)
	if fun.index_of(pad, A.controls) then
		A.VPoints = {}
		for i=1, #A.controls do
			A.VPoints[i] = 0
		end
		gs.switch("views.local_lobby")
		S.events:pub("ding", {})
	else
		if #A.controls >= 4 then
			return
		end
		table.insert(A.controls, pad)
		A.local_nicks[#A.controls] = nicks[love.math.random(#nicks)]
	end
end

function events.addbot(self)
	if #A.controls >= 4 then
		return
	end
	table.insert(A.controls, "bot")
	A.local_nicks[#A.controls] = "R.O.B."
end

function events.removebot(self)
	local idx = fun.index_of("bot", A.controls)
	if idx then
		table.remove(A.controls, idx)
	end
end

function events.cancel(self, pad, btn)
	local idx = fun.index_of(pad, A.controls)
	if idx then
		table.remove(A.controls, idx)
	elseif pad == "m" then
		self.escape:escape(function()
			return love.keyboard.isDown(btn)
		end)
	else
		self.escape:escape(function()
			return pad:isGamepadDown(btn)
		end)
	end
end

function screen:gamepadpressed(pad, btn)
	if btn == 'b' then
		events.cancel(self, pad, btn)
	end
end

function screen:gamepadreleased(pad, btn)
	if btn == 'a' then
		events.confirm(self, pad)
	end
end

function screen:keypressed(k)
	if keys.cancel == k then
		events.cancel(self, "m", k)
	elseif keys.escape == k then
		events.cancel(self, "m", k)
	end
end

function screen:keyreleased(k)
	if keys.confirm == k then
		events.confirm(self, "m")
	elseif k == "b" then
		events.addbot(self)
	elseif k == 'n' then
		events.removebot(self)
	end
end
return screen
