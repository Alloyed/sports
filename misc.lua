local vec = require 'vec'

local misc = {}

--- shallow copy.
function misc.copy(t)
	local new = {}
	for k, v in pairs(t) do new[k] = v end
	return new
end

-- fast array copy. Respects __len and __index, but not __ipairs.
function misc.icopy(t)
	local new = {}
	for i=1, #t do
		new[i] = t[i]
	end
	return new
end

-- remove all elements from table
function misc.iclear_(t)
	for k, _ in pairs(t) do
		t[k] = nil
	end
	return t
end

-- remove all array elements from table
function misc.iclear_(t)
	for i=#t, 1, -1 do
		t[i] = nil
	end
	return t
end

-- prepends b into a. implemented as a shift, then copy
function misc.prepend_(a, b)
	local shift   = #b
	local old_len = #a
	for i=old_len, 1, -1 do
		a[i+shift] = a[i]
	end
	for i=1, shift do
		a[i] = b[i]
	end
	return a
end

-- appends b into a.
function misc.append_(a, b)
	local alen = #a
	for i=1, #b do
		a[alen+i] = b[i]
	end

	return a
end

-- merge b into a. shallow.
function misc.merge_(a, b)
	local t = a
	for k, v in pairs(b) do t[k] = v end
	return t
end

-- merge two k/v maps. shallow.
function misc.merge(a, b)
	local t = {}
	for k, v in pairs(a) do t[k] = v end
	for k, v in pairs(b) do t[k] = v end
	return t
end

function misc.merge_(a, b)
	local t = {}
	for k, v in pairs(a) do t[k] = v end
	for k, v in pairs(b) do t[k] = v end
	return t
end

-- safemerge. like normal merge but keys can't get overwritten
function misc.safemerge(a, b)
	local t = {}
	for k, v in pairs(a) do t[k] = v end
	for k, v in pairs(b) do assert(t[k] == nil) t[k] = v end
	return t
end

function misc.safemerge_(a, b)
	local t = a
	for k, v in pairs(a) do t[k] = v end
	for k, v in pairs(b) do assert(t[k] == nil) t[k] = v end
	return t
end

function misc.deepmerge(a, b)
	local t = {}
	for k, v in pairs(a) do t[k] = v end
	for k, v in pairs(b) do
		if t[k] == 'table' then
			t[k] = misc.deepmerge(t[k], v)
		else
			t[k] = v
		end
	end
	return t
end

function misc.deepmerge_(a, b)
	local t = a
	for k, v in pairs(a) do t[k] = v end
	for k, v in pairs(b) do
		if t[k] == 'table' then
			t[k] = misc.deepmerge(t[k], v)
		else
			t[k] = v
		end
	end
	return t
end

-- turn a vararg into a set
function misc.set(...)
	local t = {}
	for i=1, select('#', ...) do
		t[select(i, ...)] = true
	end
	return t
end

-- turn a table into a list of keys
function misc.keys(t)
	local keys = {}
	for k, _ in pairs(t) do
		table.insert(keys, k)
	end
	return keys
end

-- given two coordinate sets, returns an iterator that returns each point in
-- between them
-- TODO: rewrite as stateless iterator
function misc.bresenham(x1, y1, x2, y2)
	return coroutine.wrap(function()
		local swapXY = math.abs(y2 - y1) > math.abs(x2 - x1)

		if swapXY then
			x1, y1 = y1, x1
			x2, y2 = y2, x2
		end

		if x1 > x2 then
			x1, x2 = x2, x1
			y1, y2 = y2, y1
		end

		local dx    = x2 - x1
		local dy    = math.floor(math.abs(y2 - y1))
		local e     = math.floor(dx / 2)
		local iy    = y1
		local ystep = y1 < y2 and 1 or -1

		for ix=x1, x2 do
			if swapXY then
				coroutine.yield(true, iy, ix)
			else
				coroutine.yield(true, ix, iy)
			end
			e = e - dy
			if e < 0 then
				iy = iy + ystep
				e  = e + dx
			end
		end
		coroutine.yield(nil)
	end)
end

function misc.inside(rect, x, y)
	x, y = vec.unpack(x, y)
	if rect.w then
		return rect.x <= x and rect.x + rect.w >= x and
		       rect.y <= y and rect.y + rect.h >= y
	else
		return rect.x == x and rect.y == y
	end
end

function misc.overlap(x1, w1, x2, w2)
	return x1 < x2+w2 and x2 < x1+w1
end

function misc.center(r)
	return math.floor(.5 + r.x + r.w *.5), math.floor(.5 + r.y + r.h * .5)
end

function misc.aabb(r1, r2)
	return r1.x < r2.x+r2.w and r2.x < r1.x+r1.w and
	       r1.y < r2.y+r2.h and r2.y < r1.y+r1.h
end

-- adds fixed margin to all sides of a rect
function misc.margin(margin, x, y, w, h, ...)
	return x - margin, y - margin, w + margin + margin, h + margin + margin, ...
end

misc.timer = love.timer.getTime
-- returns an oscillating value that depends on the love.timer.getTime() value.
-- range is [0, amplitude or 1]
function misc.osc(period, amplitude, offset)
	period = period or 1
	amplitude = amplitude or 1
	offset = offset or 0
	local t = (offset+misc.timer()) * 2 * math.pi / period
	return (math.sin(t) + 1) * .5 * amplitude
end

function misc.osc_tri(period, amplitude, offset)
	period = period or 1
	amplitude = amplitude or 1
	offset = offset or 0
	local t = (offset+misc.timer()) / period
	t = (t%1)*2
	if t > 1 then
		t = 2-t
	end
	return (t) * amplitude
end

-- round n to the nearest multiple of k
function misc.round(n, k)
	return math.floor((n / k) + .5) * k
end

-- turn a continous function into a stepped function. This behaves like
-- math.round but it floors
function misc.quantize(n, interval)
	return math.floor(n/interval)*interval
end

-- Get math.random like ranges with a deterministic seed value. this means
-- math.random(m, n) == misc.range(math.random(), m, n)
function misc.range(seed, m, n)
	if m == nil then
		return seed
	elseif n == nil then
		return math.ceil(seed*m) -- max
	else
		return math.floor(seed*(n-m)) + m
	end
end

-- returns a number such that values outside the given range get clamped to the
-- closest point inside the range
function misc.clamp(n, bot, top)
	return math.max(bot, math.min(top, n))
end

--- Binary search.
function misc.binsearch(a, prop, target)
	if target == nil then return nil end
	local max = #a
	local min = 1
	while max-min > 1 do
		local i = math.floor((max + min)*.5)
		local v = a[i][prop]
		log:assert(v ~= nil, "prop: %s, loc: %d, val: %s", prop, i, tostring(a[i]))
		if v == target then
			return i, a[i]
		elseif v < target then
			min = i + 1
		elseif v > target then
			max = i - 1
		end
	end

	if a[min][prop] == target then
		return min, a[min]
	elseif a[max][prop] == target then
		return max, a[max]
	else
		return nil
	end
end

function misc.color(r, g, b, a)
	return r, g, b, a
end

-- add an A to an RGB color.
function misc.rgba(c, a)
	return c[1], c[2], c[3], a
end

function misc.avg(a, b)
	return math.floor((a+b)*.5 + .5)
end

function misc.lerp(a, b, t)
	return a + ((b-a)*t)
end

function misc.lerpColor(a, b, t)
	local a4 = a[4] or 255
	local b4 = b[4] or 255
	return misc.lerp(a[1], b[1], t), misc.lerp(a[2], b[2], t), misc.lerp(a[3], b[3], t), misc.lerp(a4, b4, t)
end

function misc.lerpVec(v1, v2, t)
	return misc.lerp(v1.x, v2.x, t), misc.lerp(v1.y, v2.y, t)
end

local TWOPI = math.pi * 2
function misc.diffAngle(a1, a2)
	a1, a2 = a2, a1

	a2 = a2 % TWOPI
	a1 = a1 % TWOPI
	local d = (a2-a1)
	if math.abs((a2+TWOPI)-a1) < math.abs(d) then
		a2 = a2+TWOPI
	end

	if math.abs((a2-TWOPI)-a1) < math.abs(d) then
		a2 = a2-TWOPI
	end

	d = (a2-a1)
	return d
end

function misc.lerpAngle(a1, a2, t)
	a2 = a2 % TWOPI
	a1 = a1 % TWOPI
	local d_orig = math.abs(a2-a1)
	if math.abs((a2+TWOPI)-a1) < d_orig then
		a2 = a2+TWOPI
		d_orig = math.abs((a2+TWOPI)-a1)
	end

	if math.abs((a2-TWOPI)-a1) < d_orig then
		a2 = a2-TWOPI
		d_orig = math.abs((a2-TWOPI)-a1)
	end

	return misc.lerp(a1, a2, t)
end

function misc.empty(t)
	return not next(t)
end

return misc
