#!/bin/bash

# a convenient tmux script

tmux new-session -d -s gaem "rlwrap love . --server"
tmux split-window -v "rlwrap love . --local --nick 'alloyed' --joinroom --mute-music"
tmux split-window -v "rlwrap love . --local --controls bot --nick CL2 --joinroom --mute"

tmux split-window -v "sleep 1s; bash org.sh"
tmux attach-session -t gaem
