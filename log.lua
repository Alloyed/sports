-- Game logger.
-- We shouldn't rely on this to inform users, but it probably should still
-- exist.

local Settings = require 'settings'
local inspect  = require 'inspect'

local log = {}

local function pack(...)
	return {n = select('#', ...), ...}
end

local function fmt(s, ...)
	local args
	if type(s) == 'function' then
		args = pack(s(...))
		s = table.remove(args, 1)
		args.n = args.n - 1
	else
		args = pack(...)
	end

	local i = 0
	return (string.gsub(s, "%%[A-Za-z._+$0-9]+", function(input)
		i = i + 1
		if input:match("^%%%d+%$") then
			i, input = input:match("^%%(%d+)%$(.*)$")
			i = tonumber(i)
			input = "%"..input
		end

		if input == "%_" then
			return tostring(args[i])
		elseif input == "%t" then
			local t = args[i]
			if type(t) ~= 'table' then
				return tostring(t)
			elseif t.totable then
				t = t:totable()
			end
			return inspect(t)
		else
			return string.format(input, args[i])
		end
	end))
end
-- mark as display string. these need i18n eventually, and for non-english
-- builds we'll just slot in a map of english strings -> translated strings
-- _G.strings = {}
function _G.sss(s, ...)
	--_G.strings[s] = _G.strings[s] and _G.strings[s] + 1 or 1
	return fmt(s, ...)
end

local function p(s)
	io.write(s.."\n")
	io.stdout:flush()
end

function log.log(...)
	return p(" LOG:" .. sss(...))
end

function log.warning(...)
	return p("WARN:" .. sss(...))
end

function log.logs(s)
	p(s)
end

function log.dbg(...)
	if Settings.staging then
		error("stray debug line")
	end
	if Settings.debug then
		local info = debug.getinfo(2, 'S')
		return p(info.short_src..":"..info.linedefined..": " .. fmt(...))
	end
end

-- only do fn if verbose mode is on. It's assumed there will be a lot of
-- require'inspect' in here
function log.verbose(...)
	if Settings.verbose then
		local info = debug.getinfo(2, 'S')
		return p(info.short_src..":"..info.linedefined..": " .. fmt(...))
	end
end

function log.assert(a, ...)
	if not a then
		error((...), 2)
	end
end

function log.try(a, ...)
	if not a then
		log.warning("%s", (...))
	end
end

function log.error(...)
	error((...), 2)
end

setmetatable(log, {
	__call = function(t, ...)
		return log.log(...)
	end,
	__index = function(t, k)
		t.verbose("creating level %s", tostring(k))
		t[k] = t.log
		return t[k]
	end
})

return log
