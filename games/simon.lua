local class = require 'gabe.class'
local prompts = require 'gfx.prompts'
local vec   = require 'vec'
local Flux  = require 'flux'

local game = class(...)
class.mixin(game, require 'games.mixins.world')

local sequence = class((...)..".sequence")

function sequence:init(W)
	self.difficulty = 0
	self.sequence = {}
	self:playSequence(W)
end

local choices = {"up", "down", "left", "right"}
function sequence:playSequence(W)
	self.difficulty = self.difficulty + 1
	for i=#self.sequence+1, self.difficulty do
		self.sequence[i] = choices[W.rng:random(#choices)]
	end

	self.play = 1
	self.looking_i = nil
	self.looking_for = nil
	self.size = nil
	local len = #self.sequence
	W.flux:to(self, len*.5, {play = len+.99}):ease("linear"):oncomplete(function()
		self.play = nil
		self.looking_i = 0
		self:nextPrompt(W)
	end)
end

function sequence:nextPrompt(W)
	self.looking_i = self.looking_i + 1
	self.looking_for = self.sequence[self.looking_i] or ""
	local f
	if self.looking_i > 1 then
		self.size = 2
		f = W.flux:to(self, .5, {size = 1})
	end
	if self.looking_i > #self.sequence then
		if f then
			f:oncomplete(function()
				self:playSequence(W)
			end)
		else
			self:playSequence(W)
		end
	end
end

function sequence:tick(dt)
end

local gp = { up = "y", down = "a", right = "b", left = "x" }
local c = string.char
local key = { up = "↑", down = "↓", left = "←", right = "→" }
function sequence:draw1(W, R)
	if self.play then
		local btn = self.sequence[math.floor(self.play)]
		local size = self.play % 1
		size = 1 - math.min(1, size * 2)
		size = 1+ (size * size)
		love.graphics.push()
		love.graphics.translate(100, 100)
		love.graphics.scale(size)
		prompts.drawButton(gp[btn], -8, -8)
		love.graphics.pop()

		love.graphics.push()
		love.graphics.translate(200, 100)
		love.graphics.scale(size)
		local disp = key[btn]
		local w, h = prompts.keyBox(disp)
		prompts.drawKey("", disp, w*-.5, h*-.5)
		love.graphics.pop()
	elseif self.looking_for then
		love.graphics.push()
		love.graphics.translate(150, 150)
		love.graphics.scale(self.size)
		local disp = "?"
		local w, h = prompts.keyBox(disp)
		prompts.drawKey("", disp, w*-.5, h*-.5)
		love.graphics.pop()
	end
end

function game:init(player_config)
	self.flux = Flux.group()
	self.rng  = love.math.newRandomGenerator()
	self:worldInit()
	self.sequence = sequence.new(self)
	self:addItem(self.sequence)
end

function game.load()
	prompts.load()
end

function game:tick(dt)
	self.flux:update(dt)
	self:worldEach("tick", self, dt)
	self:worldClean()
end


function game:pressed(btn)
	local seq = self.sequence
	if seq.looking_for == btn then
		seq:nextPrompt(self)
	end
end

return game
