local class   = require 'gabe.class'
--local vec     = require 'vec'
local colors  = require 'gfx.colors'
local prompts = require 'gfx.prompts'
local lg      = require 'love.graphics'
local keys    = require 'keys'
local misc    = require 'misc'
local Flux    = require 'flux'

local game = class(...)
class.mixin(game, require 'games.mixins.world')

local booklet = class((...)..".booklet")

function booklet:init()
end

function booklet:draw1()
	local dimw, dimh = A.lbox:dimensions()
	lg.setColor(colors.white)
	lg.rectangle('fill', dimw*.66, 0, dimw*33, dimh)
	lg.setColor(colors.black)
	lg.push()
	lg.translate(dimw*.66, 0)
	lg.translate(2, 2) -- margin
	for i=1, 3 do
		lg.print("Strongest Potion", 0, 0) -- + 12
		lg.print("NEWTx2 BAT TOAD", 2, 12) -- + 24
		lg.translate(0, 12*3)
	end
	lg.pop()
end

local mixing_desk = class((...)..".mixing_desk")

function mixing_desk:init()
end

function mixing_desk:draw1()
	local dimw, dimh = A.lbox:dimensions()
	lg.push()
	lg.translate(0, dimh*.75)
	lg.setColor(colors.black)
	lg.rectangle('fill', misc.margin(-2, 0, 0, (dimw*.66), dimh*.25))
	--local boxw, boxh = (dimw*.66) - 4, (dimh*.25) - 4

	lg.setColor(colors.white)

	local x, y = 145, 29 -- imgui.Position("center", 145, 29)
	local xx, yy = 90, 15 -- imgui.Position("right", 90, 15)
	local dx, dy = -24, 0 --imgui.Position("label", -24)
	prompts.drawKey("up", "↑", x+dx, y+dy-yy)
	lg.print("NEWT", x, y-yy)

	prompts.drawKey("down", "↓", x+dx, y+dy+yy)
	lg.print("TOAD", x, y+yy)

	prompts.drawKey("left", "←", x+dx-xx, y+dy)
	lg.print("BAT", x-xx, y)

	prompts.drawKey("right", "→", x+dx+xx, y+dy)
	lg.print("RAT", x+xx, y)
	
	local nx, ny = 285, 4--imgui.Position("next", 285, 4)
	prompts.drawKey("space", "SPACE", nx+(dx*1.75), ny+dy)
	lg.print("POUR", nx, ny)

	lg.pop()
end

local cauldron = class((...)..".cauldron")

function cauldron:init()
	self.filled = 0
	self.color = misc.copy(colors.red)
	self.potion = { newt = 0, bat = 0, toad = 0, rat = 0 }
end

function cauldron:draw1()
	local x, y = imgui.Position("cauldron", 70, 170)
	local r = imgui.Float("rad", 40, .1)
	local sx, sy = imgui.Position("scale", 20, 10)
	sx, sy = sx/20, sy/20

	lg.push()
	lg.translate(x, y)
	lg.scale(sx, sy)
	lg.setColor(self.color)
	lg.circle('fill', 0, 0, r)
	lg.pop()

	lg.push()
	x, y = imgui.Position("beaker", 135, 170)
	local w, h = imgui.Position("rect", 20, 30)
	lg.translate(x, y)
	lg.rectangle('fill', 0, h*(1-self.filled), w, h*self.filled)
	lg.setColor(colors.white)
	lg.rectangle('line', 0, 0, w, h)
	lg.pop()
end

function cauldron:addIngredient(W, ingredient)
	if self.locked then return end
	local newColor = misc.copy(self.color)
	for k, v in pairs(newColor) do
		newColor[k] = misc.clamp(v + (math.random()*80) - 40, 30, 240)
	end
	W.flux:to(self.color, .3, newColor)
	self.potion[ingredient] = self.potion[ingredient] + 1
end

function cauldron:resetPotion(W)
	W.flux:to(self.color, .1, colors.red)
	:oncomplete(function()
		self.potion = {newt = 0, bat = 0, toad = 0, rat = 0}
		self.locked = false
	end)
end

function cauldron:fillBeaker(W)
	self.locked = true
	W.flux:to(self, .5, {filled = 1})
	:oncomplete(function()
		self.filled = 0
		W.customer:receivePotion(W, self.potion)
		self:resetPotion(W)
	end)
end

local customer = class((...)..".customer")

function customer:init()
	self.state = "waiting"
end

function customer:receivePotion(W, pot)
	if pot.bat > 0 and pot.bat == pot.toad and pot.newt == pot.bat * 2 then
		self.state = "good"
	else
		self.state = "bad"
	end
	W.flux:to(self, 2, {}):oncomplete(function()
		self.state = "waiting"
	end)
end

function customer:draw1()
	local x, y = imgui.Position("customer", 20, 20)
	lg.circle('line', x, y, 8, 16)
	lg.arc('line', x, y+20, 10, 0, -math.pi)

	if self.state == "waiting" then
		lg.print({
			colors.white, "I am going into battle, and I need\nonly your ",
			colors.green, "STRONGEST ", colors.white, "potions."
		}, 40, 10)
	elseif self.state == "good" then
		lg.print("Thank you, potion seller", 40, 10)
	elseif self.state == "bad" then
		lg.print({
			colors.white, "I've had enough of these ",colors.pink,"games",colors.white,"!\n"}, 40, 10)
	end

end

function game:init(player_config)
	self.time_now = 0
	self.flux = Flux.group()
	self:worldInit()
	self.cauldron = self:addItem(cauldron.new())
	self.customer = self:addItem(customer.new())
	self:addItem(booklet.new())
	self:addItem(mixing_desk.new())
end

function game.load()
end

function game:tick(dt)
	self.flux:update(dt)
	self.time_now = self.time_now + dt
	self:worldEach("tick", self, dt)
	self:worldClean()
end

function game:keypressed(k)
	local m
	if k == keys.up then m = "newt"
	elseif k == keys.down then m = "toad"
	elseif k == keys.left then m = "bat"
	elseif k == keys.right then m = "rat"
	end
		
	if m then
		self.cauldron:addIngredient(self, m)
	elseif k == keys.space then
		self.cauldron:fillBeaker(self)
	end
end

return game
