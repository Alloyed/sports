-- base class for server states
local class = require 'gabe.class'
local game = class("game.server")

function game:update()
end

function game.recv(state, srv, room, data, peer, channel)
end

return game
