local class = require 'gabe.class'
local misc  = require 'misc'

local world = class(...)

function world:worldInit()
	self.worldNextId = 1
	self.worldItems  = {}
end

function world:addItem(item)
	assert(item)
	assert(class.xtype(item))
	item.id = self.worldNextId
	self.worldNextId = self.worldNextId + 1
	table.insert(self.worldItems, item)
	return item
end

function world:tryFindItem(itemId)
	local i = misc.binsearch(self.worldItems, 'id', itemId)
	if not i then return nil, "item not in world" end
	return self.worldItems[i], i
end

function world:findItem(itemId)
	local item, idx = self:tryFindItem(itemId)
	if not item then error(idx, 2) end
	return item, idx
end

function world:worldEach(name, ...)
	for i=#self.worldItems, 1, -1 do
		local item = self.worldItems[i]
		if item[name] then
			item[name](item, ...)
		end
	end
end

function world:worldDo(fn, ...)
	for i=1, #self.worldItems do
		local item = self.worldItems[i]
		fn(item, ...)
	end
end

function world:worldClean()
	-- we iterate backwards so items can remove themselves
	for i=#self.worldItems, 1, -1 do
		local item = self.worldItems[i]
		if item.dead then
			if item.clean then
				item:clean(self)
			end
			table.remove(self.worldItems, i)
		end
	end
end

return world
