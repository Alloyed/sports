local class  = require 'gabe.class'
local vec    = require 'vec'
local gs     = require 'gamestate'
local colors = require 'gfx.colors'
local keys   = require 'keys'
local misc   = require 'misc'
local assman = require 'assman'
local Bump   = require 'bump'

local game = class(...)
class.mixin(game, require 'games.mixins.world')

--- {{{ mixin list
local bumpObj = class.subclass(game, "bumpObj")

function bumpObj:cleanPhysics(W)
	self:removePhysics(W.P)
end

bumpObj.clean = bumpObj.cleanPhysics

function bumpObj:removePhysics(physics)
	physics:remove(self)
end

function bumpObj:addPhysics(physics)
	local w, h = self.w, self.h
	local hw, hh = self.w*.5, self.h*.5
	physics:add(self, self.p.x-hw, self.p.y-hh, w, h)
end

function bumpObj:drawCollision()
	local w, h = self.w, self.h
	local hw, hh = self.w*.5, self.h*.5
	love.graphics.setColor(colors.white)
	love.graphics.rectangle('line', self.p.x-hw, self.p.y-hh, w, h)
end

function bumpObj:draw1()
	local w, h = self.w, self.h
	local hw, hh = self.w*.5, self.h*.5
	love.graphics.setColor(200, 200, 200)
	love.graphics.rectangle('fill', self.p.x-hw, self.p.y-hh, w, h)
end
-- }}}

--- {{{ entities
local player = class((...)..".player")
class.mixin(player, bumpObj)

player.Tweak = {
	jumpGravity   = 700,
	fallGravity   = 1200,
	hangGravity   = 0,
	impulse       = -260,
	minspeed      = 120,
	midspeed      = 120,
	midband       = .1,
	maxspeed      = 140,
	maxband       = .3,
	jump_timeout  = .15,
	friction      = .5,
	friction_snap = .1,
	hurtTimeout   = .3, -- iframes after being hurt
}

player.w, player.h = 14, 14
function player:init(W, x, y)
	self.p = vec.new(x, y)
	self.v = vec.new(0, 0)
	self.momentum = 0

	self.method = "all"
	self.last_floor = W.time_now

	self:addPhysics(W.P)
end

function player:pollInput(W)
	if self.method == "remote" then
		return self.left, self.right
	end
	local mleft  = keys.isDown("Left")
	local mright = keys.isDown("Right")

	if self.method == 'm' then
		return mleft, mright
	end

	local left, right = nil, nil
	local joys = love.joystick.getJoysticks()
	for _, joy in ipairs(joys) do
		if joy:isGamepad() then
			local gp = joy
			left  = left  or gp:isGamepadDown('dpleft')
			right = right or gp:isGamepadDown('dpright')

			left  = left  or gp:getGamepadAxis('leftx') < -.5
			right = right or gp:getGamepadAxis('leftx') > .5
		end
	end

	if self.method == 'gp' then
		return left, right
	end

	return left or mleft, right or mright
end

function player.filter(self, other)
	if class.is(other, "games.spelunking.player") then
		return
	end
	if other.onCross then
		return "cross"
	end
	return "slide"
end

function player:tick(W, dt)
	local left, right = self:pollInput(W)
	if self.method == "remote" then return end
	if self.hurt then
		left, right = false, false
		assert(W.time_now)
		assert(self.hurt)
		assert(self.Tweak.hurtTimeout)
		if W.time_now - self.hurt > self.Tweak.hurtTimeout then
			self.hurt = nil
		end
	end

	if self.tried_jumping then
		if self:canJump(W) then
			self:doJump(W)
			self.tried_jumping = nil
		elseif W.time_now - self.tried_jumping > 10/60 then
			-- too old
			self.tried_jumping = nil
		end
	end

	local speed = self.Tweak.minspeed
	if left then
		if self.last_dir == "left" then
			if self.momentum then
				if self.momentum > self.Tweak.maxband then
					speed = self.Tweak.maxspeed
				elseif self.momentum > self.Tweak.midband then
					speed = self.Tweak.midspeed
				end
			end
		elseif self.last_dir == "right" then
			S.events:netpub("turn", {player=self.id})
			self.momentum = self.momentum * .5
		end

		self.v.x = misc.lerp(self.v.x, -speed, .9)

		if self.hang_dir ~= "left" then
			self.hang  = nil
			self.climb = nil
		end
		self.last_dir = "left"
		self.momentum = self.momentum + dt
	elseif right then
		if self.last_dir == "right" then
			if self.momentum then
				if self.momentum > self.Tweak.maxband then
					speed = self.Tweak.maxspeed
				elseif self.momentum > self.Tweak.midband then
					speed = self.Tweak.midspeed
				end
			end
		elseif self.last_dir == "left" then
			S.events:netpub("turn", {player=self.id})
			self.momentum = self.momentum * .5
		end
		self.v.x = misc.lerp(self.v.x, speed, .9)

		self.last_dir = "right"
		if self.hang_dir ~= "right" then
			self.hang = nil
			self.climb = nil
		end
		self.momentum = (self.momentum or 0) + dt
	else
		self.v.x = self.v.x * self.Tweak.friction
		if math.abs(self.v.x) < self.Tweak.friction_snap then
			self.v.x = 0
		end

		self.last_dir = nil
		self.momentum = 0
	end

	local gravity = self.Tweak.fallGravity

	if self.hang then
		gravity = self.Tweak.hangGravity
		self.v.y = math.max(0, self.v.y * (.75))
	elseif self.isJumping then
		gravity = self.Tweak.jumpGravity
	end

	if self.v.y > 400 then
		self.v.y = self.v.y * .8
	end
	self.v.y = self.v.y + (gravity * dt)

	if self.v.y > 0 then
		self.isJumping = false
	end

	self.p:add_(self.v:scale(dt))

	self.floor = nil

	local goalx, goaly = self.p.x, self.p.y
	local cback = function(_self, other, x, y, xgoal, ygoal, normalx, normaly)
		if _self.parent then
			_self = _self.parent
		end
		if other.onCross then
			other:onCross(W, _self)
			return
		end

		if normaly == -1 then
			-- on ground
			_self.v.y = math.min(0, _self.v.y)
			_self.floor = true
		elseif normalx ~= 0 then
			-- wall hang
			if not self.isJumping then
				_self.hang = true
				_self.hang_dir = normalx < 0 and "right" or "left"
			end
			self.momentum = 0
		elseif normaly == 1 then
			-- hit head on ceiling
			-- TODO: nudge player instead if overlap is small
			if _self.v.y < 0 then
				_self.v.y = 0
			end
		end
	end

	local cols
	goalx, goaly, cols = W.P:move(self, goalx-7, goaly-7, self.filter, cback)
	for _, col in ipairs(cols) do
		cback(col.item, col.other, goalx, goaly, goalx, goaly, col.normal.x, col.normal.y)
	end

	self.p.x, self.p.y = goalx+7, goaly+7

	if self.floor then
		self.isJumping = false
		self.last_floor = W.time_now
	end
end

function player:draw1(W, R)
	local w, h = A.spelunking.thief:getDimensions()
	local ox, oy = imgui.Position("o", 0, 0)
	local flip = self.last_flip or false
	if self.pull then
		flip = math.cos(self.pull.dir) > 0
	elseif self.v.x < 0 then
		flip = true
	elseif self.v.x > 0 then
		flip = false
	end
	self.last_flip = flip
	flip = flip and -1 or 1

	local state = 1
	if not self:canJump(W) then
		state = 1
	elseif self.hang then
		state = 1
		flip = -flip
	elseif math.abs(self.v.x) > .5 then
		state = 2 -- run
	end

	local s = imgui.Float("scale", .5, .05)
	ox, oy = ox + w*.5, oy + h
	A.spelunking.thief:draw(state, self.p.x, self.p.y+10, 0, flip*s, s, ox, oy)
	love.graphics.setLineStyle('rough')
	--love.graphics.rectangle('line', self.p.x-7, self.p.y-7, 14, 14)
	love.graphics.setColor(colors.white)
end

function player:onGround(W)
	local timeout = self.Tweak.jump_timeout
	return (self.floor or W.time_now - self.last_floor < timeout)
end

function player:canJump(W)
	return self.hang or self:onGround(W)
end

function player:doJump()
	self.v.y = self.Tweak.impulse
	self.last_floor = -math.huge
	self.isJumping = true
	if self.hang then
		self.hang = nil
		self.climb = true
	end

	S.events:netpub("jump", {
		player = self.id
	})
end

function player:tryJumping(W)
	if self:canJump(W) then
		self:doJump()
	else
		self.tried_jumping = W.time_now
	end
end

function player:stopJumping()
	if self.isJumping then
		self.isJumping = false
		self.v.y = self.v.y * .5
	end
end

function player:tryLetGo(W)
	if self.hang then
		self.hang = nil
		self.climb = true
	end
end

function player:tryRecoil(W)
	if self.hurt then return end
	if self.v.x > 0 then
		self.v:set_(-300, -100)
	end
	self.hurt = W.time_now
end

local coin = class.subclass(game, "coin")
coin.w, coin.h = 10, 12
class.mixin(coin, bumpObj)

function coin:init(W, x, y)
	self.p = vec.new(x+8, y+10)
	self:addPhysics(W.P)
	self.variant = W.rng:random(1, 3)
end

function coin:onCross()
	self.dead = true
end

function coin:draw1(W, R)
	local w, h = A.spelunking.small_loot:getDimensions()
	local ox, oy = -1, 0
	A.spelunking.small_loot:draw(self.variant, self.p.x+ox, self.p.y+oy, 0, 1, 1, w*.5, h*.5)
end

local bigCoin = class.subclass(game, "bigCoin")
class.mixin(bigCoin, coin)

function bigCoin:onCross()
	self.dead = true
end

local spikes = class.subclass(game, "spikes")
spikes.w, spikes.h = 14, 4
class.mixin(spikes, bumpObj)

function spikes:init(W, x, y)
	self.p = vec.new(x+8, y+16-4)
	self:addPhysics(W.P)
end

function spikes:onCross(W, player)
	player:tryRecoil(W)
end

local bat = class.subclass(game, "bat")
bat.w, bat.h = 18, 18
class.mixin(bat, bumpObj)

function bat:init(W, x, y)
	self.p = vec.new(x+8, y+8)
	self:addPhysics(W.P)
end

local tiles  = class((...)..".tiles")

function tiles:init(map)
	self.shapes = {}
end

function tiles:ok(W, map)
	local layer = map.layers.fg_data
	for y=0, layer.height-1 do
		for x=0, layer.width-1 do
			local tile = layer.data[y+1][x+1]
			if tile then
				local id = tile.id + 1
				if id == 1 or id == 2 then
					local shape = {
						parent = self,
						x = x, y = y
					}
					W.P:add(shape, x*16, y*16, 16, 16)
					table.insert(self.shapes, shape)
				end
			end
		end
	end
	return self
end

function tiles:drawCollision(W, R)
	for _, shape in ipairs(self.shapes) do
		local x, y, w, h = W.P:getRect(shape)
		love.graphics.rectangle('line', x, y, w, h)
	end
end

local object_types = {
	player,
	coin,
	spikes,
	bat
}
function tiles:spawnObjects(W, player_config)
	local layer = W.map.layers.spawn
	for y=0, layer.height-1 do
		for x=0, layer.width-1 do
			local tile = layer.data[y+1][x+1]
			if tile then
				local id = tile.id + 1
				local klass = object_types[id]
				if not klass then
					error("missing object for id " .. tostring(id))
				end

				if id == 1 then
					self:spawnPlayers(W, player_config, x*16, y*16)
					--local obj = klass.new(W, x*16, y*16)
					--W:addItem(obj)
					--table.insert(W.players, obj)
				else
					W:addItem(klass.new(W, x*16, y*16))
				end
			end
		end
	end
end

function tiles:spawnPlayers(W, player_config, x, y)
	for i, playerID in ipairs(player_config) do
		local player = player.new(W, x+W.rng:random(-3, 3), y)
		if playerID == "local" then
			player.nick = Settings.nick or "player"
			player.method = A.controls[1]
			playerID = "local"
		elseif playerID == "local.1" then
			player.nick = Settings.nick or "player"
			player.method = A.controls[1]
			playerID = "local.1"
		elseif playerID == "local.2" then
			player.nick = "player 2"
			player.method = A.controls[2]
			playerID = "local.2"
		elseif A.client and playerID == A.client.clientID then
			player.nick = Settings.nick or "player"
			player.method = "all"
			playerID = "local"
		else
			player.nick = A.client.players[playerID].nick
			player.method = "remote"
		end

		if #player_config == 1 then
			player.team = 1
		elseif i < 5 then
			player.team = i
		end
		W:addItem(player)
		W.players[playerID] = player
	end
end
-- }}}

function game:init(player_config)
	player_config = player_config or {"local"}
	self.P = Bump.newWorld(64)
	self.time_now = 0
	self.rng = love.math.newRandomGenerator(1337)

	self:worldInit()

	assman.load.map("spelunking:map/map")
	self.map = A.spelunking.map
	assert(self.map.layers.fg, "missing 'fg' layer")
	local tiles = tiles.new()

	tiles:ok(self, self.map)
	self.players = {}
	tiles:spawnObjects(self, player_config)

	self:addItem(tiles)
end

function game.load()
end

function game:tick(dt)
	self.time_now = self.time_now + dt
	self:worldEach("tick", self, dt)
	self:worldClean()
end

function game:endGame()
	if A.client then
		-- this is fine
		A.client:msg("finishInstancedMatch", self.players["local"].score)
	else
		self.events:pub("finish_trial")
		local score = nil
		for _, player in pairs(self.players) do
			score = player.score
		end
		gs.switch("views.scoreboard", {score = score})
	end
end

function game:playerFor(key)
	for _, p in pairs(self.players) do
		if p.method == "all" or key == "all" then
			return p
		end

		if p.method == key then
			return p
		end
	end
	if type(key) == "userdata" then
		return nil, "no player for key " .. key:getName()
	else
		return nil, "no player for key " .. tostring(key)
	end
end

return game
