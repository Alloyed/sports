local class  = require 'gabe.class'
local keys   = require 'keys'
local fun    = require 'fun'
local misc   = require 'misc'
local colors = require 'gfx.colors'
local assman = require 'assman'
local lm     = require 'love.math'

local _game = require('games.spelunking.logic')

local game = class(...)
class.mixin(game, require'views.games.gameView')

local bg = class(game, "bg")
function bg:init()
	local W, H = A.spelunking.bg_tiles:getDimensions()
	self.quads = {}
	for iy = 0, H-1, 16 do
		for ix = 0, W-1, 16 do
			table.insert(self.quads, love.graphics.newQuad(ix, iy, 16, 16, W, H))
		end
	end
	self.heatmaps = {}
	self.start_x = 0
	self.end_x = 0
	self:gen(0)
end

function bg.load()
	assman.load.image("spelunking:map/bg_tiles")
	assman.load.anim("spelunking:ground_tiles")
	assman.load.anim("spelunking:platform_tiles")
end

function bg:gen(start_x)
	local function key(x, y)
		return y + x * 100
	end

	local function from_key(k)
		return math.floor(k/100), k % 100
	end

	local start_time = love.timer.getTime()
	local heatmap = {}
	-- spawn islands
	for i=1, 6 do
		local x, y= lm.random(0, 32)+math.floor(start_x/16), lm.random(0, 18)
		heatmap[key(x, y)] = lm.random() < .5 and 15 or lm.random(12, 14)
	end

	for i=1, 8 do
		local x, y= lm.random(0, 32)+math.floor(start_x/16), lm.random(0, 18)
		heatmap[key(x, y)] = lm.random(2, 6)
	end

	-- grow out
	for i=1, 4 do
		for k, heat in pairs(heatmap) do
			local x, y = from_key(k)
			local function place(dx, dy, dh)
				local ix, iy = x+dx, y+dy
				local ik = key(ix, iy)
				if heatmap[ik] then return end
				local iheat = heat-dh
				if iheat <= 1 then
					iheat = nil
				end
				heatmap[ik] = iheat
			end

			-- cardinal
			if love.math.random() < .4 then place( 0, -1, lm.random(1, 3)) end
			if love.math.random() < .4 then place( 0,  1, lm.random(1, 3)) end
			if love.math.random() < .4 then place(-1,  0, lm.random(0, 3)) end
			if love.math.random() < .4 then place( 1,  0, lm.random(0, 3)) end

			-- diagonal
			--if love.math.random() < .5 then place(-1, -1, 1) end
			--if love.math.random() < .5 then place( 1,  1, 1) end
			--if love.math.random() < .5 then place(-1,  1, 1) end
			--if love.math.random() < .5 then place( 1, -1, 1) end
		end

	end

	-- extra decay
	for k, heat in pairs(heatmap) do
		if love.math.random() < .2 then
			heatmap[k] = nil
		elseif love.math.random() < .2 then
			local iheat = heat-lm.random(-1, 3)
			if iheat <= 1 then
				iheat = nil
			end
			heatmap[k] = iheat
		end
	end

	-- limit cases of adjacent equivalent tiles
	for i=1, 4 do
		for k, heat in pairs(heatmap) do
			local x, y = from_key(k)
			local neighbors = {}
			neighbors[#neighbors+1] = heatmap[key(x, y+1)]
			neighbors[#neighbors+1] = heatmap[key(x, y-1)]
			neighbors[#neighbors+1] = heatmap[key(x-1, y)]
			neighbors[#neighbors+1] = heatmap[key(x+1, y)]
			while fun.index_of(heat, neighbors) do
				heat = heat - 1
				if heat <= 1 then
					heat = nil
					break
				end
			end
			heatmap[k] = heat
		end
	end
	-- tile 15 has a enforced next-to tile
	for k, heat in pairs(heatmap) do
		local x, y = from_key(k)
		if heat == 15 then
			heatmap[key(x+1, y)] = 16
		end
	end
	self.start_x = math.min(self.start_x, start_x)
	self.end_x   = math.max(self.end_x,   start_x + 32*16)

	local sb = love.graphics.newSpriteBatch(A.spelunking.bg_tiles, 2048)
	for k, heat in pairs(heatmap) do
		local ix, iy = math.floor(k/100), k % 100
		--love.graphics.draw(A.spelunking.bg_tiles, self.quads[heat], ix*16, iy*16)
		sb:add(self.quads[heat], ix*16, iy*16)
	end

	table.insert(self.heatmaps, {
		sb=sb,
		start_x = start_x,
		end_x = start_x + 32 * 16
	})
	self.gen_cost = (love.timer.getTime() - start_time) * 1000
end

function bg:update(cam_x)
	local W, _ = A.lbox:dimensions()
	if cam_x+W+128 - self.end_x > 0 then
		self:gen(self.end_x)
	end
	if cam_x - self.start_x < W then
		self:gen(self.start_x - W)
	end
end

function bg:draw(cam_x)
	local W = A.lbox.target_w
	love.graphics.clear(35, 4, 3)

	for _, heatmap in ipairs(self.heatmaps) do
		if math.abs(cam_x - heatmap.start_x) < W*3 then
			love.graphics.draw(heatmap.sb)
		end
	end
end

-- lifecycle
function game:init(prev, player_config)
	self:baseInit(_game, player_config)
	self.bg = bg.new()
	self.cam_x = 0
	self.cam_y = 0
end

function game.load()
	_game.load()
	bg.load()
	assman.load.anim("spelunking:thief")
	assman.load.anim("spelunking:small_loot")
end

function game:exit()
end

-- update
function game:tick(dt)
	self:baseTick(dt)

	if A.client and A.client.conn then
		self.time_last_sent = self.time_last_sent or self.W.time_now

		if self.W.time_now-self.time_last_sent > 1/60 then
			self.time_last_sent = self.time_last_sent + 1/60
			A.client.conn:send({
				time_now = self.W.time_now,
				player = self.W:playerFor("m")
			}, 2, "unsequenced")
		end
	end
end

function game:update(dt)
	-- camera
	local cam_ox, cam_oy = 100, -56
	--local cam_ox, cam_oy = imgui.Position("camera", 175, 0)
	local avg_x = 0
	local n = 0
	for _, player in pairs(self.W.players) do
		avg_x = player.p.x
	end


	--local cam_x = cam_ox - misc.osc_tri(20*8, 256*16)
	local cam_x = cam_ox - avg_x

	cam_x = misc.lerp(self.last_cam or cam_x, cam_x, .5)
	--if self.last_cam and cam_x > self.last_cam then
	--	cam_x = self.last_cam
	--end
	self.last_cam = cam_x
	--A.lbox.subx = cam_x - math.floor(cam_x)
	cam_x = math.floor(cam_x)

	self.cam_x = -cam_x
	self.cam_y = cam_oy

	self.bg:update(self.cam_x)
end

function game:updateAudio()
end

-- draw
function game:initRenderer(R)
end

function game:cleanRenderer(R)
end

local function draw_fg(W, R)
	if not R.tilefg then
		local sb = A.spelunking.ground_tiles:newBatch(1024*8, 'static')
		local sb2 = A.spelunking.platform_tiles:newBatch(1024*8, 'static')

		local layer = W.map.layers.fg_data
		for y=0, layer.height-1 do
			y = y + 1
			for x=0, layer.width-1 do
				x = x+1
				local tile = layer.data[y][x]
				if tile then
					local id = tile.id + 1
					if id == 1 then
						local tileId = 0

						if layer.data[y+1] and layer.data[y+1][x] then tileId = tileId + 4 end
						if layer.data[y-1] and layer.data[y-1][x] then tileId = tileId + 1 end
						if layer.data[y][x-1] then tileId = tileId + 8 end
						if layer.data[y][x+1] then tileId = tileId + 2 end

						sb:addFrame(tileId+1, 1, (x-1)*16, (y-1)*16)
					elseif id == 2 then
						local tileId = 0

						if layer.data[y+1] and layer.data[y+1][x] then tileId = tileId + 4 end
						if layer.data[y-1] and layer.data[y-1][x] then tileId = tileId + 1 end
						if layer.data[y][x-1] then tileId = tileId + 8 end
						if layer.data[y][x+1] then tileId = tileId + 2 end

						sb2:addFrame(tileId+1, 1, (x-1)*16, (y-1)*16)
					end
				end
			end
		end
		R.tilefg = {sb, sb2}
	end

	R.tilefg[1]:draw()
	R.tilefg[2]:draw()
end

function game:draw()
	local view = self
	view:startDraw()
	local R = view:renderer()
	local W = view.W

	love.graphics.push()
	love.graphics.translate(-self.cam_x, self.cam_y)
	love.graphics.setColor(colors.white)

	-- tiled
	--W.map:drawLayer(W.map.layers.bg)
	self.bg:draw(self.cam_x)
	draw_fg(W, R)
	--W.map:drawLayer(W.map.layers.fg)

	--W.map.tiled:drawLayer(W.map.layers.prop1)
	W:worldEach("draw1", W, R)

	view:drawCollision(W)
	love.graphics.pop()

	local circles = {
	}
	for _, player in pairs(W.players) do
		table.insert(circles, {
			x = player.p.x - self.cam_x,
			y = player.p.y + self.cam_y
		})
	end
	local _w, _h = A.lbox:dimensions()
	local r = love.math.noise(1, misc.round(love.timer.getTime()*10, 1.5))*1
	local base = 64 -- imgui.Float("rad", 48)
	love.graphics.setStencilTest("notequal", 1)
	love.graphics.stencil(function()
		for _, circle in ipairs(circles) do
			love.graphics.circle('fill', circle.x, circle.y, base+r)
		end
	end)
	love.graphics.setColor(15, 10, 20, 130)
	love.graphics.rectangle('fill', 0, 0, _w, _h)
	love.graphics.setStencilTest()

	love.graphics.setStencilTest("notequal", 1)
	love.graphics.stencil(function()
		for _, circle in ipairs(circles) do
			love.graphics.circle('fill', circle.x, circle.y, base-6+r*3)
		end
	end)
	love.graphics.setColor(15, 10, 20, 130)
	love.graphics.rectangle('fill', 0, 0, _w, _h)
	love.graphics.setStencilTest()
	view:endDraw()
end

-- events
function game:keypressed(k)
	local p = self.W:playerFor("m")
	if not p then return end

	if keys.Jump[k] then
		p:tryJumping(self.W)
	elseif keys.Down[k] then
		p:tryLetGo(self.W)
	end
end

function game:keyreleased(k)
	local p = self.W:playerFor("m")
	if not p then return end

	if keys.Jump[k] then
		p:stopJumping()
	end
end

function game:gamepadpressed(gp, btn)
	local p = self.W:playerFor(gp)
	if not p then return end

	if btn == 'a' then
		p:tryJumping(self.W)
	elseif btn == 'dpdown' then
		p:tryLetGo(self.W)
	end
end

function game:gamepadreleased(gp, btn)
	local p = self.W:playerFor(gp)
	if not p then return end

	if btn == 'a' then
		p:stopJumping()
	end
end

function game:recv(data, peer, channel)
	if channel == 2 then
		for playerID, pdata in pairs(data.playerObjects) do
			if playerID ~= A.client.clientID then
				local newP = pdata.player
				local oldP = self.W.players[playerID]
				local _, _i = self.W:findItem(oldP.id)
				if _i then
					oldP:removePhysics(self.W.P)
					newP:addPhysics(self.W.P)
					newP.id = oldP.id
					newP.method = "remote"
					self.W.worldItems[_i] = newP
					self.W.players[playerID] = newP
				end
			end
		end
	end
end

return game
