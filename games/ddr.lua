local class   = require 'gabe.class'
local vec     = require 'vec'
local prompts = require 'gfx.prompts'

local game = class(...)
class.mixin(game, require 'games.mixins.world')

local song = [[
0000
0000
0000
0000 
,
1000
0000
0100 
0000
,
1000
0000
0000
0000 
,
0001
0000
0020
0000
,
0001
0000
0000
0000 
]]

local function parse_song(str)
	local measures = {}
	local lines    = {}
	for measure_s in str:gmatch("[^,]+") do
		table.insert(measures, #lines+1)
		for line_s in measure_s:gmatch("%d%d%d%d") do
			local line = {}
			for i=1, 4 do
				line[i] = tonumber(line_s:sub(i, i))
			end
			table.insert(lines, line)
		end
	end
	return measures, lines
end

local track = class((...)..".track")
track.Tweak = {
	x_sep = 32,
	timing = {
		early = {
			2/60,
			4/60,
			13/60,
			19/60,
			23/60
		},
		late  = {
			2/60,
			4/60,
			11/60,
			17/60,
			27/60
		}
	}
}

function track:init(src, bpm)
	self.measures, self.lines = parse_song(src)
	self.used  = {}
	for i=1, #self.lines do
		self.used[i] = {false, false, false, false}
	end
	self.pressed = {false, false, false, false}
	self.time  = 0
	self.bpm   = bpm or 120 * 4
end

function track:to_s(time)
	return time/(self.bpm/60)
end

function track:dist(dir)
	local low  = math.max(1, math.floor(self.time+1)-1)
	local high = math.min(#self.lines,math.floor(self.time+1)+2)

	local dist, beat = math.huge, nil

	for i=low, high do
		if self.lines[i][dir] ~= 0 and not self.used[i][dir] then
			local arrow = self.lines[i][dir]
			local t = i + (.25*(arrow-1))
			return self:to_s(self.time - t), i
		end
	end
	if not beat then return nil end
	return self:to_s(dist), beat
end

function track:judgement(dist)
	local timing = self.Tweak.timing.late
	if dist < 0 then
		timing = self.Tweak.timing.early
	end
	for i, max in ipairs(timing) do
		if math.abs(dist) < max then
			return i
		end
	end
	return -1
end
track.JudgementNames = {[-1]="miss","flawless!!", "perfect!", "great", "good", "boo"}

function track:press(dir)
	self.pressed[dir] = true
	local dist, beat = self:dist(dir)
	if beat then
		self.used[beat][dir] = true
		self.echo = self.JudgementNames[self:judgement(dist)] .. "\n" .. dist
	end
end

function track:release(dir)
	self.pressed[dir] = false
end

function track:tick(W, dt)
	local otime = self.time
	self.time = (self.time + (self.bpm*dt/60)) % #self.lines
	if self.time < otime then
		for i=1, #self.lines do
			self.used[i][1] = false
			self.used[i][2] = false
			self.used[i][3] = false
			self.used[i][4] = false
		end
	end
end

function track:draw1()
	if self.echo then
		love.graphics.print(self.echo, 400, 20)
	end
	local start = math.max(1, math.floor(self.time+1)-2)
	for dir=1, 4 do
		local btn = {"dpleft", "dpdown", "dpup", "dpright"}
		love.graphics.push()
		love.graphics.translate(dir*self.Tweak.x_sep, 20)
		love.graphics.translate(12, 12)
		love.graphics.scale(self.pressed[dir] and 1.2 or 1)
		love.graphics.translate(-12, -12)
		prompts.drawButton(btn[dir], 0, 0)
		love.graphics.pop()
	end

	for i=start, math.min(start+10, #self.lines) do
		local arrows = self.lines[i]
		assert(arrows, i)
		local y = i - self.time
		for dir, n in ipairs(arrows) do
			if n ~= 0 then
				local _y = ((y+(.25*(n-1))) * 40) + 20
				local btn = {"dpleft", "dpdown", "dpup", "dpright"}
				prompts.drawButton(btn[dir], dir*self.Tweak.x_sep, _y)
			end
		end
	end
end

local ball = class((...)..".ball")

function ball:init()
	self.p = vec.new(100, 100)
end

function ball:tick(dt)
end

function ball:draw1(W, R)
	love.graphics.circle('fill', self.p.x, self.p.y, 2)
end

function game:init(player_config)
	self:worldInit()
	self.track = self:addItem(track.new(song))
end

function game.load()
	prompts.load()
end

function game:tick(dt)
	self:worldEach("tick", self, dt)
	self:worldClean()
end

return game
