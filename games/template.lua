local class = require 'gabe.class'
local vec   = require 'vec'

local game = class(...)
class.mixin(game, require 'games.mixins.world')

local ball = class((...)..".ball")

function ball:init()
	self.p = vec.new(100, 100)
end

function ball:tick(dt)
end

function ball:draw1(W, R)
	love.graphics.circle('fill', self.p.x, self.p.y, 2)
end

function game:init(player_config)
	self:worldInit()
	self:addItem(ball.new())
end

function game.load()
end

function game:tick(dt)
	self:worldEach("tick", self, dt)
	self:worldClean()
end

return game
