local class = require 'gabe.class'
local game  = require 'games.mixins.server'

require 'games.archery.logic'

local archery = class("games.archery.server")

class.mixin(archery, game)

function archery:init(config)
	self.config = config
	self.playerObjects = {}
	self.time_now = 0
	self.time_last_sent = 0
end

archery.server_sendrate = 1/60
function archery.update(state, srv, room, dt)
	state.time_now = state.time_now + dt
	local r = archery.server_sendrate
	if state.time_now-state.time_last_sent > r then
		state.time_last_sent = state.time_last_sent + r
		A.server:broadcast({
			time_now = state.time_now,
			playerObjects = state.playerObjects
		}, 2, "unsequenced")
	end
end

function archery.recv(state, srv, room, data, peer, channel)
	if channel == 2 then
		local old = state.playerObjects[peer:id()]
		if not old or old.time_now < data.time_now then
			state.playerObjects[peer:id()] = data
			--peer:send({
			--	time_now = state.time_now,
			--	playerObjects = state.playerObjects
			--}, 2, "unsequenced")
		end
	elseif channel == 3 then
		assert(data.item.fired_by)
		data.item.fired_by = peer:id()
		A.server:broadcast(data, 3, "reliable")
	end
end
