--- Archery game
-- TODO:
-- * split out controls/rendering specific-logic into the client
-- * define an API

local class      = require 'gabe.class'
local gs         = require 'gamestate'
local recolor    = require 'gfx.recolor'
local Flux       = require 'flux'
local misc       = require 'misc'
local events     = require 'events'
local Splash     = require 'splash'

local archery = class("games.archery")

require 'games.archery.entities'

archery.Tweak = {
	--gameLength = 60
	gameLength = 20
}

class.mixin(archery, require 'games.mixins.world')

function archery:init(player_config, mapname)
	self.events = events.new()

	player_config = player_config or {
		"local"
	}
	mapname = mapname or Settings.map
	self.player_keys = player_config

	-- we're running into some trouble here because we don't distinguish
	-- between the world and the game.
	--
	-- underscore time (_time_now and _flux) represents ingame time, which
	-- passes even when the world is paused (like for example during
	-- countdowns)
	--
	-- non-underscore time is best understood as in-world time, the time that
	-- passes for player movement, etc.
	self.time_now    = 0
	self._time_now   = 0
	self.flux  = Flux.group()
	self._flux = Flux.group()

	self.rng = love.math.newRandomGenerator(1337)
	self:worldInit()
	self.P = Splash.new(128)
	self.camera_type = "follow"

	local Map = class.get("games.archery.map")
	self.map = self:addItem(Map.new(self, mapname or "archery_map_3.lua"))
	self.players = {}
	self.map:spawnObjects(self, self.player_keys)

	-- countdown fields
	self.countdown_t   = 4
	self.instruction_t = 1
	self.countdown     = true
	self.timeleft      = self.Tweak.gameLength
end

function archery:startCountdown()
	if Settings.no_timer then
		self.countdown = nil
		self.instruction_t = 0
		return
	end

	local f = self._flux:to(self, self.countdown_t-1, {countdown_t=1}):ease("linear")
	f:oncomplete(class.method(self, 'startGame'))
	self.events:pub("start_countdown")
end

function archery:startGame()
	self.countdown = false
	self.nextWave = self.time_now
	self.events:pub("start_trial")

	-- show GO message
	self._flux:to(self, 1, {countdown_t=0,instruction_t=-.5})

	-- Game clock
	local f = self._flux:to(self, self.timeleft, {timeleft = 0}):ease("linear")
	f:oncomplete(class.method(self, "timeUp"))
end

-- add some empty space at the end of each game
function archery:timeUp()
	-- FIXME: the game should continue simulating here,
	-- but self.countdown pauses everything
	self.countdown = true
	self.countdown_t = 1
	self.time_is_up = true
	self.events:pub("time_up")
	local f = self._flux:to(self, 2, {countdown_t = 0})
	f:oncomplete(class.method(self, "endGame"))
end

function archery:endGame()
	if A.client then
		-- this is fine
		A.client:msg("finishInstancedMatch", self.players["local"].score)
	else
		self.events:pub("finish_trial")
		local scores = {}
		for key, player in pairs(self.players) do
			table.insert(scores, {id=key, score=player.score})
		end
		gs.switch("views.scoreboard", scores)
	end
end

function archery.load()
	recolor.load()
	local img = love.image.newImageData("assets/data/target_track.png")
	A.target_offsets = {}
	for sx=0, img:getWidth()-1, 32 do
		local f = function()
			for y=0, img:getHeight()-1 do
				for x=sx, sx+32-1 do
					local r, g, b, a = img:getPixel(x, y)
					if r == 254 and g == 4 and b == 4 then
						table.insert(A.target_offsets, y)
						return
					end
				end
			end
		end
		f()
	end
end

local flyingTarget = class.get("games.archery.flying_target")
-- game logic
function archery:tick(dt)

	self._time_now = self._time_now + dt
	self._flux:update(dt)

	if self.countdown then return end

	self.time_now = self.time_now + dt
	self.flux:update(dt)

	misc.timer = function() return self.time_now end

	if self.time_now >= self.nextWave then
		self:worldDo(function(item)
			if class.is(item, "games.archery.floating_target") then
				item.dead = true
			end
		end)
		local W = 1920/4
		local rng = self.rng
		if rng:random() < .3 then
			local x, y = rng:random(W+10, W+20), rng:random(10, 80)
			self:addItem(flyingTarget.new(self, x, y, 1))
		else
			local x, y = rng:random(-10, 0), rng:random(10, 80)
			self:addItem(flyingTarget.new(self, x, y, -1))
		end
		self.nextWave = self.time_now + rng:random(1, 3)
	end
	self:worldEach("tick", self, dt)
	self:worldClean()
	misc.timer = love.timer.getTime
end

-- this triggers once per frame
function archery:update(dt, R)
	self.events:swap()
	misc.timer = function() return self.time_now end
	self:worldEach("update", self, dt, R)
	misc.timer = love.timer.getTime
end

function archery:playerFor(key)
	for _, p in pairs(self.players) do
		if p.method == "all" or key == "all" then
			return p
		end

		if p.method == key then
			return p
		end
	end
	if type(key) == "userdata" then
		return nil, "no player for key " .. key:getName()
	else
		return nil, "no player for key " .. tostring(key)
	end
end

function archery:localPlayer()
	for id, p in pairs(self.players) do
		if id == "local" then
			return p
		end
	end
	return nil, "none found"
end

return archery
