local class      = require 'gabe.class'
local Trail      = require 'gfx.trail'
local polyline   = require 'gfx.polyline'
local tiled      = require 'sti'
local recolor    = require 'gfx.recolor'
local colors     = require 'gfx.colors'
local text       = require 'gfx.text'
local vec        = require 'vec'
local keys       = require 'keys'
local misc       = require 'misc'
local Splash     = require 'splash'
local teamcolors = require 'gfx.teamcolors'
local baton      = require 'baton'

local archery = "games.archery"

-- {{{ Player class
local player = class.subclass(archery, "player")
player.Pivot = { x = 0, y = 12 }
player.ArmPivot = { x = 32, y = 23}
player.States = {idle = 1, running = 2, jumping = 3, shooting = 4, arms = 5, falling = 6}

local arrow = class.subclass(archery, "arrow")
-- max initial impulse.
arrow.MaxSpeed = 366
-- acceleration due to gravity, or lowercase g
arrow.Gravity = 400

function player:init(W, x, y)
	assert(x)
	assert(y)
	self.team = 0
	self.p = vec.new(x, y+6)
	self.v = vec.new(0, 0)

	-- this is so initial screenshots make us look like we're on the ground
	self.last_floor = W.time_now
	self.floor = {is_slope=false}
	self.was_on_ground = true

	self.score         = 0
	self.combo         = 0
	self.display_score = 0
	self.targets_hit   = {}

	self:addPhysics(W.P)
end

function player:addPhysics(P)
	self.shape1 = Splash.circle(self.p.x, self.p.y, 5)
	self.shape2 = Splash.seg(self.p.x+6, self.p.y-20, 0, 16)
	self.left_side  = {o={x=7, y=-20}, parent = self, type = "left"}
	self.shape3 = Splash.seg(self.p.x-6, self.p.y-20, 0, 16)
	self.right_side = {o={x=-7,y=-20},parent = self, type = "right"}

	P:add(self,            self.shape1)
	P:add(self.left_side,  self.shape2)
	P:add(self.right_side, self.shape3)
end

function player:updatePhysics(P)
	P:update(self, vec.unpack(self.p))
	P:update(self.left_side, vec.unpack(vec.add(self.p, self.left_side.o)))
	P:update(self.right_side, vec.unpack(vec.add(self.p, self.right_side.o)))
end

function player:removePhysics(P)
	P:remove(self.left_side)
	P:remove(self.right_side)
	P:remove(self)
end

function player.filter(self, other)
	if other.parent then
		other = other.parent
	end
	if class.is(other, "games.archery.map") then
		return 'slide'
	elseif class.is(other, "games.archery.prop") then
		return 'slide'
	end
end

function player:bowpoint()
	local a = self.pull.dir

	local dist = -16
	return vec.new(math.cos(a)*dist, (math.sin(a)*dist)-self.ArmPivot.y+2)
end

-- FIXME: move this into view
function player:pollInput(W)
	if self.method == "remote" then
		return
	end
	if self.method == "bot" then
		if self.left == false and self.right == false then
			self.right = true
		end
		local odds = self.right and .01 or .05
		if math.random() < odds then
			self.left = not self.left
			self.right = not self.right
		end
		if math.random() < .025 then
			self:tryJumping(W)
		end
		return
	end

	if W.batons[self] then
		local input = W.batons[self]
		input:update()
		self.left, self.right = input:down'left', input:down'right'

		if input:pressed'jump' then
			if self.pull then
				self:cancelString()
				W.string = nil
			else
				self:tryJumping(W)
			end
		end

		if input:released'jump' then
			self:stopJumping()
		end

		if input._controls.shoot and input:pressed'shoot' then
			W.string = vec.new(A.lbox:mouse())
			self:pullString(W, "m")
		end

		if input._controls.shoot and input:released'shoot' then
			if self.pull and self.pull.method == "m" then
				self:releaseString(W)
				W.string = nil
			end
		end

		if input._controls.shootGP and input:pressed'shootGP' then
			--W.string = vec.new(x, y)
			self:pullString(W, input.joystick)
		end

		if input._controls.shootGP and input:released'shootGP' then
			if self.pull and self.pull.method == input.joystick then
				self:releaseString(W)
				W.string = nil
			end
		end
	end
end

function player:updatePull(W)
	local pull = self.pull
	if pull and self.method ~= "remote" then
		if pull.method == "m" then
			local d = vec.new(A.lbox:mouse())
			d:sub_(W.string)
			pull.dir = math.atan2(d.y, d.x)
			pull.mag = misc.lerp(pull.mag, -math.min(1, d:len() / 100), .4)
		elseif type(pull.method) == "userdata" then
			local gp = pull.method
			local d = vec.new(
			gp:getGamepadAxis('leftx'),
			gp:getGamepadAxis('lefty'))

			if d:len() > .2 then
				local real_dir = math.atan2(d.y, d.x)+math.pi
				--real_dir = math.floor(real_dir / (math.pi/32)) * (math.pi/32)
				--real_dir = real_dir + .01
				local sens = .6
				if self:isTouchingTarget(W) then
					sens = .1
				end
				local len = misc.quantize(d:len(), .125)
				pull.dir = misc.lerp(pull.dir, real_dir, sens)
				pull.mag = misc.lerp(pull.mag, -math.min(1, len), .125)
			else
				pull.mag = misc.lerp(pull.mag, 0, .125)
			end

			local y = gp:getGamepadAxis('triggerright')
			if y < .2 then
				self:releaseString(W)
				W.string = nil
			end
		end
	end
end

local player_maxspeed = 140

function player:tick(W, dt)
	local grav = self.isJumping and 600 or 1000
	self.v.y = self.v.y + (grav * dt)

	if self.v.y > 0 then
		self.isJumping = false
	end

	self.p:add_(self.v:scale(dt))

	self.floor = nil

	self.stopped = false
	local ox, _ = self.p.x, self.p.y
	local goalx, goaly = self.p.x, self.p.y
	local cback = function(_self, other, x, y, xgoal, ygoal, normalx, normaly)
		local ctype = "ground"
		if _self.parent then
			ctype = _self.type
			_self = _self.parent
		end
		if class.is(other, "games.archery.prop") then
			if ctype ~= "ground" or class.is(other, "games.archery.boundary") then
				-- prop as wall
				_self.stopped = true
				return
			else
				-- prop as floor
				_self.v.y = math.min(0, _self.v.y)
				_self.floor = other
				_self.floor = {is_slope = true}
				return
			end
		elseif other.is_wall then
			_self.stopped = true
			return
		else
			-- floor
			_self.v.y = math.min(0, _self.v.y)
			_self.floor = other
			_self.floor = {is_slope = other.is_slope, i = other.i}
		end
	end

	for _=1, 2 do
		goalx, goaly = goalx+self.left_side.o.x, goaly+self.left_side.o.y
		goalx, goaly = W.P:move(self.left_side, goalx, goaly, self.filter, cback)
		goalx, goaly = goalx-self.left_side.o.x, goaly-self.left_side.o.y

		goalx, goaly = goalx+self.right_side.o.x, goaly+self.right_side.o.y
		goalx, goaly = W.P:move(self.right_side, goalx, goaly, self.filter, cback)
		goalx, goaly = goalx-self.right_side.o.x, goaly-self.right_side.o.y

		goalx, goaly = W.P:move(self, goalx, goaly, self.filter, cback)
	end

	self.p.x, self.p.y = goalx, goaly

	if self.floor then
		self.isJumping = false
		self.last_floor = W.time_now
		if not self.stopped and self.floor.is_slope then
			self.p.x = ox
		end
	end
end

function player:initView(R)
	local ps = love.graphics.newParticleSystem(A.dust)
	--ps:setEmissionRate(0)
	--ps:setEmitterLifetime(.15)
	ps:setParticleLifetime(.3, .5)
	ps:setSizes(1, 1, .5, 0)
	ps:setSpeed(300)
	ps:setAreaSpread('uniform', 7, 3)
	ps:setLinearAcceleration(0, 0, 0, -80)

	R[self.id] = {dust = ps}
end

function player:update(W, dt, R)
	if W.countdown then
		return
	end

	if self.display_score and self.display_score < self.score then
		self.display_score = self.display_score + 5
	elseif self.display_score and self.display_score > self.score then
		self.display_score = self.display_score - 5
	elseif math.abs(self.score - self.display_score) < 5 then
		self.score = self.display_score
	end

	local ps = R[self.id] and R[self.id].dust
	if not self.was_on_ground and self:onGround(W) then
		ps:setPosition(self.p.x, self.p.y+8)
		ps:emit(3)
	end

	for key, data in W.events:iter("jump", "turn") do
		if data.player == self.id then
			if key == "turn" and self:onGround(W) and not self.stopped then
				ps:setPosition(self.p.x, self.p.y+8)
				ps:emit(2)
			elseif key == "jump" then
				ps:setPosition(self.p.x, self.p.y+8)
				ps:emit(10)
			end
		end
	end

	self.was_on_ground = self:onGround(W)
	local _, py = ps:getPosition()
	ps:moveTo(self.p.x, py)
	ps:setSpeed(self.v.x*-.3)
	ps:update(dt)

	-- footstep triggering
	local x, y, _, _, _, state = self:drawState(W)
	local a = A.archer
	local frame = A.archer:frameFor(state)

	if state == self.States.running and frame ~= self.last_frame then
		self.last_frame = frame
		if frame == 3 or frame == 7 then
			W.events:netpub("player_step", {})
		end
	elseif state ~= self.States.running then
		self.last_frame = nil
	end

	self:pollInput(W)
	local left, right = self.left, self.right
	if self.tried_jumping then
		if self:canJump(W) then
			self:doJump(W)
			self.tried_jumping = nil
		elseif W.time_now - self.tried_jumping > 10/60 then
			-- too old
			self.tried_jumping = nil
		end
	end
	self.left, self.right = left, right

	local speed = player_maxspeed
	if self.pull then
		self.v.x = 0
		self:updatePull(W)
	elseif left then
		if self.v.x >= 0 then
			W.events:netpub("turn", {player=self.id})
		end
		self.v.x = -speed
	elseif right then
		if self.v.x <= 0 then
			W.events:netpub("turn", {player=self.id})
		end
		self.v.x = speed
	else
		self.v.x = 0
	end
end

function player:isTouchingTarget(_W)
	-- avoids infinite loop where the arrow just does nothing over several
	-- frames
	if math.abs(self.pull.mag) < .1 then
		return false
	end
	local W = _W.P
	local y0 = 0
	local g = -arrow.Gravity
	local a = self.pull.dir
	local v = math.min(1, math.abs(self.pull.mag))*arrow.MaxSpeed
	local cosa = math.cos(a)

	local flipped = cosa > 0 and -1 or 1
	local bow = self:bowpoint()
	local xE = flipped * 500
	local xstep = flipped * 3
	local iter = 0
	local x1, y1 = self.p.x, self.p.y
	for x=0, xE, xstep do
		iter = iter + 1
		local y = y0 + (x*math.tan(a)) - ((g*x*x)/(2*math.pow(v*cosa, 2)))
		local x2, y2 = self.p.x + bow.x + x, self.p.y + bow.y + y
		if y > 3000 then
			-- this is a hack to avoid crazy numbers from near 0 values
			return false
		end
		local t, _, _ = W:castRay(x1, y1, x2, y2, function(obj)
			return class.is(obj, "games.archery.target")
		end)
		if t then
			return true
		end
		if iter > 100 then
			return false
		end
		x1, y1 = x2, y2
	end
	return false
end

function player:drawTrajectory(_W)
	if self.pull == nil then return end
	-- avoids infinite loop where the arrow just does nothing over several
	-- frames
	if math.abs(self.pull.mag) < .1 then
		return
	end
	local W = _W.P
	local y0 = 0
	local g = -arrow.Gravity
	local a = self.pull.dir
	local v = math.min(1, math.abs(self.pull.mag))*arrow.MaxSpeed
	local cosa = math.cos(a)

	local points = {}
	local flipped = cosa > 0 and -1 or 1
	local bow = self:bowpoint()
	local xE = flipped * 500
	local xstep = flipped * 4
	local iter = 0
	local fake_arrow = {v={x=xstep}}
	for x=0, xE, xstep do
		iter = iter + 1
		local y = y0 + (x*math.tan(a)) - ((g*x*x)/(2*math.pow(v*cosa, 2)))
		local x2, y2 = self.p.x + bow.x + x, self.p.y + bow.y + y
		local plen = #points
		if points[plen] and points[plen-1] then
			local x1, y1 = points[plen-1], points[plen]
			local t, ex, ey = W:castRay(x1, y1, x2, y2, function(obj)
				return arrow.filter(fake_arrow, obj) == "touch"
			end)
			if t then
				table.insert(points, ex)
				table.insert(points, ey)
				break
			end
		end
		--if #points > 2 and y2 > points[#points - 2] then
		--	break
		--end
		table.insert(points, x2)
		table.insert(points, y2)
		if iter > 100 then
			break
		end
	end

	if #points > 4 then
		local vs, idxs, mode = polyline("miter", points, love.graphics.getLineWidth()/2, 1, false)

		if A.taper then
			for i=1, #vs, 1 do
				local maxp = 60
				local coef = (math.min(maxp, i)/(maxp))
				coef = 1-coef
				--coef = coef * coef
				vs[i][8] = vs[i][8] * coef
			end
		end

		local mesh = love.graphics.newMesh(vs, mode)
		mesh:setVertexMap(idxs)
		love.graphics.setColor(colors.white)
		love.graphics.draw(mesh)
	end
end

function player:drawState(W)
	local flip = self.last_flip or false
	if self.pull then
		flip = math.cos(self.pull.dir) > 0
	elseif self.v.x < 0 then
		flip = true
	elseif self.v.x > 0 then
		flip = false
	end
	self.last_flip = flip

	local states = self.States
	local state = states.idle
	if self.pull then
		state = states.shooting
	--elseif self.isJumping then
	--	state = states.jumping
	elseif not self:canJump(W) then
		if self.v.y < 0 then
			state = states.jumping
		else
			state = states.falling
		end
	elseif math.abs(self.v.x) > .5 then
		state = states.running
	end

	local x, y, sx, sy
	if flip then
		x, y, sx, sy = self.p.x+self.Pivot.x, self.p.y-self.Pivot.y, -1, 1
	else
		x, y, sx, sy = self.p.x+self.Pivot.x, self.p.y-self.Pivot.y, 1, 1
	end
	x, y = math.floor(x), math.floor(y)
	return x, y, sx, sy, flip, state
end

function player:draw1(W, R)
	local states = self.States
	local x, y, sx, sy, flip, state = self:drawState(W)
	local a = A.archer:newBatch(16)

	-- Dust particles
	love.graphics.setLineStyle('rough')
	love.graphics.setColor(colors.white)
	love.graphics.draw(R[self.id].dust)

	-- Background arm, holds bow
	if state == states.shooting then
		local r = flip and self.pull.dir or self.pull.dir+(math.pi)

		local px, py = vec.unpack(self.ArmPivot)
		a:addFrame(state+1, 1, 0, -10, r, sx, sy, px, py)
	end

	-- Body
	local hw, hh = A.archer:getDimensions()
	hw = hw / 2
	hh = hh / 2
	a:add(state, 0, 0, 0, sx, sy, hw, hh)

	-- Foreground arm, holds arrow
	if state == states.shooting then
		local r = flip and self.pull.dir or self.pull.dir+(math.pi)
		local px, py = vec.unpack(self.ArmPivot)
		local pullback = 4*(1-math.abs(self.pull.mag))
		if math.abs(self.pull.mag) > .9 then
			r = r + (math.random()*.2)-.1
		end
		a:addFrame(state+1, 2, 0, 0-10, r, sx, sy, px-pullback, py)
	end

	recolor.apply(A.archer.palette, self.team)
	a:draw(x, y)
	love.graphics.setShader()
end

-- specifically a plugin for the HUD system
function player:drawHUD(x, y)
	recolor.apply(A.archer.palette, self.team)
	A.archer:draw(self.States.idle, x, y, 0, 2, 2)
	love.graphics.setShader()
end

-- screen-space UI
player.offscreen_alpha = 0
player.offscreen_tween = "fadeout"
function player:drawUI(W, R)
	if self.p.x < -R.cam_x then
		if self.offscreen_tween ~= "fadein" then
			self.offscreen_tween = "fadein"
			W.flux:to(self, .5, {offscreen_alpha = 1}):ease("elasticout")
		end
	elseif self.offscreen_tween ~= "fadeout" then
		self.offscreen_tween = "fadeout"
		W.flux:to(self, .3, {offscreen_alpha = 0})
	end

	if self.offscreen_alpha > 0 then
		recolor.apply(A.o_arrow.palette, self.team)
		love.graphics.setColor(255, 255, 255, 255*self.offscreen_alpha)
		local px, py = 10, -16
		A.o_arrow:draw(1, px, self.p.y+py+(self.team*-22)+32,
		0, self.offscreen_alpha, self.offscreen_alpha,
		0, 16)
		love.graphics.setShader()
	end
end

function player:drawText(W, R)
	if self.pull then
		--self:drawTrajectory(W)
	end
end

local function draw_seg(x, y, dx, dy)
	return love.graphics.line(x, y, x+dx, y+dy)
end

function player:drawCollision()
	love.graphics.setColor(self.isJumping and colors.white or colors.red)
	if self.shape1.unpack then
		love.graphics.circle('line', self.shape1:unpack())
	end
	if self.shape2.unpack then
		draw_seg(self.shape2:unpack())
	end
	if self.shape3.unpack then
		draw_seg(self.shape3:unpack())
	end
end

function player:onGround(W)
	return (self.floor or W.time_now - self.last_floor < .2)
end

function player:canJump(W)
	--return not self.pull and self:onGround(W)
	return self:onGround(W)
end

function player:doJump(W)
	self.v.y = -200
	self.last_floor = -math.huge
	self.isJumping = true

	W.events:netpub("jump", {
		player = self.id
	})
end

function player:tryJumping(W)
	if self:canJump(W) then
		self:doJump(W)
	else
		self.tried_jumping = W.time_now
	end
end

function player:stopJumping()
	self.isJumping = false
end

function player:pullString(W, method)
	self.pull = { method = method }
	if type(method) == "userdata" then
		local gp = method

		local d = vec.new(gp:getGamepadAxis('leftx'), gp:getGamepadAxis('lefty'))
		self.pull.dir = math.atan2(d.y, d.x)+math.pi
		--self.pull.mag = -math.min(1, d:len())
		self.pull.mag = 0
	elseif method == "m" then
		local d = vec.new(A.lbox:mouse())
		d:sub_(W.string)
		self.pull.dir = math.atan2(d.y, d.x)
		self.pull.mag = -math.min(1, d:len() / 100)
	else
		error("unsupported input method "..tostring(method))
	end
end


function player:releaseString(W)
	if not self.pull then return end
	if math.abs(self.pull.mag) > .2 then
		local spawn_loc = vec.new(self.p:add(self:bowpoint()))
		local item = arrow.new(W, self, spawn_loc, self.pull.mag, self.pull.dir)
		if A.client then
			A.client.conn:send({
				time_now = W.time_now,
				item = item
			}, 3, "reliable")
		else
			item:addPhysics(W.P)
			W:addItem(item)
		end

		W.events:netpub("arrow_fire", {
			fired_by = self.id,
			arrow = item.id
		})
	end
	self:cancelString()
end

function player:cancelString()
	self.pull = nil
end
-- }}}

-- {{{ Arrows/ammo

function arrow:init(W, parent, pos, mag, dir)
	self.team       = parent.team
	self.fired_by   = parent.id
	self.p          = vec.new(pos)
	self.v          = vec.new(vec.radial(arrow.MaxSpeed*mag, dir))
	self.tail       = vec.new(-1, 0)
	self.wiggle     = 0
	self.anim_clock = 0

	self.trail = Trail.new(self.p)
	self.trail.dotLifetime = .5
end

function arrow:addPhysics(P)
	self.shape = Splash.circle(self.p.x, self.p.y, .5)
	P:add(self, self.shape)
end

function arrow:updatePhysics(P)
	P:update(self, self.p.x, self.p.y)
end

function arrow:removePhysics(P)
	if self.shape then
		P:remove(self)
		self.shape = nil
	end
end

function arrow.filter(self, other)
	if other.parent then
		other = other.parent
	end

	if other.onCross then
		return 'cross'
	elseif class.is(other, "games.archery.target") then
		if other.dir and other.dir*self.v.x < 0 then
			return nil
		end
		return 'touch'
	elseif class.is(other, "games.archery.map") then
		return 'touch'
	elseif class.is(other, "games.archery.prop") then
		return 'touch'
	end
end

local response = class.subclass(archery, "response")

function response:init(W, msg, x, y)
	self.msg = msg
	self.p = vec.new(x, y)
	self.fade = 1
	W.flux:to(self, 1, {fade = 0}):ease("quadout"):oncomplete(function()
		self.dead = true
	end)
end

function response:drawText()
	love.graphics.setColor(misc.rgba(colors.white, 255*self.fade))
	--love.graphics.print(self.msg, self.p.x, self.p.y+(1-self.fade)*4)
	text.chars(self.msg, function(t, s, i, x, y)
		return t:add(s, x, y+misc.osc(1, 1, (i/8)))
	end, self.p.x, self.p.y-((1-self.fade)*4))
end

function arrow:update(W, dt)
	local coef = self.v:len() / 100
	self.anim_clock = (self.anim_clock + (dt*coef*1000/96)) % 3

	self.trail:moveEmitter(self.p)
	self.trail:update(dt)

	for _, data in W.events:iter("hit") do
		if data.arrow == self.id then
			local msg = "HIT!"
			if data.type == "bullseye" then
				msg = "BULLS\n EYE!"
			elseif data.type == "apple" then
				msg = "APPLE\n SHOT!"
			end
			msg = msg .."\n +"..tostring(data.points)

			-- FIXME: spawn to left if we are currently on the far right of the
			-- screen
			W:addItem(response.new(W, msg, data.pos.x+20, data.pos.y))
		end
	end
end

function arrow.respond(W, _self, other, x, y, xgoal, ygoal, normalx, normaly)
	local player = W:findItem(_self.fired_by)
	-- target
	if other.onCross then
		other:onCross(W, _self)
		return
	elseif class.is(other, "games.archery.target") then
		if other.dir and other.dir*_self.v.x < 0 then
			-- ignore touch
			return nil
		end
		player.combo = player.combo + 1
		local bonus_hit = false
		if not player.targets_hit[other.id] then
			local _y  = y - other.p.y
			assert(type(_y)=="number")
			assert(type(other.BonusY1)=="number")
			assert(type(other.BonusY2)=="number")
			local points, shot_type
			if _self.has_apple then
				bonus_hit = true
				points = other.BonusScore*2
				shot_type = "apple"
			elseif _y >= other.BonusY1 and _y <= other.BonusY2 then
				bonus_hit = true
				points = other.BonusScore
				shot_type = "bullseye"
			else
				points = other.Score
				shot_type = "hit"
			end
			player.score = player.score + points
			W.events:pub("hit", {
				arrow=_self.id,
				type = shot_type,
				points = points,
				target=other.id,
				pos=vec.new(x, y)
			})
			player.targets_hit[other.id] = true
		end

		if class.is(other, "games.archery.flyingTarget") then
			local b = vec.new(_self.v.x, _self.v.y)
			b:scale_(1/75)
			if bonus_hit then
				b:scale_(1.5)
			end
			other.surprised = true
			local f = W.flux:to(other.bounce, .05, b)
			f = f:after(other.bounce, .15, {x=0, y=0})
			f = f:after(other, .2, {})
			f:oncomplete(function() other.surprised = nil end)
		end
		-- floor
	elseif other.parent then
		-- pass
		player.combo = 0
	elseif class.is(other, "games.archery.prop") then
		-- pass
		player.combo = 0
	else
		-- no effect
		return
	end
	_self.stuck = true
	if other.id then
		_self.stuck_to = other.id
		_self.stuck_diff = vec.new(_self.p:sub(other.p))
	end
	W.events:pub("arrow_land", {
		arrow = _self.id
	})
	_self.v:set_(0, 0)
	_self.wiggle = 1
	W.flux:to(_self, .5, {wiggle = 0}):ease('linear')
end

function arrow:tick(W, dt)
	if self.stuck then
		self:removePhysics(W.P)
		if self.stuck_to then
			local stuckto = W:tryFindItem(self.stuck_to)
			if stuckto == nil or stuckto.dead then
				self.dead = true
			else
				self.p:set_(stuckto.p)
				if stuckto.bounce then
					self.p:add_(stuckto.bounce)
				end
				self.p:add_(self.stuck_diff)
			end
		end
		return
	end

	self.v.y = self.v.y + (self.Gravity * dt)
	self.p:add_(self.v:scale(dt))
	if self.v:len() > .1 then
		self.tail:set_(self.v):normalize_():scale_(-1)
	end

	self.p.x, self.p.y = W.P:move(self, self.p.x, self.p.y, self.filter, function(...)
		return arrow.respond(W, ...)
	end)
end

local deform = 700
function arrow:draw2(W, R)
	love.graphics.setColor(misc.rgba(teamcolors[self.team], 40))

	if not self.stuck then
		love.graphics.setLineWidth(4)
		self.trail:draw()

		love.graphics.setColor(misc.rgba(teamcolors[self.team], 200))
		love.graphics.setLineWidth(1)
		self.trail:draw()
	end

	local r = 2
	love.graphics.setColor(colors.white)
	local a = self.tail:angle()
	a = a + self.wiggle*misc.osc(.2, math.pi/8)
	local speed_deform = 1+(self.v:len()/deform)
	local batch = R.arrows
	local row = self.stuck and 2 or 1
	if self.has_apple then row = row + 2 end
	local frame = self.stuck and 1 or (math.floor(self.anim_clock)+1)
	batch:addFrame(row, frame, self.p.x, self.p.y, a, -speed_deform, 2-speed_deform, 15, 5)
	batch:attribute("colorscheme", recolor.texy(A.arrow.palette, self.team))
end

function arrow:drawCollision()
	if self.shape then
		love.graphics.setColor(colors.white)
		love.graphics.circle('line', self.shape:unpack())
	end
end

-- }}} Ammo end

-- prop is the base behavior for a non-moving object
local prop = class.subclass(archery, "prop")
prop.Radius = 16
prop.Anim_name = "obstacle_rock1"
prop.offset = vec.new(0, 0)
prop.is_slope = true

function prop:init(W, x, y)
	self.p = vec.new(x, y+12)
	self.shape = Splash.circle(self.p.x, self.p.y, self.Radius)
	W.P:add(self, self.shape)
end

function prop:clean(W)
	W.P:remove(self)
end

function prop:draw2()
	local anim = A[self.Anim_name]
	local w, h = anim:getDimensions()
	anim:draw(1, self.p.x, self.p.y, 0, 1, 1, w*.5, h*.5)
end

function prop:drawCollision()
	love.graphics.setColor(colors.white)
	love.graphics.circle('line', self.shape:unpack())
end

local rock = class.subclass(archery, "rock")
class.mixin(rock, prop)

-- problem with boundaries:
-- because they are line segments, they only collide with the player's ground
-- circle, not the side walls which actually mark the side of the player. This
-- is fine for now: they are invisible walls, so it doesn't particularly matter
-- if the side of a sprite goes through them, but it limits our ability to do
-- square walls in the future without similar hacks
local boundary = class.subclass(archery, "boundary")
--class.mixin(boundary, prop)
--boundary.draw2 = nil

function boundary:init(W, x, y)
	self.p = vec.new(x, y-400)
	self.shape = Splash.seg(self.p.x, self.p.y, 0, 600)
	--W.P:add(self, self.shape) -- unused
	W.boundaries = W.boundaries or {}
	W.boundaries[class.xtype(self)] = W.worldNextId
end

function boundary:tick(W, dt)
	for i=1, #W.worldItems do
		local item = W.worldItems[i]
		if class.is(item, "games.archery.player") then -- or class.is(item, "games.archery.arrow") then
			self:wrap(W, item)
		end
	end
end

function boundary:drawCollision()
	love.graphics.setColor(colors.white)
	draw_seg(self.shape:unpack())
end

local boundaryL = class.subclass(archery, "boundaryL")
class.mixin(boundaryL, boundary)
function boundaryL:wrap(W, item)
	if item.p and item.p.x < self.p.x-2 then
		local other = W:findItem(W.boundaries["games.archery.boundaryR"])
		item.p.x = other.p.x - (self.p.x - item.p.x)
		if item.updatePhysics then
			item:updatePhysics(W.P)
		end
		if item.trail then
			item.trail:moveAll(self.p.x-other.p.x, 0)
		end
	end
end

local boundaryR = class.subclass(archery, "boundaryR")
class.mixin(boundaryR, boundary)
function boundaryR:wrap(W, item)
	if item.p and item.p.x > self.p.x+2 then
		local other = W:findItem(W.boundaries["games.archery.boundaryL"])
		item.p.x = other.p.x + (item.p.x - self.p.x)
		if item.updatePhysics then
			item:updatePhysics(W.P)
		end
		if item.trail then
			item.trail:moveAll(-(other.p.x-self.p.x), 0)
		end
	end
end

-- {{{ Targets
-- targets are things that give you points when you hit them with arrows. they
-- can be anti-targets, meaning you get negative points when you hit them, but
-- same difference tbh
local target = class.subclass(archery, "target")
target.Score = 100
target.BonusScore = 200

local staticTarget = class.subclass(archery, "static_target")
class.mixin(staticTarget, target)

staticTarget.BonusY1 = 6.5
staticTarget.BonusY2 = 10.5

function staticTarget:init(w, x, y, dir)
	self.p = vec.new(x, y)
	self.start = vec.new(x, y)
	self.dir = dir or 1

	self.start_time = w.time_now

	self.shape1 = Splash.seg(self.p.x, self.p.y, 0, 17)
	w.P:add(self, self.shape1)
end

function staticTarget:clean(w)
	w.P:remove(self)
end

function staticTarget:drawUnder(W, R)
	local w, h = A.target_pole:getDimensions()
	local px, py = self.p.x, self.start.y
	local ox, oy = 0, -11 --imgui.Position("o")
	A.target_pole:draw(1, px+ox, py+oy, 0, self.dir, 1, w*.5, 0)
end

function staticTarget:drawCollision()
	love.graphics.setColor(colors.white)
	local x, y, dx, dy = self.shape1:unpack()
	draw_seg(x, y, dx, dy)
	love.graphics.setColor(colors.red)
	love.graphics.line(x, y+self.BonusY1, x, y+self.BonusY2)
end

-- flying targets are held up by birds :)
local flyingTarget = class.subclass(archery, "flying_target")
class.mixin(flyingTarget, target)

flyingTarget.BonusY1 = 6.5
flyingTarget.BonusY2 = 10.5

function flyingTarget:init(w, x, y, dir)
	self.p = vec.new(x, y)
	self.start = vec.new(x, y)
	self.bounce = vec.new(0, 0)
	self.dir = dir

	self.style       = w.rng:random(1, 4)
	self.anim_offset = w.rng:random()

	self.p.x = self.start.x
	self.p.y = 0 --self:targetY()
	self.start_time = w.time_now

	self.shape1 = Splash.seg(self.p.x, self.p.y, 0, 17)
	w.P:add(self, self.shape1)
end

function flyingTarget:draw2()
	local w, h = A.target:getDimensions()
	local px, py = self.p.x, self.start.y
	local bx, by = vec.unpack(self.bounce)
	local ox, oy = 2, 40--imgui.position("ok", 2, 39)
	if self.dir == -1 then
		ox, oy = -1, 40
	end
	local style = self.style + (self.surprised and 4 or 0)
	local f = A.target:frameFor(style, self.anim_offset)
	A.target:drawFrame(style, f, px+bx+ox, py+by+oy, 0, self.dir, 1, w*.5, h*.5)
end

function flyingTarget:clean(w)
	w.P:remove(self)
end

function flyingTarget:targetY()
	local f = A.target:frameFor(self.style, self.anim_offset)
	assert(A.target_offsets[f])
	return self.start.y + A.target_offsets[f]
end

function flyingTarget:tick(w, dt)
	local offx  = ((w.time_now - self.start_time) / 30) * ((1920/4)+400)
	local goalx = self.start.x + offx * -self.dir
	local goaly = self:targetY()

	self.p.x, self.p.y = w.P:move(self, goalx, goaly, function(item, other)
		if class.is(other, 'games.archery.arrow') and other.stuck == nil then
			return 'cross'
		end
	end, function(item, other, ...)
		return arrow.respond(w, other, item, ...)
	end)
	if (w.time_now - self.start_time) > 30 then
		self.dead = true
	end
end

function flyingTarget:draw2()
	local w, h = A.target:getDimensions()
	local px, py = self.p.x, self.start.y
	local bx, by = vec.unpack(self.bounce)
	local ox, oy = 2, 40--imgui.position("ok", 2, 39)
	if self.dir == -1 then
		ox, oy = -1, 40
	end
	local style = self.style + (self.surprised and 4 or 0)
	local f = A.target:frameFor(style, self.anim_offset)
	A.target:drawFrame(style, f, px+bx+ox, py+by+oy, 0, self.dir, 1, w*.5, h*.5)
end

function flyingTarget:drawCollision()
	love.graphics.setColor(colors.white)
	local x, y, dx, dy = self.shape1:unpack()
	draw_seg(x, y, dx, dy)
	love.graphics.setColor(colors.red)
	love.graphics.line(x, y+self.BonusY1, x, y+self.BonusY2)
end


local floatingTarget = class.subclass(archery, "floating_target")
class.mixin(floatingTarget, flyingTarget)

function floatingTarget:tick(w, dt)
	--self.p.x = self.start.x
	--self.p.y = self.start.y + misc.osc(3, 4, 0)
	local goalx = self.start.x
	local goaly = self:targetY()

	self.p.x, self.p.y = w.P:move(self, goalx, goaly, function()
		return nil
	end, nil)
end

local fallingTarget = class.subclass(archery, "falling_target")
class.mixin(fallingTarget, target)

fallingTarget.BonusY1 = 6.5
fallingTarget.BonusY2 = 10.5
fallingTarget.Gravity = 400

function fallingTarget:init(W, x, y)
	self.p = vec.new(x, y)
	self.v = vec.new(0, 0)
	self.start = vec.new(x, y)
	self.bird = vec.new(x, y)

	self.style       = W.rng:random(1, 4)
	self.anim_offset = W.rng:random()

	self.shape1 = Splash.circle(self.p.x, self.p.y, 17)
	W.P:add(self, self.shape1)
	W.flux:to(self.bird, 4, {x = x-330, y = y-180}):oncomplete(function()
		self.bird_finished = true
	end)
end

function fallingTarget:clean(w)
	w.P:remove(self)
end

function fallingTarget:tick(W, dt)
	if self.stuck then
		if self.bird_finished then
			self.dead = true
		end
		return
	end
	self.v.y = self.v.y + self.Gravity * dt
	self.v.y = math.min(200, self.v.y)
	local goalx = self.start.x
	local goaly = self.p.y + self.v.y * dt

	self.p.x, self.p.y = W.P:move(self, goalx, goaly, function(_, other)
		if other.parent then
			return "slide"
		end
	end, function(item, other)
		self.stuck = true
	end)
end

function fallingTarget:draw2()
	local w, h = A.target:getDimensions()
	local px, py = self.p.x, self.p.y
	local bx, by = 0, 0 --vec.unpack(self.bounce)
	local ox, oy = 2, 40--imgui.position("ok", 2, 39)
	local style = self.style + (self.surprised and 4 or 0)
	local f = A.target:frameFor(style, self.anim_offset)
	A.target:drawFrame(style, f, px+bx+ox, py+by+oy, 0, 1, 1, w*.5, h*.5)

	f = A.target:frameFor(style, self.anim_offset)
	px, py = self.bird.x, self.bird.y
	A.target:drawFrame(style, f, px+bx+ox, py+by+oy, 0, 1, 1, w*.5, h*.5)
end

function fallingTarget:drawCollision()
	love.graphics.setColor(colors.white)
	love.graphics.circle('line', self.shape1:unpack())
	love.graphics.setColor(colors.red)
end

-- }}}

local apple = class.subclass(archery, "apple")
apple.Radius = 4

function apple:init(W, x, y, style)
	self.p = vec.new(x, y)
	self.style = style
	self:addPhysics(W.P)
end

function apple:addPhysics(P)
	self.shape = Splash.circle(self.p.x, self.p.y, self.Radius)
	P:add(self, self.shape)
end

function apple:removePhysics(P)
	if self.shape then
		P:remove(self)
		self.shape = nil
	end
end

function apple:tick(W, dt)
	if self.used and W.time_now - self.used >= 10 then
		self.used = nil
	end
end

function apple:onCross(W, arrow)
	if self.used then return end
	W.events:netpub("apple_pierce", {})
	arrow.has_apple = true
	self.used = W.time_now
end

function apple:draw2(W, R)
	local scale = 1
	if self.used then
		scale = (W.time_now - self.used) - 9
		scale = math.max(0, scale)
		scale = scale * scale
	end
	local anim = A.apple
	local w, h = anim:getDimensions()
	anim:draw(self.style, self.p.x, self.p.y, 0, scale, scale, w*.5, h*.5)
end

apple.drawCollision = rock.drawCollision

-- {{{ Map
local function parse_collision(img, x, y, w, h)
	local function is_on(r, g, b, a)
		return a > 5
	end
	local x0, y0, x1, y1
	for ix=x, x+w-1 do
		for iy=y, y+h-1 do
			if is_on(img:getPixel(ix, iy)) then
				x1, y1 = ix-x, iy-y
				break
			end
		end
	end

	for ix=x+w-1, x, -1 do
		for iy=y+h-1, y, -1 do
			if is_on(img:getPixel(ix, iy)) then
				x0, y0 = ix-x, iy-y
				break
			end
		end
	end

	return x0, y0, x1, y1
end

local function prepare_lines(tset)
	if not tset.properties.collision_map then
		error("tileset " .. tset.name .. " is missing field `collision_map`")
	end
	local img = love.image.newImageData("assets/map/"..tset.properties.collision_map)
	local tile_shapes = {}
	local W = math.floor(img:getWidth()/16)
	local tileN = W * math.floor(img:getHeight()/16)
	for i=0, tileN-1 do
		local x, y, w, h = (i % W)*16, math.floor(i / W)*16, 16, 16
		tile_shapes[i+1] = {
			parse_collision(img, x, y, w, h)
		}
	end
	return tile_shapes
end

local map = class.subclass(archery, "map")

local valid_cameras = {["follow"] = true, ["static"] = true}
function map:init(W, map)
	self.tiled = tiled("assets/map/"..map)
	assert(self.tiled.layers.fg, "missing 'fg' layer")
	assert(self.tiled.layers.bg, "missing 'bg' layer")
	if self.tiled.properties and self.tiled.properties.cameraType then
		W.camera_type = self.tiled.properties.cameraType
		assert(valid_cameras[W.camera_type], "invalid camera")
	end

	self:initPhysics(W)
end

function map:initPhysics(W)
	local tset = nil
	for _, set in ipairs(self.tiled.tilesets) do
		if set.name:match("nightbg") then
			tset = set
			break
		end
	end
	assert(tset)
	local linedata = prepare_lines(tset)

	-- generate lines from tiledata, which are just {x0,y0,x1,y1} tables
	local lines = {}
	local fg = self.tiled.layers.fg
	for y=0, fg.height-1 do
		for x=0, fg.width-1 do
			local tile = fg.data[y+1][x+1]
			if tile then
				local _tset = self.tiled.tilesets[tile.tileset]
				if _tset == tset then
					local id = tile.id+1
					local cdata = linedata[id]
					if cdata[1] then
						table.insert(lines, {
							cdata[1]+(x*16), cdata[2]+(y*16),
							cdata[3]+(x*16), cdata[4]+(y*16),
						})
					end
				end
			end
		end
	end

	-- join smaller line segments into a larger segment. This is a hacky,
	-- of-the-top-of-the-head algorithm but hopefully it works well enough that
	-- I won't need to modify it
	local function close_to(x0, y0, x1, y1, threshold)
		threshold = threshold or 3
		return math.abs(x1-x0) < threshold and math.abs(y1-y0) < threshold
	end

	repeat
		local joined = false
		local function join()
			for i1=1, #lines do
				for i2=1, #lines do
					if i1 ~= i2 then
						local line1, line2 = lines[i1], lines[i2]
						-- does line2 start where line1 ends?
						if close_to(line1[3], line1[4], line2[1], line2[2]) then
							local dx1, dy1 = line1[3] - line1[1], line1[4] - line1[2]
							local dx2, dy2 = line2[3] - line2[1], line2[4] - line2[2]
							-- do the two line segments have slope that's close
							-- enough to each other?
							-- note: .15 is a magic number here, I just tweaked
							-- it until I got the results I wanted
							if math.abs(math.atan2(dy1, dx1) - math.atan2(dy2, dx2)) < .15 then
								-- then, join the lines into 1 line
								joined = true
								line1[3], line1[4] = line2[3], line2[4]
								table.remove(lines, i2)
								return
							else
								-- otherwise, make the meeting point the same to
								-- avoid seams
								line1[3], line1[4] = line2[1], line2[2]
							end
						end
					end
				end
			end
		end
		join()
	until joined == false

	-- turn lines into splash segs
	self.splash_shapes = {}
	for _, line in ipairs(lines) do
		table.insert(self.splash_shapes, Splash.seg(line[1], line[2], line[3]-line[1], line[4] - line[2]))
	end

	self.refs = {}
	for i=1, #self.splash_shapes do
		local _, _, dx, dy = self.splash_shapes[i]:unpack()
		table.insert(self.refs, {
			parent = self,
			i = i,
			type = math.abs(dy) > 0 and "slope" or "ground",
			shape = self.splash_shapes[i],
			is_slope = math.abs(dy) > 0,
			is_wall = math.abs(dx) == 0,
		})
		W.P:add(self.refs[i], self.refs[i].shape)
	end
end

local object_types = {
	player,
	{args = true, staticTarget, 1},
	rock,
	boundaryL,
	boundaryR,
	{args = true, staticTarget, -1},
	{args = true, nil}, -- top target
	{args = true, floatingTarget, 1},
	{args = true, nil}, -- ring
	{args = true, floatingTarget, -1},
	{args = true, apple, 1},
	{args = true, apple, 2},
}
function map:spawnObjects(W, player_config)
	local layer = self.tiled.layers.spawn
	for y=0, layer.height-1 do
		for x=0, layer.width-1 do
			local tile = layer.data[y+1][x+1]
			if tile then
				local id = tile.id + 1
				local klass = object_types[id]
				if not klass then
					error("missing object for id " .. tostring(id))
				end

				if id == 1 then
					self:spawnPlayers(W, player_config, x*16, y*16)
				elseif klass.args == true then
					local args = {unpack(klass, 2)}
					klass = klass[1]
					W:addItem(klass.new(W, x*16, y*16, unpack(args)))
				else
					W:addItem(klass.new(W, x*16, y*16))
				end
			end
		end
	end
end

local mkb_in = {
  controls = {
    left  = {'key:left', 'key:a'},
    right = {'key:right', 'key:d'},
    up    = {'key:up', 'key:w'},
    down  = {'key:down', 'key:s'},
    jump  = {'mouse:2', "key:up", "key:w"},
    shoot = {'mouse:1'},
  },
  pairs = {
    move = {'left', 'right', 'up', 'down'}
  },
}

local pad_in = {
  controls = {
    left    = {'axis:leftx-', 'button:dpleft'},
    right   = {'axis:leftx+', 'button:dpright'},
    up      = {'axis:lefty-', 'button:dpup'},
    down    = {'axis:lefty+', 'button:dpdown'},
    jump    = {'button:a'},
    shootGP = {'axis:triggerright+', 'axis:triggerleft+'},
  },
  pairs = {
    move = {'left', 'right', 'up', 'down'}
  },
}

function map:spawnPlayers(W, player_config, x, y)
	for i, playerID in ipairs(player_config) do
		local player = player.new(W, x+W.rng:random(-3, 3), y)
		if playerID == "local" then
			player.nick = A.local_nicks[1]
			player.method = A.controls[1]
			playerID = "local"
		elseif playerID == "local.1" then
			player.nick = A.local_nicks[1]
			player.method = A.controls[1]
			playerID = "local.1"
		elseif playerID == "local.2" then
			player.nick = A.local_nicks[2]
			player.method = A.controls[2]
			playerID = "local.2"
		elseif playerID == "local.3" then
			player.nick = A.local_nicks[3]
			player.method = A.controls[3]
			playerID = "local.3"
		elseif playerID == "local.4" then
			player.nick = A.local_nicks[4]
			player.method = A.controls[4]
			playerID = "local.4"
		elseif A.client and playerID == A.client.clientID then
			player.nick = Settings.nick or "player"
			player.method = A.controls[1]
			playerID = "local"
		else
			if A.client then
				player.nick = A.client.players[playerID].nick
			end
			player.method = "remote"
		end

		W.batons = W.batons or {}
		log("register baton for %s", playerID)
		if player.method == "m" then
			W.batons[player] = baton.new(mkb_in)
		elseif player.method == "all" then
			W.batons[player] = baton.new(mkb_in)
		elseif type(player.method) == 'userdata' then
			local cfg = misc.deepmerge_({}, pad_in)
			cfg.joystick = player.method
			W.batons[player] = baton.new(cfg)
		else
			log("actually nvm %s", playerID)
		end

		if #player_config == 1 then
			player.team = 1
		elseif i < 5 then
			player.team = i
		end
		W:addItem(player)
		W.players[playerID] = player
	end
end

function map:tick(W, dt)
end

function map:drawCollision(W)
	for i, seg in ipairs(self.splash_shapes) do
		local color = colors.white
		love.graphics.setColor(color)
		local x, y, _, _ = seg:unpack()
		--local msg = string.format("%03.f, %03.f, %03.f, %03.f", x, y, x+dx, y+dy)
		--love.graphics.print(msg, 0, 200+(_*20))
		draw_seg(seg:unpack())
		love.graphics.line(x, y, x, y+3)
		--love.graphics.circle('fill', x+dx, y+dy, 2)
	end
end

-- }}}
