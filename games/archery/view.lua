local class   = require 'gabe.class'
local text    = require 'gfx.text'
local recolor = require 'gfx.recolor'
local colors  = require 'gfx.colors'
local vec     = require 'vec'
local misc    = require 'misc'
local keys    = require 'keys'
local assman  = require 'assman'

local _archery = require "games.archery.logic"

local archery = class("games.archery.view")

function archery:init(prev, player_config, mapname)
	misc.timer = function() return self.W.time_now end
	self.W = _archery.new(player_config, mapname)
	misc.timer = love.timer.getTime

	local R = self:renderer()
	self.W:worldEach("initView", R)
end

function archery:enter()
	self.W:startCountdown()
end

function archery:exit()
	A.music:stop()
end

-- stick asset loading here, will be used as a coroutine
function archery.load()
	_archery.load()

	love.graphics.setFont(A.bbattle)
	assman.load.anim("target")
	assman.load.anim("apple")
	assman.load.anim("target_pole")

	assman.load.anim("obstacle_rock1")
	assman.load.anim("male_archer", function(anim)
		A.archer = anim
		anim.img, anim.palette = recolor.generate(anim.img, "assets/teamcolors.png")
	end)

	assman.load.anim("arrow", function(anim)
		anim.img, anim.palette = recolor.generate(anim.img, "assets/teamcolors.png")
	end)

	assman.load.anim("stars")
	assman.load.anim("moon")
	assman.load.anim("hud_players")
	assman.load.anim("hud_time")
	assman.load.anim("offscreen_arrow", function(anim)
		A.o_arrow = anim
		anim.img, anim.palette = recolor.generate(anim.img, "assets/teamcolors.png")
	end)


	A.music:load("trial")
	A.music:load("countdown")

	A.arrow_fire = love.audio.newSource("assets/sfx/arrow_fire.ogg")
	A.arrow_fire:setVolume(.4)

	A.arrow_land = love.audio.newSource("assets/sfx/arrow_land.ogg")
	A.arrow_land:setVolume(.6)

	A.target_hit = love.audio.newSource("assets/sfx/target_hit.ogg")
	A.target_hit:setVolume(.2)

	A.apple_pierce = love.audio.newSource("assets/sfx/apple_pierce.ogg")
	A.apple_pierce:setVolume(1)

	A.time_up = love.audio.newSource("assets/sfx/time_up.ogg")
	A.time_up:setVolume(.2)

	A.steps = {
		love.audio.newSource("assets/sfx/step1.ogg"),
		love.audio.newSource("assets/sfx/step2.ogg"),
		love.audio.newSource("assets/sfx/step3.ogg"),
		love.audio.newSource("assets/sfx/step4.ogg"),
	}
	for _, a in ipairs(A.steps) do a:setVolume(.2) end

	A.bg = A.bg or love.graphics.newImage("assets/map/Night_background_tiles.png")
	A.bg:setWrap("repeat", "clamp")

	A.ground_fg = love.graphics.newImage("assets/map/nightbg_3.png")

	assman.load.image("dust")
end

function archery:updateAudio()
	local W = self.W

	for _ in W.events:iter("start_countdown") do
		A.music:play("countdown")
	end

	for _ in W.events:iter("start_trial") do
		A.music:fadein("trial")
	end

	for _ in W.events:iter("arrow_fire") do
		local a = A.arrow_fire:clone()
		a:play()
	end

	for _ in W.events:iter("time_up") do
		A.music:stop()
		A.time_up:clone():play()
	end

	for _ in W.events:iter("arrow_land") do
		local a = A.arrow_land:clone()
		a:play()
	end

	for _ in W.events:iter("apple_pierce") do
		local a = A.apple_pierce:clone()
		a:play()
	end

	for _, ev in W.events:iter("hit") do
		local arrow  = W:findItem(ev.arrow)
		local player = W:findItem(arrow.fired_by)
		local a = A.target_hit:clone()
		a:setPitch(1 + math.min(12, player.combo-1)/12)
		a:play()
	end

	for _ in W.events:iter("player_step", "jump", "land") do
		local a = A.steps[math.random(#A.steps)]:clone()
		a:play()
	end
end

archery.client_sendrate = 1/30
function archery:tick(dt)
	self.W:tick(dt)
end

function archery:update(dt)
	self.W:update(dt, self:renderer())
	self:updateAudio()

	if A.client and A.client.conn then
		self.time_last_sent = self.time_last_sent or self.W.time_now

		local r = self.client_sendrate
		if self.W.time_now-self.time_last_sent > r then
			self.time_last_sent = self.time_last_sent + r
			A.client.conn:send({
				time_now = self.W.time_now,
				player = self.W:localPlayer()
			}, 2, "unsequenced")
		end
	end
end

function archery:renderer()
	local R = C[self].render
	if not C[self].render then
		R = {}
		R.stars = A.stars:newBatch()

		R.arrows = A.arrow:newBatch(512, "stream")
		R.arrows:enableAttributes {
			{"colorscheme", "float", 1}
		}

		C[self].render = R
	end
	return R
end

function archery:cleanRenderer(R)
	R.stars:clear()
	R.arrows:clear()
end

local rng = love.math.newRandomGenerator()

function archery:drawbg(R, cam_x)
	-- horizon
	local W, H = A.lbox:dimensions()
	local q = love.graphics.newQuad(-16, -16, W+32, H, A.bg:getDimensions())
	love.graphics.draw(A.bg, q, -32+(cam_x/8)%16, 0)

	-- stars
	local sb = R.stars
	local layers = {8, 12, 14}
	rng:setSeed(1)
	for n=1, 13 do
		local l = layers[math.floor(rng:random() * 3)+1]
		local x = rng:random() * W
		local y = rng:random() * H/3
		local row = math.floor(rng:random() * A.stars:getRows()) + 1
		sb:add(row, (x+(cam_x/l)) % W*2, y)
	end
	sb:draw(-20, 0)

	-- moon
	A.moon:draw(1, 400, 40) --imgui.Position("moon"))
end

function archery:draw()
	local R = self:renderer()
	self:cleanRenderer(R)
	local _self = self
	misc.timer = function() return _self.W.time_now end
	self = self.W

	love.graphics.clear(200, 200, 200)
	love.graphics.push()

	local cam_x
	local cam_ox = 175
	if self.camera_type == "static" then
		cam_x = -16
	elseif #A.controls == 1 then
		-- one player camera
		local avg_x = 0
		local n = 0
		for pid, player in pairs(self.players) do
			if pid:match("local") then
				avg_x = player.p.x
			end
		end
		cam_x = cam_ox - avg_x
	else
		-- local multiplayer camera
		--local left_cam, right_cam = imgui.Position("bounds", 0, 280-175)
		local left_cam, right_cam = 0, 280-175
		local avg_x = 0
		local n = 0
		local rightmost = 0
		for pid, player in pairs(self.players) do
			if pid:match("local") then
				avg_x = avg_x + player.p.x
				if player.p.x > rightmost then
					rightmost = player.p.x
				end
			end
		end
		avg_x = avg_x / #A.controls
		local diff = -misc.clamp(rightmost - avg_x, left_cam, right_cam)
		cam_x = cam_ox - (rightmost + diff)
	end

	cam_x = misc.lerp(self.last_cam or cam_x, cam_x, 1)
	--if self.last_cam and cam_x > self.last_cam then
	--	cam_x = self.last_cam
	--end
	self.last_cam = cam_x
	--A.lbox.subx = cam_x - math.floor(cam_x)
	cam_x = math.floor(cam_x)

	_self:drawbg(R, cam_x)

	-- camera
	love.graphics.push()
	love.graphics.translate(cam_x, 0)
	R.cam_x = cam_x
	love.graphics.setColor(colors.white)

	-- tiled
	self.map.tiled:drawLayer(self.map.tiled.layers.bg)

	self:worldEach("drawUnder", self, R)
	self.map.tiled:drawLayer(self.map.tiled.layers.prop1)

	self.map.tiled:drawLayer(self.map.tiled.layers.fg)
	self:worldEach("draw1", self, R)

	self:worldEach("drawTrajectory", self, R)
	self.map.tiled:drawLayer(self.map.tiled.layers.prop2)
	self:worldEach("draw2", self, R)

	local batches = self.map.tiled.layers.fg.batches
	local textures = {}
	for i, batch in ipairs(batches) do
		textures[i] = batch:getTexture()
		batch:setTexture(A.ground_fg)
	end
	love.graphics.setColor(colors.white)
	self.map.tiled:drawLayer(self.map.tiled.layers.fg)
	for i, batch in ipairs(batches) do
		batch:setTexture(textures[i])
	end

	recolor.applyBatched(A.arrow.palette)
	R.arrows:draw()
	love.graphics.setShader()


	if Settings.debug_ui then
		local _
		_, Settings.draw_collision = imgui.Checkbox("draw collision", Settings.draw_collision)
	end
	self:worldEach("drawText", self, R)
	if Settings.draw_collision then
		self:worldEach("drawCollision", self)
	end

	love.graphics.pop()
	love.graphics.pop()
	misc.timer = love.timer.getTime
end

function archery:drawUI()
	local R = self:renderer()
	self = self.W

	local boxW, boxH = A.lbox:dimensions()

	if self.string then
		love.graphics.setColor(colors.white)
		local mouse = vec.new(A.lbox:mouse())
		local diff  = vec.new(mouse:sub(self.string))

		local dir = math.atan2(diff.y, diff.x)
		local mag = (math.min(1, diff:len() / 100))*100
		diff.x, diff.y = mag*math.cos(dir), mag*math.sin(dir)
		mouse.x, mouse.y = self.string.x + diff.x, self.string.y + diff.y

		love.graphics.setLineStyle('rough')
		love.graphics.setLineWidth(math.max(1, (100-mag)/25))
		love.graphics.line(self.string.x, self.string.y, mouse.x, mouse.y)
		love.graphics.setLineWidth(1)
		love.graphics.setScissor(mouse.x-10, mouse.y-10, 20, 20)
		love.graphics.circle('line', self.string.x, self.string.y, 100)
		love.graphics.setScissor()
	end
	self:worldEach("drawUI", self, R)
	if self.countdown_t > 0 then
		love.graphics.push()
		love.graphics.setColor(colors.white)
		local c = math.floor(self.countdown_t)
		c = c >= 1 and tostring(c) or "GO!"
		if self.time_is_up then
			c = "Time is up!"
		end
		love.graphics.translate(boxW*.5, boxH*.5)
		love.graphics.scale(2)
		local text_w = A.bbattle:getWidth(c)
		local text_h = A.bbattle:getHeight()
		local s = 1
		if c ~= "GO!" then
			local sd = self.countdown_t % 1
			s = 1 + (sd * sd)
		end
		love.graphics.print(c, 0, 0, 0, s, s, text_w*.5, text_h*.5)
		love.graphics.pop()
	end

	if self.instruction_t > .001 then
		love.graphics.push()
		love.graphics.setColor(misc.rgba(colors.white, 255*self.instruction_t))
		love.graphics.translate(boxW*.5, boxH*.5)
		love.graphics.scale(2*self.instruction_t)
		local c = "HIT THE TARGETS!"
		local text_w = A.bbattle:getWidth(c)
		local text_h = A.bbattle:getHeight()
		local s = 1
		love.graphics.print(c, 0, -text_h*2, 0, s, s, text_w*.5, text_h*.5)
		love.graphics.pop()
	end

	if self.timeleft then
		love.graphics.push()
		love.graphics.setColor(colors.white)
		local min = math.floor(math.ceil(self.timeleft) / 60)
		local s   = math.ceil(self.timeleft) % 60
		local ms = 0
		if self.timeleft <= 10 then
			ms  = self.timeleft % 1
		end
		local ts = string.format("%02.f:%02.f", min, s)
		love.graphics.translate(boxW*.5, 6)
		love.graphics.scale(2+ms*.1)
		local text_w = A.bbattle:getWidth(ts)
		love.graphics.print(ts, -text_w*.5, 0)
		love.graphics.pop()
	end

	local i = 0
	for _, playerID in ipairs(self.player_keys) do
		if A.client and playerID == A.client.clientID then
			playerID = "local"
		end
		local player = self.players[playerID]
		assert(player, playerID)
		love.graphics.push()
		love.graphics.setColor(colors.white)
		local o1, o2 = 100, 0
		local o = o1
		if i < 2 then o = o2 end
		local D = 380
		love.graphics.translate(o+(D*i/4), 10)
		A.hud_players:draw(i+1, 0, 0)
		love.graphics.stencil(function()
			love.graphics.circle('fill', 24, 24, 14)
		end)
		love.graphics.setStencilTest("greater", 0)
		player:drawHUD(-39, -13)
		love.graphics.setStencilTest()

		assert(player.nick)
		local nx, ny = 40, 5
		text.chars(player.nick, function(t, s, i, x, y)
			return t:add(s, x, y)
		end, nx, ny)

		local sx, sy = 44, 19
		local num = string.format("%06.f", player.display_score)
		-- TODO: come up with some other kind of motion when scores change
		text.chars(num, function(t, s, i, x, y)
			return t:add(s, x, y)
		end, sx, sy)
		love.graphics.pop()
		i = i + 1
	end
end

function archery:recv(data, peer, channel)
	if channel == 2 then
		for playerID, pdata in pairs(data.playerObjects) do
			if playerID ~= A.client.clientID then
				local newP = pdata.player
				assert(newP, require'inspect'(data))
				local oldP = self.W.players[playerID]
				local _, _i = self.W:findItem(oldP.id)
				if _i then
					oldP:removePhysics(self.W.P)
					newP:addPhysics(self.W.P)
					newP.id = oldP.id
					newP.method = "remote"
					self.W.worldItems[_i] = newP
					self.W.players[playerID] = newP
				end
			end
		end
	elseif channel == 3 then
		data.item.id = nil
		if data.item.fired_by == A.client.clientID then
			data.item.fired_by = self.W.players["local"].id
		else
			data.item.fired_by = self.W.players[data.item.fired_by].id
		end
		data.item:addPhysics(self.W.P)
		self.W:addItem(data.item)
	end
end

return archery
