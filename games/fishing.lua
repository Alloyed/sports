local class  = require 'gabe.class'
local colors = require 'gfx.colors'
local misc   = require 'misc'
local vec    = require 'vec'

local game = class(...)
class.mixin(game, require 'games.mixins.world')

local bobber = class((...)..".bobber")
bobber.States = {"above", "falling", "rising", "catching"}

function bobber:init()
	self.p = vec.new(0, 10)
	self.v = vec.new(0, 0)
	self.state = "above"
end

function bobber:tick(W, dt)
	local w, h = A.lbox:dimensions()
	local max_above = 200
	local max_below = 30
	if self.state == "above" then
		self.p.y = 10
		if self.charge then
			local mx, my = A.lbox:mouse()
			local d = vec.new(self.charge.last_pos:sub(mx, my))
			local diff = d:len() / dt
			self.charge.vel = misc.lerp(self.charge.vel, diff, .1)
			self.charge.dir = d:angle()
			self.charge.last_pos:set_(mx, my)
		else
			self.p.x = 10
		end
	elseif self.state == "falling" then
		if self.spin then
			local mx, my = A.lbox:mouse()
			local mx0, my0 = self.spin.last_pos:unpack()
			local cx, cy = self.spin.center:unpack()
			local th1 = math.atan2(my - cy, mx - cx)
			local th0 = math.atan2(my0 - cy, mx0 - cx)
			self.spin.last_pos:set_(mx, my)

			local newvel = misc.diffAngle(th1, th0) / dt
			-- scale velocity back based on distance from center this makes
			-- spinning on the edge more effective than spazzing out in the
			-- center
			local dist = self.spin.center:dist(self.spin.last_pos)
			local factor = math.abs(dist)/100
			newvel = newvel * misc.clamp(factor, .01, 1)

			if newvel > self.spin.vel then
				self.spin.vel = misc.lerp(self.spin.vel, newvel, .0125)
			else
				self.spin.vel = misc.lerp(self.spin.vel, newvel, .025)
			end
			self.spin.th = self.spin.th + self.spin.vel * dt

			-- FIXME: pull toward player
			self.v.y = misc.lerp(self.v.y, -5*self.spin.vel, .1)
		end
		if self.p.y > 77 then
			self.grav = self.grav or 300
			self.grav = misc.lerp(self.grav, 0, .1)
			self.v.y = self.v.y + self.grav * dt

			self.v.x = misc.clamp(self.v.x, -max_below, max_below)
			self.v.y = misc.clamp(self.v.y, -max_below, max_below)

			self.v.x = misc.lerp(self.v.x, 0, .01)
			self.v.y = misc.lerp(self.v.y, 0, .005)
		else
			self.grav = nil
			local grav = 300
			self.v.y = self.v.y + grav * dt

			--self.v.y = misc.clamp(self.v.y, -max_above, max_above)
		end

		self.p:add_(self.v:scale(dt))

		if love.mouse.isDown(2) then
			self.state = "above"
		end
	end
end

function bobber:mousepressed(W, x, y, btn)
	if btn ~= 1 then return end

	if self.state == "above" then
		self.spin = nil
		self.charge = {vel = 0, last_pos = vec.new(x, y), start = vec.new(x, y)}
		return true
	elseif self.state == "falling" then
		self.spin = { 
			center = vec.new(x, y + 100), 
			th = -math.pi/2,
			last_pos = vec.new(x, y),
			vel = 0
		}
		return true
	end
end

function bobber:mousereleased(W, x, y, btn)
	if btn ~= 1 then return end

	if self.state == "above" and self.charge then
		self.v:set_(vec.radial(-.2*self.charge.vel, self.charge.dir))
		self.charge = nil
		self.state = "falling"
		return true
	elseif self.state == "falling" then
		self.spin = nil
		return true
	end
end

function bobber:draw1(W, R)
	love.graphics.circle('fill', self.p.x, self.p.y, 2)
end

function bobber:drawUI(W, R)
	if self.spin then
		local p = self.spin.center
		local th = self.spin.th
		love.graphics.circle('line', p.x, p.y, 100)
		love.graphics.circle('line', p.x, p.y, .5)
		love.graphics.circle('line', p.x + math.cos(th) * 90, p.y+math.sin(th)*90, 5)
	elseif self.charge then
		local mx, my = A.lbox:mouse()
		love.graphics.line(self.charge.start.x, self.charge.start.y, mx, my)
	end
end

local fish = class((...)..".fish")

function fish:init(x, y)
	self.p = vec.new(x, y)
	self.size = vec.new(20, 10)
	self.size:scale_(math.random(4, 10) / 8)
end

function fish:tick(dt)
end

function fish:draw1()
	local w, h   = self.size:unpack()
	local hw, hh = w/2, h/2
	love.graphics.setColor(colors.white)
	love.graphics.rectangle('fill', self.p.x-hw, self.p.y-hh, w, h)
end

function game:init(player_config)
	self.time_now = 0
	self:worldInit()
	self.player = bobber.new()
	self:addItem(self.player)
	for i=1, 10 do
		local x, y = math.random(0, 476), math.random(92, 92+173)
		self:addItem(fish.new(x, y))
	end
end

function game.load()
end

function game:tick(dt)
	self.time_now = self.time_now + dt
	self:worldEach("tick", self, dt)
	self:worldClean()
end

return game
