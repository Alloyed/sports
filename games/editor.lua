local class = require 'gabe.class'
local vec   = require 'vec'
local colors  = require 'gfx.colors'

local game = class(...)
class.mixin(game, require 'games.mixins.world')

local ball = class((...)..".ball")

function ball:init()
	self.p = vec.new(100, 100)
end

function ball:tick(dt)
end

function ball:draw1(W, R)
	love.graphics.circle('fill', self.p.x, self.p.y, 2)
end

local map = class((...)..".map")

function map:init()
	self.grid = {}
end

function map:set(i, x, y, tile)
	if not self.grid[i] then
		self.grid[i] = {}
	end

	if not self.grid[i][x] then
		self.grid[i][x] = {}
	end

	self.grid[i][x][y] = tile
end

function map:get(i, x, y)
	return self.grid[i] and self.grid[i][x] and self.grid[i][x][y]
end

function map:drawLayer(W, R, i)
	if not self.grid[i] then return end
	R[self] = R[self] or {layers = {}}
	R[self].layers[i] = R[self].layers[i] or A.editor:newBatch(4096)
	local layer = R[self].layers[i]

	layer:clear()
	for x, col in pairs(self.grid[i]) do
		for y, tile in pairs(col) do
			layer:addFrame(1, tile, x*24, y*24)
		end
	end

	layer:draw()
end

function game:init(player_config)
	self:worldInit()
	self.map = self:addItem(map.new())
end

function game.load()

end

function game:tick(dt)
	self:worldEach("tick", self, dt)
	self:worldClean()
end

return game
