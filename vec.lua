-- 2d vector operations.
-- You can use the vec.new() to get vector objects, or you can just pass in
-- tables with x/y fields.
local class = require 'gabe.class'

local vec = class(...)

local function xy(x, y)
	if type(x) == 'table' then
		return x.x, x.y
	end
	return x, y
end

function vec:init(x, y)
	self.x, self.y = xy(x, y)
end

function vec.set_(v, x, y)
	v.x, v.y = xy(x, y)
	return v
end

function vec.len(x, y)
	x, y = xy(x, y)
	return math.sqrt(x*x + y*y)
end

function vec.len2(x, y)
	x, y = xy(x, y)
	return x*x + y*y
end

function vec.dist(v, x, y)
	x, y = xy(x, y)
	x, y = v.x - x, v.y - y
	return math.sqrt(x*x + y*y)
end

function vec.dist2(v, x, y)
	x, y = xy(x, y)
	x, y = v.x - x, v.y - y
	return x*x + y*y
end

function vec.add_(v, x, y)
	x, y = xy(x, y)
	v.x = v.x + x
	v.y = v.y + y
	return v
end

function vec.add(v, x, y)
	x, y = xy(x, y)
	return v.x + x, v.y + y
end

function vec.sub_(v, x, y)
	x, y = xy(x, y)
	v.x = v.x - x
	v.y = v.y - y
	return v
end

function vec.sub(v, x, y)
	x, y = xy(x, y)
	return v.x - x, v.y - y
end

function vec.scale_(v, f)
	v.x = v.x * f
	v.y = v.y * f
	return v
end

function vec.scale(v, f)
	return v.x * f, v.y * f
end

function vec.lerp_(v, v2, factor)
	local f = 1.0 - factor
	v.x = (v.x * f) + (v2.x * factor)
	v.y = (v.y * f) + (v2.y * factor)
	return v
end

function vec.lerp(v, v2, factor)
	local f = 1.0 - factor
	v.x = (v.x * f) + (v2.x * factor)
	v.y = (v.y * f) + (v2.y * factor)
end

function vec.normalize_(v, cutoff)
	local len = vec.len(v)
	if cutoff and len < cutoff then
		return vec.scale_(v, 0)
	end
	return vec.scale_(v, 1 / len)
end

function vec.dot(v, x, y)
	x, y = xy(x, y)
	return v.x * x + v.y * y
end

--- uhh
function vec.uhh(v1, v2)
	local x = v1.x*v2.x - v1.y*v2.y
	local y = v1.y*v2.x + v1.x*v2.y
	return x, y
end

function vec.permul(v, x, y)
	x, y = xy(x, y)
	return v.x * x, v.y * y
end

function vec.floorto(x, y, step)
	if type(x) == 'table' then
		step = y
		x, y = x.x, x.y
	end
	step = step or 1
	return math.floor(x / step) * step, math.floor(y / step) * step
end

function vec.floorto_(v, step)
	step = step or 1
	v.x, v.y = math.floor(v.x / step) * step, math.floor(v.y / step) * step
	return v
end

function vec.map(x, y, fn)
	if type(x) == 'table' then
		fn = y
		x, y = x.x, x.y
	end
	return fn(x), fn(y)
end

function vec.map_(v, fn)
	v.x, v.y = fn(v.x), fn(v.y)
	return v
end

function vec.eq(v, x, y)
	x, y = xy(x, y)
	return v.x == x and v.y == y
end

-- doesn't save space but you know
function vec.unpack(x, y)
	return xy(x, y)
end

-- turn into a string usable as a table key and so-on
function vec.stringify(x, y)
	x, y = xy(x, y)
	return tostring(x) .. "|" .. tostring(y)
end

-- convert from radial coords to polar coords. note: radial coords should _never_ be vectors.
function vec.radial(radius, angle)
	return radius*math.cos(angle), radius*math.sin(angle)
end

function vec.angle(x, y)
	x, y = xy(x, y)
	return math.atan2(y, x)
end

return vec
