tup.export("DISPLAY")
ASEPRITE = [[DISPLAY="$DISPLAY" aseprite -b]]
SPFLAGS  = [[--format json-array --sheet-pack --inner-padding=6 --filename-format '{title}.{tag}.{frame}']]
SPFLAGS2 = [[--format json-array --sheet-pack --inner-padding=6 --filename-format '{title}.{tag}.{frame}']]
local function c(...)
	return table.concat({...}, " ")
end

local function add(t, ...)
	for i=1, select('#', ...) do
		table.insert(t, (select(i, ...)))
	end
	return t
end

--local tnames = tup.glob('tiles/*.ase')
--tup.frule {
--	inputs = tup.glob('tiles/*.ase'),
--	command = c(ASEPRITE, "--data tiles.json", "--sheet tiles.png", SPFLAGS2, table.unpack(tnames)),
--	outputs = {"tiles.json", "tiles.png"}
--}
--
--tup.frule {
--	inputs = add(tup.glob('sprites/*.ase'), 'tiles.png'),
--	command = ASEPRITE.." --data sprites.json --sheet sprites.png "..SPFLAGS.." %f", 
--	outputs = {"sprites.json", "sprites.png"}
--}
--
--tup.foreach_rule(tup.glob("frames/*.ase"), ASEPRITE .. " --sheet %o --inner-padding 24 %f", "frame_%B.png")
tup.foreach_rule(tup.glob("music/*.wav"), "oggenc %f -o %o", "music/%B.ogg")
tup.foreach_rule(tup.glob("sfx/*.wav"), "oggenc %f -o %o", "sfx/%B.ogg")
