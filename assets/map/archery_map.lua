return {
  version = "1.1",
  luaversion = "5.1",
  tiledversion = "0.15.0",
  orientation = "orthogonal",
  renderorder = "right-down",
  width = 240,
  height = 18,
  tilewidth = 16,
  tileheight = 16,
  nextobjectid = 1,
  properties = {},
  tilesets = {
    {
      name = "nightbg_2",
      firstgid = 1,
      tilewidth = 16,
      tileheight = 16,
      spacing = 0,
      margin = 0,
      image = "nightbg_2.png",
      imagewidth = 128,
      imageheight = 64,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {
        ["collision_map"] = "nightbg_collision.png"
      },
      terrains = {},
      tilecount = 32,
      tiles = {
        {
          id = 0,
          properties = {
            ["col"] = "0"
          }
        }
      }
    },
    {
      name = "Night_background_tiles",
      firstgid = 33,
      tilewidth = 16,
      tileheight = 16,
      spacing = 0,
      margin = 0,
      image = "Night_background_tiles.png",
      imagewidth = 16,
      imageheight = 144,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 9,
      tiles = {}
    },
    {
      name = "spawners",
      firstgid = 42,
      tilewidth = 16,
      tileheight = 16,
      spacing = 0,
      margin = 0,
      image = "spawners.png",
      imagewidth = 128,
      imageheight = 32,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 16,
      tiles = {}
    },
    {
      name = "bg_objects",
      firstgid = 58,
      tilewidth = 16,
      tileheight = 16,
      spacing = 0,
      margin = 0,
      image = "bg_objects.png",
      imagewidth = 160,
      imageheight = 256,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 160,
      tiles = {}
    },
    {
      name = "wazowski",
      firstgid = 218,
      tilewidth = 16,
      tileheight = 16,
      spacing = 0,
      margin = 0,
      image = "wazowski.png",
      imagewidth = 97,
      imageheight = 95,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 30,
      tiles = {}
    }
  },
  layers = {
    {
      type = "tilelayer",
      name = "bg",
      x = 0,
      y = 0,
      width = 240,
      height = 18,
      visible = false,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJztzrcRAlEUBMHLP0S05tAaXDYAjG/xnO6q8afrAAAAAAAAAAAAAAAA/m+QhmmUxmmSpqVHQKtZmqdFWqZVWlcOAc02aZv6tEv7dCg9Alod0ymd0yVd0630CGh1T4/0TK/0Tp/SI+CXL2D2G0A="
    },
    {
      type = "tilelayer",
      name = "fg",
      x = 0,
      y = 0,
      width = 240,
      height = 18,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJzt0cEOgjAQRVEERAVEQTf+/4/6FtOENGiCQgfjPcndFDNDJcsAAAAAAAAAAAAAAAAAAAAAAAAAP6U6qr06qEr1alA3dVc7e5bbbwuXN12G933n7l963q9/v5j3/dfcX9rcyuaEWfnovFVXdVYX1alH1Mme1fbbZvSOYV4R7QvP+y8bov9i6uzdeer7Lr1/a/fhe6bb39rcLppVT5x/WpjXvNjnnfd919r/L99va/f33k9ERGl6AnoLRwk="
    },
    {
      type = "tilelayer",
      name = "prop1",
      x = 0,
      y = 0,
      width = 240,
      height = 18,
      visible = false,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJztlMlOFkEURvu5FHcqO8Ud4E70FZQNCbohAZc4YeJsIiiYyKAmgFOCEyYoqK/j+RedtJ3qrumvrh/yneSsuuv2vfVVV1EIIYQQQgghhBBCCCGEEEIIIUQ487kb6JDerDfwJt7C23gHF/Buhz30q07uWQaB3Of3KOXZrx5c6/ju3T28jw/wIT7Cx/gEn3rWSoHPPLlmiT2vg3BOm8h5F4TkeQyP4xCeyNRDyjp1nuEiLuFzfIHLuIIvI+rm4LDOkiLbfp/jHITkeRJP4WkcztRDbB2f7F7hKq7hOm7ga3yDbz36GwRSzxL7TzStT3HvxJzjQfn3Q/I8g2dxBM9l6iG2jk92m7iF2/gO3+MH/IifPPrzJcXdkHqW2Lu9aX2KeyfmHMfOWeYQS0ieoziG43je8Hyqgx5i6vTe88luBz/jF/yK3/A77uIPy9r62XLNrd7zjuGdcp83HepV64TO4kLs3d603iXby3gFJx2+09uHpnPscn7rfbpkWq1b5tCGyzwheV7ACbyIlwzPZyx9VdkN7MGETx2f7PbwJ/7CfTzA3/gH/9bera433T9ThT03U897hvd89rlax3WWEGx3u4nqnjWtd8l2Gq/iNYdv9vah6Ry77Gu9T1um9bplDm1MF/Z5UufZRvmdfvXgWic2uzaq6033xkxhz813dtO/nYumfW3rsbpnbett2c7iHF4Pb9+Zep+2TEPoch7xP13dgUIIIYTIwz90pEpz"
    },
    {
      type = "tilelayer",
      name = "prop2",
      x = 0,
      y = 0,
      width = 240,
      height = 18,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJztwQEBAAAAgiD/r25IQAEAAAAAAAAAAAAAAAAAAADArwFDgAAB"
    },
    {
      type = "tilelayer",
      name = "spawn",
      x = 0,
      y = 0,
      width = 240,
      height = 18,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJzt2EESgyAMAEDe02r//7yevDljaFVM2D0DkwTCMLQGAEBV79EBkNZVZ+fXdUedZT00B/sM+yK9UbF/KuY0E/sHcb39svwxN6tonrPUY5Ml3yxxAvdxL5xDHaHPcjwEgIfy7gGAZ1pHBwBJfUYHAIRV/VN8Xbx+1bpFzZ4/AMDZvgGTBJM="
    }
  }
}
