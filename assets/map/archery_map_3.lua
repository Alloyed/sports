return {
  version = "1.1",
  luaversion = "5.1",
  tiledversion = "0.15.0",
  orientation = "orthogonal",
  renderorder = "right-down",
  width = 35,
  height = 18,
  tilewidth = 16,
  tileheight = 16,
  nextobjectid = 1,
  properties = {
    ["cameraType"] = "static"
  },
  tilesets = {
    {
      name = "nightbg_2",
      firstgid = 1,
      tilewidth = 16,
      tileheight = 16,
      spacing = 0,
      margin = 0,
      image = "nightbg_2.png",
      imagewidth = 128,
      imageheight = 64,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {
        ["collision_map"] = "nightbg_collision.png"
      },
      terrains = {},
      tilecount = 32,
      tiles = {
        {
          id = 0,
          properties = {
            ["col"] = "0"
          }
        }
      }
    },
    {
      name = "Night_background_tiles",
      firstgid = 33,
      tilewidth = 16,
      tileheight = 16,
      spacing = 0,
      margin = 0,
      image = "Night_background_tiles.png",
      imagewidth = 16,
      imageheight = 144,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 9,
      tiles = {}
    },
    {
      name = "spawners",
      firstgid = 42,
      tilewidth = 16,
      tileheight = 16,
      spacing = 0,
      margin = 0,
      image = "spawners.png",
      imagewidth = 256,
      imageheight = 32,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 32,
      tiles = {}
    },
    {
      name = "bg_objects",
      firstgid = 74,
      tilewidth = 16,
      tileheight = 16,
      spacing = 0,
      margin = 0,
      image = "bg_objects.png",
      imagewidth = 160,
      imageheight = 256,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 160,
      tiles = {}
    },
    {
      name = "wazowski",
      firstgid = 234,
      tilewidth = 16,
      tileheight = 16,
      spacing = 0,
      margin = 0,
      image = "wazowski.png",
      imagewidth = 97,
      imageheight = 95,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 30,
      tiles = {}
    }
  },
  layers = {
    {
      type = "tilelayer",
      name = "bg",
      x = 0,
      y = 0,
      width = 35,
      height = 18,
      visible = false,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJxjYBgFo2AUjIJRMApGwSgYnoCRcaBdQF8AAAu3AAM="
    },
    {
      type = "tilelayer",
      name = "prop1",
      x = 0,
      y = 0,
      width = 35,
      height = 18,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJztzsdqQkEYhmGv0LKLukt0Z9mpuR8TG9gFu2DUKCSxgSYWOLHE28grKMhgOR7wuJkPHvhnYOb7DQYZGRmZ+yeo4//b+QWvCCGMCKKI3XgPMXEkkEQKaWSQRe7Kv4wwwQyLhl3yKKCIEsqooIrahbcd4fwAK2ywa9iljjc00EQL72gf6TrMAEPh7hFPcMCpsv9DmD/xhS566J/o2md64t4FNzzwqtzlMCOM8Y0fTHZdM8yhnHm7Ec4++BHAs8p+RZh/scASK6zxd6RLRr/8A/IfOv8="
    },
    {
      type = "tilelayer",
      name = "fg",
      x = 0,
      y = 0,
      width = 35,
      height = 18,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJxjYBgFo2AUjIJRMApGwXAHLEDMCsTsODATEDMTMEMYiEWgNDobGx+bHIjmAWJeIObHgbmAmBtJrxCaWSC+5CjGigGOOQl1"
    },
    {
      type = "tilelayer",
      name = "prop2",
      x = 0,
      y = 0,
      width = 35,
      height = 18,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJxjYBgFo2AUjIJRMApGwXACvTQ2fw0QryVS7XQy9FBi30gHAJexBTA="
    },
    {
      type = "tilelayer",
      name = "spawn",
      x = 0,
      y = 0,
      width = 35,
      height = 18,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJxjYBgFhID+QDtgFFAVaA+0A4YhGA1T6gKTYWT/QPuFEqA7QPbqDZC91ABaVDRLh4pmjSQAAGV5AdM="
    }
  }
}
