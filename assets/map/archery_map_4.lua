return {
  version = "1.1",
  luaversion = "5.1",
  tiledversion = "0.15.0",
  orientation = "orthogonal",
  renderorder = "right-down",
  width = 35,
  height = 18,
  tilewidth = 16,
  tileheight = 16,
  nextobjectid = 1,
  properties = {
    ["cameraType"] = "static"
  },
  tilesets = {
    {
      name = "nightbg_2",
      firstgid = 1,
      tilewidth = 16,
      tileheight = 16,
      spacing = 0,
      margin = 0,
      image = "nightbg_2.png",
      imagewidth = 128,
      imageheight = 64,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {
        ["collision_map"] = "nightbg_collision.png"
      },
      terrains = {},
      tilecount = 32,
      tiles = {
        {
          id = 0,
          properties = {
            ["col"] = "0"
          }
        }
      }
    },
    {
      name = "Night_background_tiles",
      firstgid = 33,
      tilewidth = 16,
      tileheight = 16,
      spacing = 0,
      margin = 0,
      image = "Night_background_tiles.png",
      imagewidth = 16,
      imageheight = 144,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 9,
      tiles = {}
    },
    {
      name = "spawners",
      firstgid = 42,
      tilewidth = 16,
      tileheight = 16,
      spacing = 0,
      margin = 0,
      image = "spawners.png",
      imagewidth = 256,
      imageheight = 32,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 32,
      tiles = {}
    },
    {
      name = "bg_objects",
      firstgid = 74,
      tilewidth = 16,
      tileheight = 16,
      spacing = 0,
      margin = 0,
      image = "bg_objects.png",
      imagewidth = 160,
      imageheight = 256,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 160,
      tiles = {}
    },
    {
      name = "wazowski",
      firstgid = 234,
      tilewidth = 16,
      tileheight = 16,
      spacing = 0,
      margin = 0,
      image = "wazowski.png",
      imagewidth = 97,
      imageheight = 95,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 30,
      tiles = {}
    }
  },
  layers = {
    {
      type = "tilelayer",
      name = "bg",
      x = 0,
      y = 0,
      width = 35,
      height = 18,
      visible = false,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJxjYBgFo2AUjIJRMApGwSgYnoCRcaBdQF8AAAu3AAM="
    },
    {
      type = "tilelayer",
      name = "prop1",
      x = 0,
      y = 0,
      width = 35,
      height = 18,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJxjYBgFo2AUjIKBB0voaD6t7RqJ4ORAOwAKLgHxZSqYc4ZC/bep4AZc4DqZ+p5Twe4HVDBjFNAWAACwjgig"
    },
    {
      type = "tilelayer",
      name = "fg",
      x = 0,
      y = 0,
      width = 35,
      height = 18,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJztz7sKgDAUg+F6Ba9QdfH9X9R/qNBBkXKqLgl8cxLnFEVRFOU6NRq0WLGhQInq4/4RE2bsQYcew0u7nvrv5Nzlg5R+y674s48sht6UXWfXkumzddcfvRYHoDMN1A=="
    },
    {
      type = "tilelayer",
      name = "prop2",
      x = 0,
      y = 0,
      width = 35,
      height = 18,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJztzllKgmEAQFH3vw5Tcx4SNCdQ04LKCcwRnN2GZxWfmP+F835jsaio/1mcFxIkSfFKmkzglzwFipQoU+GNauCXGnXeadCkRZtO4JcuPfp8MGDIJ1+BX7754ZcRYyZMmQV+mfPHgiUr1mzYBn7ZsefAkRNnLlwDv0Q9R1ly95544G53AyyO"
    },
    {
      type = "tilelayer",
      name = "spawn",
      x = 0,
      y = 0,
      width = 35,
      height = 18,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJxjYBg6QHugHTAKRsEoGAWjgC5Af6AdAAS6A2Sv3gDZO1iAFpTWGVBXDF0AAHhlAQw="
    }
  }
}
