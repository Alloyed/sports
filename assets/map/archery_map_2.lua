return {
  version = "1.1",
  luaversion = "5.1",
  tiledversion = "0.15.0",
  orientation = "orthogonal",
  renderorder = "right-down",
  width = 240,
  height = 18,
  tilewidth = 16,
  tileheight = 16,
  nextobjectid = 1,
  properties = {},
  tilesets = {
    {
      name = "nightbg_2",
      firstgid = 1,
      tilewidth = 16,
      tileheight = 16,
      spacing = 0,
      margin = 0,
      image = "nightbg_2.png",
      imagewidth = 128,
      imageheight = 64,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {
        ["collision_map"] = "nightbg_collision.png"
      },
      terrains = {},
      tilecount = 32,
      tiles = {
        {
          id = 0,
          properties = {
            ["col"] = "0"
          }
        }
      }
    },
    {
      name = "Night_background_tiles",
      firstgid = 33,
      tilewidth = 16,
      tileheight = 16,
      spacing = 0,
      margin = 0,
      image = "Night_background_tiles.png",
      imagewidth = 16,
      imageheight = 144,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 9,
      tiles = {}
    },
    {
      name = "spawners",
      firstgid = 42,
      tilewidth = 16,
      tileheight = 16,
      spacing = 0,
      margin = 0,
      image = "spawners.png",
      imagewidth = 256,
      imageheight = 32,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 32,
      tiles = {}
    },
    {
      name = "bg_objects",
      firstgid = 74,
      tilewidth = 16,
      tileheight = 16,
      spacing = 0,
      margin = 0,
      image = "bg_objects.png",
      imagewidth = 160,
      imageheight = 256,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 160,
      tiles = {}
    },
    {
      name = "wazowski",
      firstgid = 234,
      tilewidth = 16,
      tileheight = 16,
      spacing = 0,
      margin = 0,
      image = "wazowski.png",
      imagewidth = 97,
      imageheight = 95,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 30,
      tiles = {}
    }
  },
  layers = {
    {
      type = "tilelayer",
      name = "bg",
      x = 0,
      y = 0,
      width = 240,
      height = 18,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJzt00cOwjAARNGEegHuf0Bq6L0MB0CKkMBZvCf/jVcjS64qAAAAAAAAAAAAAACA/5umWZqnRVqmVdFFQFtNWqdN2qZd2hddBN0zKT3gg0M6plM6p0u6Fl30va6+MfzKLd3TIz3fF3VOXXQS0FIvf7WfBmmYRmns/0LnvACfLBVQ"
    },
    {
      type = "tilelayer",
      name = "fg",
      x = 0,
      y = 0,
      width = 240,
      height = 18,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJztmdtugzAQBd2ml7TpDcNLm///z64lLLYbQ0gcwNAZaRQHIoHNWRs5zgEAAAAAAAAAAAAAAAAAAAAA5PEgPopPYi16sWmtF7wv+PtsKtc9m6q1Ud+9OlapY8E78Vl5L+573M3SM8hB5+JNfBc/xG/jcaJrhnkh5My37ahPtJsLzoechmyuOYNjn80lvoifylfxq8eD68axr8aHXPPYl8w1ufhZ4Jq5OQ3Z1BmcOk92jYzzUmU+4/HUfc09TmPHsa/Ghzy40/U/vhPYdwUYJjcX16y/pWQxVcu3Wi9y+qjvK+a8hJqd29y1Yavcsn7GjnEpNXuuZs69Lw7Vcul9XJvUb8dU2Rpaf7eUZ70m6vff2PYb6GNphmwtvVdh9090O/U9dS71m9S+TmPOedWeKltHc119H+QZc41zpv7Pw86dtTnXmGN2z93uxcd27U73IpbuPyIi4n/0F+mjfMU="
    },
    {
      type = "tilelayer",
      name = "prop1",
      x = 0,
      y = 0,
      width = 240,
      height = 18,
      visible = false,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJztlMeKFUEYRvv5DDvDzrDTcWd4BXUzoG4G1NWYcwazoI4BxgxjGqHNr+FpmIa2qK5boavrKt+Bw13c6r//1FVVQgghhBBCCCGEEEIIIYQQQggRz/HSCYxIU+sJPImn8DSewbN4bsQchopTupZpoPT+/k/zHCoH3zihvbuIl/AyXsGreA2v443AWDkIqadULan7Og172kfJuyBmnqtwNa7BtYVyyBnH5Cbewtt4B+/iPbyPDxLiluBfrSXHbIfe4xLEzHMdrscNuLFQDqlxQmb3EB/hY1zAJ/gUn+HzgPymgdy15Pomctw7KXs8Ld9+zDw34WbcglsL5ZAaJ2R2i/gCX+IrfI1v8C2+C8gvlBx3Q+5ahr7bW3LcOyl7nFpnO4dUYua5DbfjDO6w/D87Qg4pcZpzIbNbwvf4AT/iJ/yMy/hlwrPmbvnOzcx5yXKm7fOiR7xunNhafBj6bm/xme1e3If7PeI1fejbY5/9Nev0mWk37jwe6/za8KknZp47cRfuxj2W/+cc7zNZjsyhS9sDVxyzTyGzq/ErfsPv+AN/4i/8bZztPm+7f2ZXcnRhq722nAvpczeOby0xTLrbbfh8Lz470sQ5iIc84tVV/x779NWs0/Ud2uKexwudXxsHqsn11FXeebpo35OaQ9sDV5xun5pzKbNz0X3edv/MreToIrT/tee5Mejra+14pq/n5vOTdqSJcwSPBuYcg1mn6zuM5XA1Xj3ib8a6A4UQQghRhj/SEWtY"
    },
    {
      type = "tilelayer",
      name = "prop2",
      x = 0,
      y = 0,
      width = 240,
      height = 18,
      visible = false,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJzt07EJACAMRUH3H0Nb3VEbGzsloMgdpP2keSkBAAAAAAAAAAAAAADwojyu3H4CHrd2EtXN3Dndq+NawB/ws7WTqG7mjg4BANjRAUHnDRc="
    },
    {
      type = "tilelayer",
      name = "spawn",
      x = 0,
      y = 0,
      width = 240,
      height = 18,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJzt20EOhCAMBVDPMzpz/+O5NQYTYCCl8b2lMaRCP+jCbSv7PlwHxjiSjg3IGLns0QUsRn7rzeqd3nGjelmG1jEzv9YZymqyUXNPtvPXnpCb9RsrW35p05qXaz+8JWu1z7nifHh/zlMn3Dl/57EvjGEen8kvJfoiB+sEefn+hbycv5BXdH794wD9ovML9Lvn9xdSxX8+0QUAAECDExyBBvw="
    }
  }
}
