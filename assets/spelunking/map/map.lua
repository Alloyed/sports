return {
  version = "1.1",
  luaversion = "5.1",
  tiledversion = "0.15.0",
  orientation = "orthogonal",
  renderorder = "right-down",
  width = 256,
  height = 22,
  tilewidth = 16,
  tileheight = 16,
  nextobjectid = 1,
  properties = {},
  tilesets = {
    {
      name = "spawners",
      firstgid = 1,
      tilewidth = 16,
      tileheight = 16,
      spacing = 0,
      margin = 0,
      image = "spawners.png",
      imagewidth = 128,
      imageheight = 32,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 16,
      tiles = {}
    },
    {
      name = "tiles",
      firstgid = 17,
      tilewidth = 16,
      tileheight = 16,
      spacing = 0,
      margin = 0,
      image = "tiles.png",
      imagewidth = 32,
      imageheight = 16,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 2,
      tiles = {}
    },
    {
      name = "World_tileset",
      firstgid = 19,
      tilewidth = 16,
      tileheight = 16,
      spacing = 0,
      margin = 0,
      image = "World_tileset.png",
      imagewidth = 128,
      imageheight = 80,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {
        {
          name = "bg",
          tile = -1,
          properties = {}
        }
      },
      tilecount = 40,
      tiles = {
        {
          id = 0,
          terrain = { 0, 0, 0, 0 }
        },
        {
          id = 1,
          terrain = { 0, 0, 0, 0 }
        },
        {
          id = 2,
          terrain = { 0, 0, 0, 0 }
        },
        {
          id = 3,
          terrain = { 0, 0, 0, 0 }
        },
        {
          id = 4,
          terrain = { 0, 0, 0, 0 }
        },
        {
          id = 5,
          terrain = { 0, 0, 0, 0 }
        },
        {
          id = 6,
          terrain = { 0, 0, 0, 0 }
        },
        {
          id = 7,
          terrain = { 0, 0, 0, 0 }
        },
        {
          id = 8,
          terrain = { 0, 0, 0, 0 }
        },
        {
          id = 9,
          terrain = { 0, 0, 0, 0 }
        },
        {
          id = 10,
          terrain = { 0, 0, 0, 0 }
        },
        {
          id = 11,
          terrain = { 0, 0, 0, 0 }
        },
        {
          id = 12,
          terrain = { 0, 0, 0, 0 }
        },
        {
          id = 13,
          terrain = { 0, 0, 0, 0 }
        },
        {
          id = 14,
          terrain = { 0, 0, 0, 0 }
        },
        {
          id = 15,
          terrain = { 0, 0, 0, 0 }
        },
        {
          id = 16,
          terrain = { 0, 0, 0, 0 }
        }
      }
    }
  },
  layers = {
    {
      type = "tilelayer",
      name = "bg",
      x = 0,
      y = 0,
      width = 256,
      height = 22,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJztw0ENAAAMA6EzUv82Z2JPSFg1VVVVVVVVVVVVVVVVVVVVVVVVVVVV9fUBwX1+Hw=="
    },
    {
      type = "tilelayer",
      name = "fg_data",
      x = 0,
      y = 0,
      width = 256,
      height = 22,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJzt2EtuwzAQBFFdwbn/YbPKxgARWpwxW9IroLc0x63Sh6/jOF4iIiIib7kyyXMm7AHPJtGLSvgPjEn0ohL+A2MSvaiE/8CYRC8q4T8w5hte/JzI7Dr/keB/9x4+WX9lHx1rYi/85z//n4venjPnKvy/H1fobfZZf5aOOVP+u0q63iuwD53xf4bO7wrsQ2fPmXMF/t+Tlc7OnOvNnOFVrTv7zZB0bVZ5Vunf7t9HH/yvuzYr1knzP2EP6ENfWf6nkHQPQh+f9PX+TK3usvOMH3N0v3/wP4uVvnR5H77hvftAHvxf58oO7PA+ZfY/7jTLiJ09i4iIiIhIRn4BTeOKLg=="
    },
    {
      type = "tilelayer",
      name = "fg",
      x = 0,
      y = 0,
      width = 256,
      height = 22,
      visible = false,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJzt2usOgjAMhmEuRuQPev+XJyaaLLhDGa102/skTUiMY5h+CpvTBAAAAAAAerdcPYGTpPO/4jqXSAGetN6T0lyRf+BX6z1J/oF6rfck+QfqafTkutVjq+enwuNU5cYJxyuJ5SqWO8vshfMNj8k/vNPoyVTeciUdR/PcVjzMAagxQk+SPSDOQy6+98xWLPJvPWfgHzzk35pF/rmnQA9G6GGyCsTlcrFf1z5aqTV8yfv2ewpH9hFi1+gl/zdBzVvdr5oghpLLRc26vmTNW2tcaaa18q/xzM9eATwZoc+08kQu0Zuwn/e/b7PyuVbl8QCcU7r/B9Av8l82nyjAs3fGU72be22kqv0e1Pr8ACul/+ID6NcLrGk3hQ=="
    },
    {
      type = "tilelayer",
      name = "spawn",
      x = 0,
      y = 0,
      width = 256,
      height = 22,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJztzjEBAAAIA6DZv7Qd9ugBCUgAAAAAAAAAAAAAAAAAAAAAAIDP5joAAAAAVBZn1AAC"
    }
  }
}
