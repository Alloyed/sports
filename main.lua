Settings = require 'settings'
require 'gabe' (not Settings.debug)

local suit_gp = require 'suit_gamepad'
local state  = require 'gabe.state'
local events = require 'events'
local gs     = require 'gamestate'
local reload = require 'gabe.reload'
local assman = require 'assman'
local lbox   = require 'gfx.lbox'
local colors = require 'gfx.colors'
local music  = require 'music'
local stats  = require 'gfx.stats'

require 'globals' ()

A = A or {}
assman.init(A)
function state.start()
	A.controls = A.controls or {"all"}
	A.VPoints  = A.VPoints  or {0}
	if not A.is_server and not A.lbox then
		A.lbox  = lbox.new(2, 1920/4, 1080/4, not Settings.stretch)
		A.music  = A.music or music.new()
		A.music:setMute(Settings.mute_music)
		love.audio.setVolume(Settings.volume)
	end

	if A.lbox then
		love.graphics.setFont(A.bbattle)
	end

	S.playlist = {i = 1,
		{"games.archery.view", "archery_map_3.lua"},
		{"games.archery.view", "archery_map_3.lua"},
		{"games.archery.view", "archery_map_4.lua"},
	}
	S.events = events.new()
	S.frame_timer = 0
	S.input_method = "mouse"
	gs.reset(Settings.view)
	if Settings.debug then
		if _G._eval then
			_G._eval()
		end
	end
end

function love.load()
	local pull = nil
	local eval = nil
	for i=1, #arg do
		if pull then
			if not pull(arg[i]) then
				pull = nil
			end
		elseif arg[i] == "--server" then
			A.is_server = true
			Settings.view = "views.server"
		elseif arg[i] == "--view" then
			pull = function(a)
				Settings.view = a:gsub("%.lua$","")
			end
		elseif arg[i] == "--map" then
			pull = function(a)
				Settings.map = a
			end
		elseif arg[i] == "--nick" then
			pull = function(a)
				Settings.nick = a
			end
		elseif arg[i] == "--scale" then
			pull = function(a)
				Settings.scale = tonumber(a)
				if not Settings.scale then error("invalid scale: " ..tostring(a)) end
			end
		elseif arg[i] == "--volume" then
			pull = function(a)
				Settings.volume = tonumber(a)
				if not Settings.scale then error("invalid scale: " ..tostring(a)) end
				love.audio.setVolume(Settings.volume)
			end
		elseif arg[i] == "--mute" then
			Settings.volume = 0
			love.audio.setVolume(Settings.volume)
		elseif arg[i] == "--mute-music" then
			Settings.mute_music = true
			A.music  = A.music or music.new()
			A.music:setMute(Settings.mute_music)
		elseif arg[i] == "--controls" then
			pull = function(a)
				A.controls = {a}
				print(require'inspect'(A.controls))
			end
		elseif arg[i] == "--joinroom" then
			Settings.view = "views.pickroom"
			A.submitroom = true
		elseif arg[i] == "--local" then
			A.local_server = true
		elseif arg[i] == "--eval" then
			assert(Settings.debug == true, "debug mode must be on to use eval")
			pull = function(a)
				local preamble = [[
				local gs = require 'gamestate'
				local misc = require 'misc'
				]]
				local chunk, err = loadstring(preamble .. a)
				if chunk then
					eval = chunk
				else
					error(err)
				end
			end
		elseif arg[i] == "--fullscreen" then
			Settings.fullscreen = true
		elseif arg[i] == "--stretch" then
			Settings.stretch = true
		end
	end


	if not A.is_server then
		love.window.setMode(1920*Settings.scale/4, 1080*Settings.scale/4, {
			resizable  = Settings.resizable,
			fullscreen = Settings.fullscreen,
			vsync      = Settings.vsync,
		})
		love.window.setTitle("SPORTES")
		if Settings.debug then
			require 'dbg_ui' ()
		end

		assman.load.image("pixel")
		assman.load.shader("silhouette")
		assman.load.bmfont("bbattle")
		love.graphics.setFont(A.bbattle)

		A.ping = love.audio.newSource("assets/sfx/ping.ogg")
		--A.ping:setVolume(.2)

		A.target_hit = love.audio.newSource("assets/sfx/target_hit.ogg")
		A.target_hit:setVolume(.2)

		love.joystick.loadGamepadMappings("gamepads.txt")
		A.local_nicks = {Settings.nick}
	end

	require 'repler'.load()
	_G._eval = eval
end

function love.update(dt)
	if imgui then imgui.NewFrame() imgui.pollCCs() end
	suit_gp.update()

	-- misc always update assets
	if A.client then
		A.client:update(dt)
	end

	if A.server then
		A.server:update(dt)
	end

	if S.events then
		S.events:swap()

		for _ in S.events:iter("ding") do
			local a = A.ping:clone()
			a:setVolume(.75)
			a:setPitch(math.random()+.5)
			a:play()
		end

		for _ in S.events:iter("test_volume") do
			A.target_hit:stop()
			A.target_hit:play()
		end
	end
	if A.music then
		A.music:update(dt)
	end

	if S.gamestate.update then
		S.gamestate:update(dt)
	end

	if S.gamestate.tick then
		-- this resolves a few collision bugs
		S.gamestate:tick(dt/4)
		S.gamestate:tick(dt/4)
		S.gamestate:tick(dt/4)
		S.gamestate:tick(dt/4)
	end

	-- suit
	-- FIXME: if you enterFrame here, keyboard input breaks. If you don't
	-- enterFrame, then gamestate transitions are broken
	--if not class.is(S.gamestate, "views.pickroom") then
	--	suit.enterFrame()
	--end
	if S.gamestate.suit then
		S.gamestate:suit(dt)
	end
end

function love.draw()
	A.lbox:render(1, function()
		love.graphics.clear()
		love.graphics.setColor(colors.white)
		if S.gamestate.draw then
			S.gamestate:draw()
		end
	end)
	A.lbox:render(2, function()
		love.graphics.clear(colors.clear)
		love.graphics.setColor(colors.white)
		if S.gamestate.drawUI then
			S.gamestate:drawUI()
		end
	end)
	if S.gamestate.postDraw then
		S.gamestate:postDraw()
	else
		A.lbox:draw()
	end

	if Settings.debug_ui then
		if S.gamestate.dbg_draw then
			S.gamestate:dbg_draw()
		end

		local _
		_, Settings.showfps = imgui.Checkbox("show fps", Settings.showfps)
	end

	if Settings.showfps then
		stats.draw()
	end
end

function love.mousepressed(x, y, m)
	x, y = A.lbox:win2screen(x, y)
	if S.gamestate.mousepressed then
		return S.gamestate:mousepressed(x, y, m)
	end
end

function love.mousereleased(x, y, m)
	S.input_method = "mouse"

	x, y = A.lbox:win2screen(x, y)
	if S.gamestate.mousereleased then
		return S.gamestate:mousereleased(x, y, m)
	end
end

function love.mousemoved(x, y, dx, dy, touch)
	S.input_method = "mouse"
	x, y = A.lbox:win2screen(x, y)
	dx, dy = dx/A.lbox.scale, dy/A.lbox.scale
	if S.gamestate.mousemoved then
		return S.gamestate:mousemoved(x, y, dx, dy, touch)
	end
end

gs.inject("keypressed", "key")
gs.inject("keyreleased")
gs.inject("gamepadpressed")
gs.inject("gamepadreleased", "gamepad")
gs.inject("gamepadaxis")

function love.textinput(k)
	if S.gamestate.textinput then
		return S.gamestate:textinput(k)
	end

	if not Settings.debug then return end

	-- debug shortcuts
	if k == 'r' then
		if A.client and A.client.connected then
			A.client:msg("reload")
		elseif A.server then
			reload.reload_all()
			gs.refresh()
			log.warning("TODO")
		else
			reload.reload_all()
			gs.refresh()
		end
	elseif k == 'R' then
		if A.client and A.client.connected then
			A.client:msg("reset")
		elseif A.server then
			log.warning("TODO")
		else
			reload.reload_all()
			gs.refresh()
			gs.exit()
			state.reset()
			reload.reload_all()
			gs.refresh()
		end
	elseif k == 'Q' then -- drop connections, then do 'R'
		if A.client then
			A.client:close()
		end

		A.client = nil
		A.server = nil
		reload.reload_all()
		gs.refresh()
		gs.exit()
		state.reset()
		reload.reload_all()
		gs.refresh()
	elseif k == '`' then
		log.info("debug ui: %s", Settings.debug_ui)
		Settings.debug_ui = not Settings.debug_ui
	end
end

function love.quit()
	if A.client then
		A.client:close()
	end
	if A.server then
		log.warning("closeme")
	end
end

function love.threaderror(thread, errorstr)
	error("Thread error!\n"..errorstr)
end

function love.resize()
	A.lbox:recalculate()
end

if love.window and love.window.isOpen() then
	require 'dbg_ui' ()
end
