local class  = require'gabe.class'
local shared = require 'net.shared'
local enet   = require 'enet'
local bitser = require 'bitser'
local log    = require 'log'

local Server = class(...)

function Server:init()
	local host   = assert(enet.host_create("*:"..shared.port, 10, shared.channels))
	self.host    = host
	log.info("Starting server at *:%_", shared.port)
	log.info("Max connections: 4")
end

function Server:close()
	log.info("Closing server")
	self.host:flush()
	self.host:destroy()
end

function Server:broadcast(data, channel, style)
	assert(type(data) == 'table')
	return self.host:broadcast(bitser.dumps(data), channel, style)
end

function Server:broadcastFrom(origin, data, channel, style)
	for i=1, self.host:peer_count() do
		local peer = self.host:get_peer(i)
		if peer:state() == "connected" then
			peer = shared.get_peer(peer)
			if peer:id() ~= origin:id() then
				peer:send(data, channel, style)
			end
		end
	end
end

function Server:msg(name, ...)
	self:broadcast({n=select('#', ...), name, ...}, 1)
end

function Server:msgTo(peer, name, ...)
	peer:send({n=select('#', ...), name, ...}, 1)
end

function Server:msgFrom(origin, name, ...)
	for i=1, self.host:peer_count() do
		local peer = self.host:get_peer(i)
		if peer:state() == "connected" then
			peer = shared.get_peer(peer)
			if peer:id() ~= origin:id() then
				peer:send({n=select('#',...), name, ...}, 1)
			end
		end
	end
end

function Server:update(dt)
	shared.pump_events(S.gamestate, A.server.host)
end

return Server
