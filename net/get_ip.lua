local channel, hostname = ...
local socket  = require 'socket'

local ip, err = socket.dns.toip(hostname)

if ip then
	channel:supply( { ip = ip } )
else
	channel:supply( { err = err } )
end
