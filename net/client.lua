local class  = require'gabe.class'
local shared = require 'net.shared'
local enet   = require 'enet'
local state  = require 'gabe.state'
local gs     = require 'gamestate'
local reload = require 'gabe.reload'

local Client = class(...)

function Client:init(hostname)
	self.host = enet.host_create()
	self.hostname = hostname
	assert(self.hostname)

	self.resolving = true
	self.time   = 0
	self.server_time = nil
	self.offset = -.1
	self.buffer = {}
end

function Client:close()
	log.info("closing client")
	self.connected = false
	self.resolving = false
	if self.conn then
		self.conn:disconnect(0)
	end
	if self.host then
		self.host:service(1)
		self.host:destroy()
		self.host = nil
	end
end

function Client:channel()
	if not self._channel then
		local th = love.thread.newThread("net/get_ip.lua")
		self._channel = love.thread.newChannel()
		th:start(self._channel, self.hostname)
	end
	return self._channel
end

function Client:update(dt)
	if self.resolving then
		local ip = self:channel():pop()
		if ip and ip.ip then
			local addr = ip.ip .. ":" .. shared.port
			local raw = assert(self.host:connect(addr, shared.channels))
			log.info("connnecting to %s", addr)
			self.conn = shared.get_peer(raw)
			self.conn:timeout(3)
			self.resolving = false
			self._channel = nil
		elseif ip and ip.err then
			self.err = "Could not get IP address. Are you online?"
			self:close()
		end
		return
	end

	self.time = self.time + dt
	if self.server_time then
		self.server_time = self.server_time + dt
	end

	if not self.host then return end

	--shared.pump_events(self)
	shared.pump_to_buffer(self)
	shared.process_buffer(self)

	if not self.closing then return end

	if not self.connected then
		if self.time > shared.timeout then
			self.err = "timed out"
			self:close()
		end
		return
	end

	-- put the actual update here
end

function Client:connect(id)
	self.connected = true
	log.info("client connected")
end

function Client:disconnect(peer, event_n)
	self.connected = false
	if event_n == shared.DISCONNECT.FULL then
		self.err = "Server is full."
	elseif event_n == shared.DISCONNECT.NOT_IN_LOBBY then
		self.err = "Wait for the game to finish before connecting."
	else
		self.err = "Unknown error code: " .. event_n
	end
	log.info("client disconnected: %s", self.err)
end

local messages = {}

function messages:toggleReady()
end

function messages:switchView(viewname, ...)
	log("vname %s", viewname)
	gs.switch(viewname, ...)
end

function messages:testVersion(server_version)
	local client_version = require "version"
	if server_version ~= client_version then
		error[[
Your version of the game does not match the server's.
You can get the latest version by downloading
https://alloyed.me/shared/sports-alpha.love
]]
	end
end

function messages:invalidRoom()
	S.gamestate.invalid = true
end

function messages:startInstancedMatch(gamename, ...)
	gs.switch("games."..gamename..".view", ...)
end

function messages:updatePlayers(players)
	self.players = players
end

function messages:updatePlayerColors(players)
	self.player_colors = players
end

function messages:setClientID(id)
	self.clientID = id
end

function messages:requestState()
	self:msg("updateView", S.gamestate.name)
end

function messages:sendLine(data)
	if data.origin and data.origin == self.clientID then
		return
	end

	if S.gamestate.overlay then
		S.gamestate.overlay:recv_line(data)
	end
end

function messages:tmpLine(data)
	if data.origin and data.origin == self.clientID then
		return
	end

	if S.gamestate.overlay then
		S.gamestate.overlay:recv_tmp_line(data)
	end
end

function messages:event(evname, evdata)
	if evdata.origin and evdata.origin == self.clientID then
		return
	end
	S.events:pub(evname, evdata)
end

function messages:reload()
	reload.reload_all()
	gs.refresh()
end

function messages:reset()
	reload.reload_all()
	gs.refresh()
	state.reset()
end

function Client:recv(data, peer, channel)
	if channel == 1 then -- RPC recieve
		local name = table.remove(data, 1)
		if not messages[name] then error("no such view " .. name) end
		return messages[name](self, unpack(data))
	end
	assert(S.gamestate.recv, class.xtype(S.gamestate))
	S.gamestate:recv(data, peer, channel)
end

function Client:msg(name, ...)
	if self.conn then
		self.conn:msg(name, ...)
		return true
	else
		return false
	end
end

return Client
