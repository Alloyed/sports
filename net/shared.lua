local class = require 'gabe.class'
local bitser = require 'bitser'

local shared = {}

shared.channels = 4
shared.CHANNEL = {
	VIEW = 0,
	MSG = 1,
}

if (require'version'):match("demo") then
	shared.port     = "4076"
else
	shared.port     = "4075"
end
shared.timeout  = 5

shared.DISCONNECT = {
	UNKNOWN = 0,
	FULL = 1,
	NOT_IN_LOBBY = 2, -- FIXME, drop-in should be possible
}

local peerobj = class((...)..".peer")

function peerobj:init(conn)
	self.conn = conn
	assert(self.conn)
end

function peerobj:id()
	return "P"..tostring(self.conn:index())
end

function peerobj:send(data, ...)
	return self.conn:send(bitser.dumps(data), ...)
end

function peerobj:timeout(...)
	return self.conn:timeout(...)
end

function peerobj:disconnect(n)
	assert(n == nil or type(n) == 'number')
	return self.conn:disconnect(n)
end

function peerobj:msg(name, ...)
	self.conn:send(bitser.dumps {name, ...}, 1)
end

peerobj._mt.__tostring = function(t)
	return t:id() --.. " " .. t.conn:connect_id()
end

local _peerobjs = setmetatable({}, {__mode = "k"})
function shared.get_peer(peer)
	local p = _peerobjs[peer]
	if not p then
		p = peerobj.new(peer) 
		_peerobjs[peer] = p
	end
	return p
end

function shared.pump_events(self, host)
	host = host or self.host
	local event = host:service()
	while event ~= nil do
		local peer = shared.get_peer(event.peer)
		if event.type == "connect" then
			self:connect(peer)
		elseif event.type == "disconnect" then
			self:disconnect(peer, event.data)
		elseif event.type == "receive" then
			local ok, data = pcall(bitser.loads, event.data)
			if not ok then
				error(string.format("invalid input data:\n%s\n%s",tostring(data),(require'inspect'(event))))
			end
			self:recv(data, peer, event.channel)
		end
		event = host:service()
	end
end

function shared.pump_to_buffer(net, host)
	host = host or net.host
	local event = host:service()
	while event ~= nil do
		local peer = shared.get_peer(event.peer)
		if event.type == "connect" then
			net:connect(peer)
		elseif event.type == "disconnect" then
			net:disconnect(peer, event.data)
		elseif event.type == "receive" then
			local ok, data = pcall(bitser.loads, event.data)
			if not ok then
				error(string.format("invalid input data:\n%s",(require'inspect'(event))))
			end
			table.insert(net.buffer, {data, peer, event.channel})
		end
		event = host:service()
	end
end

local function find_last_old_packet(net)
	local i = 1
	local buflen = #net.buffer
	if love.keyboard.isDown('q') then
		A.text = require'inspect'(net.buffer)
	end
	if A.text then
		imgui.Text(A.text)
	end
	--do return buflen end
	while i <= buflen do
		local packet = net.buffer[i]
		local data = packet[1]

		if data.time_now == nil then
			-- continue
		elseif net.server_time == nil then
			net.server_time = data.time_now
			net.delay = 4
			return 0
		else
			if (net.server_time+(net.delay/60)) < data.time_now then
				print("2new", net.server_time+(net.delay/60), data.time_now)
				-- return the last data. if this is the first data, then just
				-- hand over nothing (0)
				return i - 1
			end
		end

		i = i + 1
	end
	return buflen
end

function shared.process_buffer(net)
	table.sort(net.buffer, function(a, b)
		local at = a[1].time_now or 0
		local bt = b[1].time_now or 0
		return at > bt
	end)

	local buflen = find_last_old_packet(net)
	for i=1, buflen do
		net:recv(net.buffer[i][1], net.buffer[i][2], net.buffer[i][3])
	end

	-- TODO: batch shift
	for i=buflen, 1, -1 do
		table.remove(net.buffer, i)
	end
end

return shared
