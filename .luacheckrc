local app = {
	globals = {
		"A", "S", "MT", "Config", "C",
		"SW", "SH", "TARGET_SH", "TARGET_SW", "TS", "HTS",
		"dims", "log", "dbg", "scale", "sss", "Route", "Settings", "imgui"
	}
}

stds.app = app
std   = "luajit+love+app"

ignore={"431", "212", "21/._*"}

exclude_files = {"rocks"}
