return [[ // vim: ft=glsl :
#ifdef PIXEL

// #extension GL_EXT_gpu_shader4: enable

extern vec2 step_size;
extern vec2 tex_size;
vec4 effect( vec4 color, Image tex, vec2 tex_pos, vec2 screen_pos)
{
	// get color of pixels:
	//number alpha = texture2D(tex, texturePos).a;
	if (texture2D(tex, tex_pos).a > .5) {
		discard;
	}
	number alpha = 0.0;
	//vec2 tex_size = textureSize2D(tex, 0);
	vec2 step = vec2(step_size.x/tex_size.x, step_size.y/tex_size.y);
	alpha += texture2D(tex, tex_pos + vec2( step.x, 0.0f)).a;
	alpha += texture2D(tex, tex_pos + vec2(-step.x, 0.0f)).a;
	alpha += texture2D(tex, tex_pos + vec2(0.0f,  step.y)).a;
	alpha += texture2D(tex, tex_pos + vec2(0.0f, -step.y)).a;

	alpha += texture2D(tex, tex_pos + vec2(-step.x, -step.y)).a;
	alpha += texture2D(tex, tex_pos + vec2(step.x, -step.y)).a;
	alpha += texture2D(tex, tex_pos + vec2(-step.x, step.y)).a;
	alpha += texture2D(tex, tex_pos + vec2(step.x, step.y)).a;

	//if (alpha < .1) {
	//	discard;
	//}
	return vec4(color.rgb, alpha*color.a);
}
#endif

#ifdef VERTEX
vec4 position(mat4 transform_projection, vec4 vertex_position)
{
	return transform_projection * vertex_position;
}
#endif
]]
