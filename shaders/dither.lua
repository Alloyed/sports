return [[ // vim: ft=glsl :
#ifdef PIXEL
vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords)
{
	if (mod(int(screen_coords.x)+mod(int(screen_coords.y), 2), 2) == 0) {
		discard;
	}
	return Texel(texture, texture_coords) * color;
}
#endif

#ifdef VERTEX
vec4 position(mat4 transform_projection, vec4 vertex_position)
{
	return transform_projection * vertex_position;
}
#endif
]]
