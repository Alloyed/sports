return [[ // vim: ft=glsl :
#ifdef PIXEL
extern float level;
vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords)
{
	vec4 o = Texel(texture, texture_coords) * color;
	// values from http://stackoverflow.com/q/687261
	vec3 gray = vec3(dot(o.rgb, vec3(0.2989, .5870, 0.1140)));
	o.rgb = mix(o.rgb, gray, level);
	return o;
}
#endif

#ifdef VERTEX
vec4 position(mat4 transform_projection, vec4 vertex_position)
{
	return transform_projection * vertex_position;
}
#endif
]]
