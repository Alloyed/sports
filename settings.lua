-- App config

local nicks = {
	"Gaston",
	"Motavio",
	"Stan",
	"Celery",
	"Sandman",
	"Prompto",
	"Cornholio",
	"Doubleday",
	"Wrinkles",
	"Giorgio",
	"Mark",
	"Valentino",
	"Blofeld",
	"Mustard",
	"Ideaguy",
	"Yocheved",
	"Yorick",
	"Rosencrantz",
	"Guildenstern",
	"Hamtaro",
	"Caillou",
}
math.randomseed(love.timer.getTime())
local default_nick = nicks[math.random(#nicks)]

return _G.Settings or {
	-- release mode
	debug   = true,
	staging = false,
	verbose = true,
	-- window settings
	scale      = 1,
	fullscreen = false,
	stretch    = false,
	vsync      = true,
	resizable  = true,

	-- run mode
	is_server = false,
	nick = default_nick,

	-- audio
	volume = .75,
	mute_music = false,

	view = "views.mainmenu",
}
