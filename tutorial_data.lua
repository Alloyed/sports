-- assets for the tutorials
return setmetatable({
	["games.archery.view"] = {
		video = love.graphics.newVideo("assets/tutorial.ogv", false),
		title = "VERSARCHERY",
		description = [[
{move} to move.
{fire} to aim, then release to fire.
{reset}

Hit as many [lite]TARGETS as you can
before time runs out.

Bullseyes give you extra [lite]POINTS.
]]
	},
	["views.games.spelunking"] = {
		video = love.graphics.newVideo("assets/tutorial.ogv", false),
		title = "SPELUNKING",
		description = [[
WIP
]]
	}
}, { __index = function()
	return {
		video = love.graphics.newVideo("assets/tutorial.ogv", false),
		title = "WIP GAME",
		description = [[
Tutorial not yet written.
Go do that in tutorial_data.lua
]],
	}
end})
