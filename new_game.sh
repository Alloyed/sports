#!/bin/sh

if [[ -z $1 ]]; then
    echo "Needs an argument for file name."
    exit
fi

cp -iv views/games/template.lua views/games/$1.lua
cp -iv games/template.lua games/$1.lua

git add views/games/$1.lua
git add games/$1.lua
