input:defineKeys('left',  'left', 'a')
input:defineKeys('right', 'right', 'd')
input:defineKeys('crouch', 'down', 's')
input:defineKeys('jump', 'x', 'k', 'up', 'w')
input:defineKeys('shoot', 'z', 'j')

-- TODO: gamepads, multiple players

local function pollInput(obj)
	local x = 0
	if input:isDown('left') then
		x = x + 100
	end
	if input:isDown('right') then
		x = x - 100
	end
	obj.v.x = x

	obj.crouch = input:isDown('crouch')

	if input:pressed('jump') then
		obj:tryJump()
	elseif input:released('jump') then
		obj:stopJump()
	end

	if input:pressed('shoot') then
		obj:tryShoot()
	end
end

function love.update(dt)
	pollInput(player)
end
